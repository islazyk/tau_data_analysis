<div align="center">
    <h1>
        Tau Data Analysis
    </h1>
</div>

<div align="center">
    <b>SUSY Tau+X</b>
</div>

----

The following repository contains baseline analysis scripts for <b> SUSY Tau+X </b> search using Machine Learning techniques.

<b> Algorithms: </b>
* Standard Analysis (Cut & Count Based)
* Boosted Decision Trees (XGBoost) 
* Neural Networks

<b> Libraries: </b>

ROOT I/O:

* [UpROOT](https://uproot.readthedocs.io/en/latest/)

Data Processing:

* [Awkward Array](https://awkward-array.readthedocs.io/en/latest/)
* [NumPy](https://numpy.org/)
* [Pandas](https://pandas.pydata.org/)

Data Visualization:

* [Atlas MPL Style](https://atlas-mpl.readthedocs.io/en/latest/styles.html)
* [Matplotlib](https://matplotlib.org/)
* [Seaborn](https://seaborn.pydata.org/)

Machine Learning:

* [SciPy](https://scipy.org/)
* [SkLearn](https://scikit-learn.org/stable/)
* [XGBoost](https://xgboost.readthedocs.io/en/stable/)

Other:

* [tqdm](https://tqdm.github.io/)

----

<b> Pipelines: </b>
* Regression
* Binary-classification
* Multiclass-classification

----

<b> Functionalities: </b>
* Stopping function preventing overfitting
* Cross-validation
    * k-fold cross-validation
* Hyperparameters tuning:
    * randomized grid-search
    * exhaustive grid-search
* GPU computing

--- 
