import os, sys
import numpy as np 
import awkward as ak 
import uproot 
import pandas 
import pickle
import gc 
import argparse 


run = "Run3"

if run == "Run2":
    campaigns = ["MC20a", "MC20d", "MC20e"]
    ffzmm_hjvt = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.28/export/templates_run2/zmm_templates_hjvt_run2.root"
    ffmj_hjvt = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.28/export/templates_run2/mj_templates_hjvt_run2.root"
    ffmuhad_ljvt = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.28/export/templates_run2/muhad_templates_ljvt_run2.root"

elif run == "Run3":
    campaigns = ["MC23a", "MC23c"]
    ffzmm_hjvt = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.28/comp_new_ff/store/two_antimed_template_hjvt_width.root"
    ffmj_hjvt = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.28/comp_new_ff/store/mj_antimed_template_hjvt_width.root"
    ffmuhad_ljvt = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.28/comp_new_ff/store/muhad_antimed_template_ljvt_width.root"

else:
    print("Wait like 10-20 years for Run4")

bkgs = ["data", "ttbar", "diboson", "wenu", "zee", "singletop", "wmunu", "zmumu", "znunu"] # "ztautau", "wtaunu" #These latter three I expect will need to be split


#Function to config the correct num/denom names as TEfficiency objects cannot be read by uproot
def getstring(abs_tau_eta, tau_dm):

    result = "medium"
    strdm = "_dm"
    streta = "_eta"
    abs_tau_eta = abs(abs_tau_eta)

    if abs_tau_eta >= 0 and abs_tau_eta < 1:
        streta = streta + "0"
    elif abs_tau_eta >= 1 and abs_tau_eta < 1.38:
        streta = streta + "1"
    elif abs_tau_eta >= 1.51 and abs_tau_eta < 2.5:
        streta = streta + "2"
    else:
        print("tau eta out of range")

    if tau_dm == 0:
        strdm = strdm + "0"
    elif tau_dm == 1:
        strdm = strdm + "1"
    elif tau_dm == 2:
        strdm = strdm + "2"
    elif tau_dm == 3:
        strdm = strdm + "3"
    elif tau_dm == 4:
        strdm = strdm + "4"
    else:
        print("tau decay mode out of range")

    num_result = result + streta + strdm
    denom_result = result + streta + strdm + "_denom"
    return num_result, denom_result

#Function to get the fake factor weights 
def getFFAlt(filepath, tau_pt, tau_eta_abs, tau_dm, ffdict):
    #Get the numerator histogram and divide it by the denominator histogram and store it as a list in a dict
    num_hist_name, denom_hist_name = getstring(tau_eta_abs, tau_dm)[0], getstring(tau_eta_abs, tau_dm)[1]
    ptval = np.array([20,25,30,35,40,45,50,60,70,90,150,3000000])
    ptindex = np.searchsorted(ptval,tau_pt)
    if ptindex==0:
        print("Tau pt less than 20 GeV, something is seriously wrong!")
    ptname = "pt" + str(ptval[ptindex-1]) + str(ptval[ptindex])
    dict_entry_name = ptname + "_" + num_hist_name
    tempweight = 1
    if dict_entry_name in ffdict:
        tempweight = ffdict[dict_entry_name]
    else:
        with uproot.open(filepath) as file:
            num_hist = file[num_hist_name]
            denom_hist = file[denom_hist_name]

        #Get the bin index
            bin_index = np.digitize([tau_pt], num_hist.axis().edges()) - 1

            if tau_pt >= num_hist.axis().edges()[-1]: #Make the last bin inclusive
                tempweight = num_hist.values()[-1]/denom_hist.values()[-1]
            else:
                tempweight = num_hist.values()[bin_index]/denom_hist.values()[bin_index]
        ffdict[dict_entry_name] = tempweight
        
    return float(tempweight)

#Equivalent substitue for ROOT::TLorentzVector
class LorentzVector:
    def __init__(self):
        self.E = 0
        self.px = 0
        self.py = 0
        self.pz = 0

    def SetPtEtaPhiM(self, pt, eta, phi, m):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.m = m 

        self.px = pt * np.cos(phi)
        self.py = pt * np.sin(phi)
        self.pz = pt * np.sinh(eta)
        self.E = np.sqrt(self.px**2 + self.py**2 + self.pz** + m**2)

    def __add__(self, other):
        if not isinstance(other, LorentzVector):
            raise TypeError("Trying to add an LorentzVector with some other object")
        
        result = LorentzVector()

        result.E = self.E + other.E 
        result.px = self.px + other.px 
        result.py = self.py + other.py 
        result.pz = self.pz + other.pz 

        result.pt = np.sqrt(result.px**2 + result.py**2)
        result.eta = np.arcsinh(result.pz / result.pt)
        result.phi = np.arctan2(result.py, result.px)
        result.m = np.sqrt(result.E**2 - result.px**2 + result.py**2 - result.pz**2)

        return result 

    
zmm_hjvt_dict = {}
muhad_ljvt_dict = {}
mj_hjvt_dict = {}


#Start the main loop over all backgrounds and campaings
for bkg in bkgs:
    for campaign in campaigns:
        path = f"/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.28/tauX/PHYS/{campaign}/vector_XEplateau/{bkg}.root"
        with uproot.open(path) as file:
            
            ar = file["NOMINAL"].arrays(library = "ak")
            print(f"Processing background {bkg} for campaign {campaign}")
            print(f"Number of events in file: {len(ar.eventClean)}")

            #Applying some preselection cuts
            # jet_n >=2 is probably ok here, since require exactly one ff tau
            ar = ar[ar.tau_n != 0]
            ar = ar[ar.jet_n >= 2]
            ar = ar[(ar.jet_n + ar.tau_n) >= 3]
            ar = ar[ar.IsMETTrigPassed != 0]
            ar = ar[ar.isBadTile == 0]
            ar = ar[ar.eventClean != 0]
            ar = ar[ar.met >= 200000]

            
            if bkg == "data":
                ar["totalweight"] = 1
            else:
                ar["totalweight"] = ar.lumiweight * ar.mcEventWeight * ar.pileupweight * ar.jvt_weight * ar.bjet_weight * ar.tau_medium_weight * ar.ele_weight * ar.mu_weight * ar.beamSpotWeight

            # array tau_counter?    
            ar["tau_counter"] = ak.sum(ar.tau_JetRNNMedium,axis=1)
            ar["tau_cand_counter"] = ar.tau_n - ar.tau_counter
    
            # Need to treat 1tau and 2tau channels in a separate way? Not really, just give medium taus weight=1
            # Select exactly 1 tau candidate
            ar = ar[ar.tau_cand_counter >= 1]
            # We either do the trivial case of 1 tau candidate, or >1 tau candidate, but only using the leading tau. 

            ar = ar[ar.jet_pt[:, 0]/1000 >= 120]  # this should always happen
            ar = ar[ar.jet_pt[:, 1]/1000 >= 20]   # also always happens by selection 
            ar = ar[ar.jet_delPhiMet[:, 0] >= 0.4] 
            ar = ar[ar.jet_delPhiMet[:, 1] >= 0.4] 

            
            get_zmm_hjvt_weight = 1
            get_mj_hjvt_weight = 1
            get_muhad_ljvt_weight = 1

            # mask RNN tau score with 1/0 depending on the cut, subtract taus that are medium, multiply by the weights. This way only anti-ID taus get the weights.
            ar["ff_weight_mask"] = ak.values_astype((ar.tau_RNNJetScoreSigTrans) > 0.01,"int64") -  (ar.tau_JetRNNMedium)
            zmm_hjvt_weight = ak.to_list(ar["ff_weight_mask"])
            muhad_ljvt_weight = ak.to_list(ar["ff_weight_mask"])
            mj_hjvt_weight = ak.to_list(ar["ff_weight_mask"])

            tau_JetRNNMedium_dict = {}
            
            for k in range(len(ar.eventClean)):
                if(k%10000==0):
                    print("Processed: ",k,"/",len(ar.eventClean))
                count_taus=0
                for l in range(len(ar.tau_pt[k])):
                    if ar.tau_RNNJetScoreSigTrans[k][l] < 0.01:
                        continue
                    if ar.tau_JetRNNMedium[k][l]==1:
                        continue
                    count_taus = count_taus+1 # count candidate taus

                    get_zmm_hjvt_weight = getFFAlt(ffzmm_hjvt, ar.tau_pt[k][l]/1000, abs(ar.tau_eta[k][l]), ar.tau_NNDecayMode[k][l],zmm_hjvt_dict)
                    get_muhad_ljvt_weight = getFFAlt(ffmuhad_ljvt, ar.tau_pt[k][l]/1000, abs(ar.tau_eta[k][l]), ar.tau_NNDecayMode[k][l],muhad_ljvt_dict)
                    get_mj_hjvt_weight = getFFAlt(ffmj_hjvt, ar.tau_pt[k][l]/1000, abs(ar.tau_eta[k][l]), ar.tau_NNDecayMode[k][l],mj_hjvt_dict)

                    if(count_taus==1):
                        zmm_hjvt_weight[k][l] = zmm_hjvt_weight[k][l]  * get_zmm_hjvt_weight
                        muhad_ljvt_weight[k][l] = muhad_ljvt_weight[k][l]  * get_muhad_ljvt_weight
                        mj_hjvt_weight[k][l] = mj_hjvt_weight[k][l]  * get_mj_hjvt_weight


                    if(count_taus>=2): #should only happen for the >1 tau candidate case
                        if(ar.tau_cand_counter[k]==1):
                            print("Candidate counter==1, shouldn't be in the >=2 candidates loop")
                        zmm_hjvt_weight[k][l] = zmm_hjvt_weight[k][l]  * (1 - get_zmm_hjvt_weight)
                        muhad_ljvt_weight[k][l] = muhad_ljvt_weight[k][l] * (1 - get_muhad_ljvt_weight)
                        mj_hjvt_weight[k][l] = mj_hjvt_weight[k][l] * (1 - get_mj_hjvt_weight)
                        
                        # for every other tau candidate, suppress our leading tau by the probability that they are NOT promoted.

            
            ar["zmm_hjvt_weight"] = ak.Array(zmm_hjvt_weight)
            ar["muhad_ljvt_weight"] = ak.Array(muhad_ljvt_weight)
            ar["mj_hvjt_weight"] = ak.Array(mj_hjvt_weight)

            # need to understand at which point to redefine tau_1_pt etc values, here or in a separate postprod?

            #Write to file - will have a lot of n_feature variables due to some bug/oversight:
            #But it's not a problem as we don't need to load these buggy variables
            #Solution provided by uproot certified gamer Angus
            with uproot.recreate(f"/disk/atlas3/users/thillers/fakeTaus/data_driven/R24.2.28/{campaign}/{bkg}_faketau.root") as f:
                f["NOMINAL"] = dict(
                    zip(ak.fields(ar), ak.unzip(ar))
                )

            #Explicitly remove object from memory
            del ar 
            gc.collect()
            
print ("job done")
