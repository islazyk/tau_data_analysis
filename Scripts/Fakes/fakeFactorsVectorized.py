import os, sys
import numpy as np 
import awkward as ak 
import uproot 
import pandas 
import pickle
import gc 
import argparse 

#ok, step number 1 -- need usable FFs for ML.
# For these the first approach is to use MC-based only (also easier to interpret).
# So for now let's start with something simple?


#Define the paths to the fake factors - these are optimized for Run2 - but it's the best we have for the moment
ffzee = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.7/zmm/two_comp_run2/two_hjvt_tight.root"
ffzmm = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.7/muhad/muhad_comp_run2/mj_ljvt_tight.root"
ffhjvt = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.7/mj/mj_comp_run2/mj_hjvt_tight.root"
ffhjvt2 = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.7/muhad/muhad_comp_run2/mj_hjvt_tight.root"

#Function to config the correct num/denom names as TEfficiency objects cannot be read by uproot
def getstring(abs_tau_eta, tau_dm):

    result = "medium"
    strdm = "_dm"
    streta = "_eta"
    abs_tau_eta = abs(abs_tau_eta)

    if abs_tau_eta >= 0 and abs_tau_eta < 1:
        streta = streta + "0"
    elif abs_tau_eta >= 1 and abs_tau_eta < 1.38:
        streta = streta + "1"
    elif abs_tau_eta >= 1.51 and abs_tau_eta < 2.5:
        streta = streta + "2"
    else:
        print("tau eta out of range")

    if tau_dm == 0:
        strdm = strdm + "0"
    elif tau_dm == 1:
        strdm = strdm + "1"
    elif tau_dm == 2:
        strdm = strdm + "2"
    elif tau_dm == 3:
        strdm = strdm + "3"
    elif tau_dm == 4:
        strdm = strdm + "4"
    else:
        print("tau decay mode out of range")

    num_result = result + streta + strdm
    denom_result = result + streta + strdm + "_denom"
    return num_result, denom_result

#Function to get the fake factor weights 
def getFFAlt(filepath, tau_pt, tau_eta_abs, tau_dm, ffdict):
    #Get the numerator histogram and divide it by the denominator histogram and store it as a list in a dict
    num_hist_name, denom_hist_name = getstring(tau_eta_abs, tau_dm)[0], getstring(tau_eta_abs, tau_dm)[1]
    ptval = np.array([20,25,30,35,40,45,50,60,70,90,150,3000000])
    ptindex = np.searchsorted(ptval,tau_pt)
    if ptindex==0:
        print("Tau pt less than 20 GeV, something is seriously wrong!")
    ptname = "pt" + str(ptval[ptindex-1]) + str(ptval[ptindex])
    dict_entry_name = ptname + "_" + num_hist_name
    tempweight = 1
    if dict_entry_name in ffdict:
        tempweight = ffdict[dict_entry_name]
    else:
        with uproot.open(filepath) as file:
            num_hist = file[num_hist_name]
            denom_hist = file[denom_hist_name]

        #Get the bin index
            bin_index = np.digitize([tau_pt], num_hist.axis().edges()) - 1

            if tau_pt >= num_hist.axis().edges()[-1]: #Make the last bin inclusive
                tempweight = num_hist.values()[-1]/denom_hist.values()[-1]
            else:
                tempweight = num_hist.values()[bin_index]/denom_hist.values()[bin_index]
        ffdict[dict_entry_name] = tempweight
        
    return float(tempweight)

#Equivalent substitue for ROOT::TLorentzVector
class LorentzVector:
    def __init__(self):
        self.E = 0
        self.px = 0
        self.py = 0
        self.pz = 0

    def SetPtEtaPhiM(self, pt, eta, phi, m):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.m = m 

        self.px = pt * np.cos(phi)
        self.py = pt * np.sin(phi)
        self.pz = pt * np.sinh(eta)
        self.E = np.sqrt(self.px**2 + self.py**2 + self.pz** + m**2)

    def __add__(self, other):
        if not isinstance(other, LorentzVector):
            raise TypeError("Trying to add an LorentzVector with some other object")
        
        result = LorentzVector()

        result.E = self.E + other.E 
        result.px = self.px + other.px 
        result.py = self.py + other.py 
        result.pz = self.pz + other.pz 

        result.pt = np.sqrt(result.px**2 + result.py**2)
        result.eta = np.arcsinh(result.pz / result.pt)
        result.phi = np.arctan2(result.py, result.px)
        result.m = np.sqrt(result.E**2 - result.px**2 + result.py**2 - result.pz**2)

        return result 

    
ffdict1 = {}
ffdict2 = {}
ffdict3 = {}
ffdict4 = {}

#Take input from the shell script used to run on batch 
parser = argparse.ArgumentParser()
parser.add_argument("--backgrounds", nargs = "*", help = "List of backgrounds, separated by spaces")
parser.add_argument("--years", nargs = "*", help = "List of years, a/d/e or a/c, separated by spaces")
parser.add_argument("--MC", help = "Which MC campaign / Run to consider. Either MC20 (Run2) or MC23 (Run3)")
args = parser.parse_args()

bkgs = args.backgrounds
years = args.years
MC = args.MC

#Start the main loop over all backgrounds and campaings
for bkg in bkgs:
    for year in years:
        path = f"/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.28/tauX/PHYS/{MC}{year}/vector_XEplateau/{bkg}.root"
        with uproot.open(path) as file:
            
            ar = file["NOMINAL"].arrays(library = "ak")
            print(f"Processing background {bkg} for campaign {MC}{year}")
            print(f"Number of events in file: {len(ar.eventClean)}")

            #Applying some preselection cuts
            # jet_n >=2 is probably ok here, since require exactly one ff tau
            ar = ar[ar.tau_n != 0]
            ar = ar[ar.jet_n >= 2] 
            ar = ar[(ar.jet_n + ar.tau_n) >= 3]
            ar = ar[ar.IsMETTrigPassed != 0]
            ar = ar[ar.isBadTile == 0]
            ar = ar[ar.eventClean != 0]
            ar = ar[ar.met >= 180000]

            
            if bkg == "data":
                ar["totalweight"] = 1
            else:
                ar["totalweight"] = ar.lumiweight * ar.mcEventWeight * ar.pileupweight * ar.jvt_weight * ar.bjet_weight * ar.tau_medium_weight * ar.ele_weight * ar.mu_weight * ar.beamSpotWeight

            # array tau_counter?    
            ar["tau_counter"] = ak.sum(ar.tau_JetRNNMedium,axis=1)
            ar["tau_cand_counter"] = ar.tau_n - ar.tau_counter
    
            # Need to treat 1tau and 2tau channels in a separate way? Not really, just give medium taus weight=1
            # Select exactly 1 tau candidate
            ar = ar[ar.tau_cand_counter == 1]
            # do the (in this case) trivial selection 

            ar = ar[ar.jet_pt[:, 0]/1000 >= 120]
            ar = ar[ar.jet_pt[:, 1]/1000 >= 20]
            ar = ar[ar.jet_delPhiMet[:, 0] >= 0.4]     
            ar = ar[ar.jet_delPhiMet[:, 1] >= 0.4] 

            
            get_ele_weight = 1
            get_mu_weight = 1
            get_mj_weight = 1
            get_muhad_weight = 1
            # mask RNN tau score with 1/0 depending on the cut, subtract taus that are medium, multiply by the weights. This way only anti-ID taus get the weights.
            ar["ff_weight_mask"] = ak.values_astype((ar.tau_RNNJetScoreSigTrans) > 0.01,"int64") -  (ar.tau_JetRNNMedium)
            ff_weight_1=ak.to_list(ar["ff_weight_mask"])
            ff_weight_2=ak.to_list(ar["ff_weight_mask"])
            ff_weight_3=ak.to_list(ar["ff_weight_mask"])
            ff_weight_4=ak.to_list(ar["ff_weight_mask"])
            
            for k in range(len(ar.eventClean)):
                if(k%10000==0):
                    print("Processed: ",k,"/",len(ar.eventClean))
                for l in range(len(ar.tau_pt[k])):
                    if ar.tau_RNNJetScoreSigTrans[k][l] < 0.01:
                        continue
                    if ar.tau_JetRNNMedium[k][l]==1:
                        continue
                    get_ele_weight = getFFAlt(ffzee, ar.tau_pt[k][l]/1000, abs(ar.tau_eta[k][l]), ar.tau_NNDecayMode[k][l],ffdict1)
                    get_mu_weight = getFFAlt(ffzmm, ar.tau_pt[k][l]/1000, abs(ar.tau_eta[k][l]), ar.tau_NNDecayMode[k][l],ffdict2)
                    get_mj_weight = getFFAlt(ffhjvt, ar.tau_pt[k][l]/1000, abs(ar.tau_eta[k][l]), ar.tau_NNDecayMode[k][l],ffdict3)
                    get_muhad_weight = getFFAlt(ffhjvt2, ar.tau_pt[k][l]/1000, abs(ar.tau_eta[k][l]), ar.tau_NNDecayMode[k][l],ffdict4)
                
                    ff_weight_1[k][l] = ff_weight_1[k][l]  * get_ele_weight
                    ff_weight_2[k][l] = ff_weight_2[k][l]  * get_mu_weight
                    ff_weight_3[k][l] = ff_weight_3[k][l]  * get_mj_weight
                    ff_weight_4[k][l] = ff_weight_4[k][l]  * get_muhad_weight
            
            ar["ff_weight_1"] = ak.Array(ff_weight_1)
            ar["ff_weight_2"] = ak.Array(ff_weight_2)
            ar["ff_weight_3"] = ak.Array(ff_weight_3)
            ar["ff_weight_4"] = ak.Array(ff_weight_4)
            
            # need to understand at which point to redefine tau_1_pt etc values, here or in a separate postprod?

            #Write to file - will have a lot of n_feature variables due to some bug/oversight:
            #But it's not a problem as we don't need to load these buggy variables
            #Solution provided by uproot certified gamer Angus
            #with uproot.recreate(f"/disk/atlas3/users/thillers/fakeTaus/R24.2.28/MC20{year}/{bkg}_faketau.root") as f:
            with uproot.recreate(f"/disk/atlas3/users/thillers/fakeTaus/batchtest/{MC}{year}/{bkg}_faketau.root") as f: #Temp check for Znunu - might have cut it in half by accident
                f["NOMINAL"] = dict(
                    zip(ak.fields(ar), ak.unzip(ar))
                )

            #Explicitly remove object from memory
            del ar 
            gc.collect()
            
print ("job done")