import os, sys
import numpy as np 
import awkward as ak 
import uproot 

bkgs = [
    "data"
]
years = ["a"]

#Define the paths to the fake factors
ffzee = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.7/zmm/two_comp_run2/two_hjvt_tight.root"
ffzmm = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.7/muhad/muhad_comp_run2/mj_ljvt_tight.root"
ffhjvt = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.7/mj/mj_comp_run2/mj_hjvt_tight.root"
ffhjvt2 = "/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.7/muhad/muhad_comp_run2/mj_hjvt_tight.root"

#Function to config the correct num/denom names as TEfficiency objects cannot be read by uproot
def getstring(abs_tau_eta, tau_dm):

    result = "medium"
    strdm = "_dm"
    streta = "_eta"
    abs_tau_eta = abs(abs_tau_eta)

    if abs_tau_eta >= 0 and abs_tau_eta < 1:
        streta = streta + "0"
    elif abs_tau_eta >= 1 and abs_tau_eta < 1.38:
        streta = streta + "1"
    elif abs_tau_eta >= 1.51 and abs_tau_eta < 2.5:
        streta = streta + "2"
    else:
        print("tau eta out of range")

    if tau_dm == 0:
        strdm = strdm + "0"
    elif tau_dm == 1:
        strdm = strdm + "1"
    elif tau_dm == 2:
        strdm = strdm + "2"
    elif tau_dm == 3:
        strdm = strdm + "3"
    elif tau_dm == 4:
        strdm = strdm + "4"
    else:
        print("tau decay mode out of range")

    num_result = result + streta + strdm
    denom_result = result + streta + strdm + "_denom"
    return num_result, denom_result

#Function to get the fake factor weights 
def getFFAlt(filepath, tau_pt, tau_eta_abs, tau_dm):
    #Get the numerator histogram and divide it by the denominator histogram and store it as a list in a dict
    num_hist_name, denom_hist_name = getstring(tau_eta_abs, tau_dm)[0], getstring(tau_eta_abs, tau_dm)[1]
    
    with uproot.open(filepath) as file:

        num_hist = file[num_hist_name]
        denom_hist = file[denom_hist_name]

        #Get the bin index
        bin_index = np.digitize([tau_pt], num_hist.axis().edges()) - 1

        if tau_pt >= num_hist.axis().edges()[-1]: #Make the last bin inclusive
            tempweight = num_hist.values()[-1]/denom_hist.values()[-1]
        else:
            tempweight = num_hist.values()[bin_index]/denom_hist.values()[bin_index]

        return float(tempweight)

#Equivalent substitue for ROOT::TLorentzVector
class LorentzVector:
    def __init__(self):
        self.E = 0
        self.px = 0
        self.py = 0
        self.pz = 0

    def SetPtEtaPhiM(self, pt, eta, phi, m):
        self.pt = pt
        self.eta = eta
        self.phi = phi
        self.m = m 

        self.px = pt * np.cos(phi)
        self.py = pt * np.sin(phi)
        self.pz = pt * np.sinh(eta)
        self.E = np.sqrt(self.px**2 + self.py**2 + self.pz** + m**2)

    def __add__(self, other):
        if not isinstance(other, LorentzVector):
            raise TypeError("Trying to add an LorentzVector with some other object")
        
        result = LorentzVector()

        result.E = self.E + other.E 
        result.px = self.px + other.px 
        result.py = self.py + other.py 
        result.pz = self.pz + other.pz 

        result.pt = np.sqrt(result.px**2 + result.py**2)
        result.eta = np.arcsinh(result.pz / result.pt)
        result.phi = np.arctan2(result.py, result.px)
        result.m = np.sqrt(result.E**2 - result.px**2 + result.py**2 - result.pz**2)

        return result 

#Initialize an array to save events to 
selected_events = ak.Array([])

#Start the main loop over all backgrounds and campaings
for bkg in bkgs:
    for year in years:
        path = f"/disk/atlas3/users/nifomin/tauX/ff_fodder_24.2.7/smalltest/smalltest.root"
        with uproot.open(path) as file:
            ar = file["NOMINAL"].arrays()

            #Applying some preselection cuts
            ar = ar[ar.tau_n != 0]
            ar = ar[ar.jet_n != 0] 
            ar = ar[(ar.jet_n + ar.tau_n) >= 3]
            ar = ar[ar.IsMETTrigPassed != 0]
            ar = ar[ar.isBadTile == 0]
            ar = ar[ar.eventClean != 0]
            ar = ar[ar.met >= 180000]

            
            if bkg == "data":
                ar["totalweight"] = 1
            else:
                ar["totalweight"] = ar.lumiweight * ar.mcEventWeight * ar.pileupweight * ar.jvt_weight * ar.bjet_weight * ar.tau_medium_weight * ar.ele_weight * ar.mu_weight * ar.beamSpotWeight

            for k in range(len(ar.eventClean)):
                if k % 10000 == 0:
                    print(f"{bkg}: {k} events processed")


                #Prevent accidental unblinding for the SRs
                if ((ar.met[k] > 300000) and (ar.ht[k] > 800000) and (ar.met[k]/ar.meff[k] > 0.2)) and bkg == "data":
                    continue 

                tau_counter = 0
                tau_index_prep = -999

                for i in range(len(ar.tau_pt[k])):
                    if ar.tau_JetRNNMedium[k][i] == 1:
                        tau_counter += 1
                    if tau_counter == 1:
                        tau_index_prep = i 
                        tau_counter += 1

                if tau_counter > 0:
                    tau_counter -= 1

                if tau_counter > 1:
                    continue #Only the leading and the subleading tau compose the FF source

                tau_ids_n = 1
                if tau_counter == 1:
                    tau_ids_n = 1
                if tau_counter == 0:
                    tau_ids_n = ar.tau_n[k]

                tau_ids = [0] * tau_ids_n
                if tau_counter == 1:
                    tau_ids[0] = tau_index_prep
                elif tau_counter == 0:
                    tau_ids = list(range(ar.tau_n[k]))

                if tau_counter > 0:
                    continue

                for tau_index in tau_ids:

                    if ar.tau_RNNJetScoreSigTrans[k][tau_index] < 0.01:
                        continue
                    
                    if tau_index == 0:
                        tau_index1, tau_index2 = 1, 2
                    elif tau_index == 2:
                        tau_index1, tau_index2 = 0, 2
                    elif tau_index >= 2:
                        tau_index1, tau_index2 = 0, 1

                    #Initiate a bunch of four-vectors
                    tau1, jet1, jet2, metvec, mu1  = LorentzVector(), LorentzVector(), LorentzVector(), LorentzVector(), LorentzVector()

                    tau1.SetPtEtaPhiM(ar.tau_pt[k][tau_index], ar.tau_eta[k][tau_index], ar.tau_phi[k][tau_index], 0)
                    if ar.mu_n[k] > 0:
                        mu1.SetPtEtaPhiM(ar.mu_pt[k][0], ar.mu_eta[k][0], ar.mu_phi[k][0], 0)
                    
                    t1, t2, j1, j2 = 0, 0, 0, 0

                    if ar.jet_n[k] >= 1:
                        j1 = ar.jet_pt[k][0]
                    if ar.jet_n[k] >= 2:
                        j2 = ar.jet_pt[k][1]
                    if ar.tau_n[k] >= 2:
                        t1 = ar.tau_pt[k][tau_index1]
                    if ar.tau_n[k] >= 3:
                        t2 = ar.tau_pt[k][tau_index2]
                    
                    #Consider the leading jet objects and compare their momenta
                    if (t1 > j1):
                        jet1.SetPtEtaPhiM(ar.tau_pt[k][tau_index1], ar.tau_eta[k][tau_index1], ar.tau_phi[k][tau_index1], 0)
                    if (j1 > t1):
                        jet1.SetPtEtaPhiM(ar.jet_pt[k][0], ar.jet_pt[k][0], ar.jet_phi[k][0], 0)

                    #Consider the subleading jet objects
                    if t2 > j1:
                        jet2.SetPtEtaPhiM(ar.tau_pt[k][tau_index2], ar.tau_eta[k][tau_index2], ar.tau_phi[k][tau_index2], 0)
                    if t1 > j1 and j1 > t2:
                        jet2.SetPtEtaPhiM(ar.jet_pt[k][0], ar.jet_eta[k][0], ar.jet_phi[k][0], 0)
                    if j1 > t1 and t1 > j2:
                        jet2.SetPtEtaPhiM(ar.tau_pt[k][tau_index1], ar.tau_eta[k][tau_index1], ar.tau_phi[k][tau_index1], 0)
                    if j1 > t1 and j2 > t1:
                        jet2.SetPtEtaPhiM(ar.jet_pt[k][1], ar.jet_eta[k][1], ar.jet_phi[k][1], 0)

                    
                    delPhiMet = np.abs(np.abs(np.abs(jet1.phi - ar.met_phi[k]) - np.pi) - np.pi)
                    mTJetMet = np.sqrt(2*jet1.pt * ar.met[k] * (1 - np.cos(delPhiMet)))
                    delPhiMet2 = np.abs(np.abs(np.abs(jet2.phi - ar.met_phi[k]) - np.pi) - np.pi)
                    mTJetMet2 = np.sqrt(2*jet2.pt * ar.met[k] * (1 - np.cos(delPhiMet2)))
                    Mtaumu = 0
                    if ar.mu_n[k] > 0:
                        Mtaumu = (tau1 + mu1).m


                    if jet1.pt < 120000:
                        continue
                    if jet2.pt < 20000:
                        continue
                    if j1 > t1 and ar.jet_isBadTight[k][0] == 1:
                        continue
                    if delPhiMet < 0.4 and (ar.met[k]/ar.meff[k] > 0.2):
                        continue
                    if delPhiMet2 < 0.4 and (ar.met[k]/ar.meff[k] > 0.2):
                        continue

                    #Onto computing the weights
                    ffweight, ffweightmu, ffweightmj, ffweightmuhad = 1, 1, 1, 1

                    if tau_counter == 0:
                        for l in range(ar.tau_n[k]):

                            if ar.tau_RNNJetScoreSigTrans[k][l] < 0.01:
                                continue
                            if ar.tau_NNDecayMode[k][l] > 4:
                                continue

                            tempweight, tempweightmu, tempweightmj, tempweightmuhad = 1, 1, 1, 1

                            #Call the getFF function, hopefully this works
                            
                            get_ele_weight = getFFAlt(ffzee, ar.tau_pt[k][l]/1000, abs(ar.tau_eta[k][l]), ar.tau_NNDecayMode[k][l])
                            get_mu_weight = getFFAlt(ffzmm, ar.tau_pt[k][l]/1000, abs(ar.tau_eta[k][l]), ar.tau_NNDecayMode[k][l])
                            get_mj_weight = getFFAlt(ffhjvt, ar.tau_pt[k][l]/1000, abs(ar.tau_eta[k][l]), ar.tau_NNDecayMode[k][l])
                            get_muhad_weight = getFFAlt(ffhjvt2, ar.tau_pt[k][l]/1000, abs(ar.tau_eta[k][l]), ar.tau_NNDecayMode[k][l])

                            tempweight = get_ele_weight
                            tempweightmu = get_mu_weight
                            tempweightmj = get_mj_weight
                            tempweightmuhad = get_muhad_weight

                            #Check that fake factor weights are behaving as expected
                            if tempweight > 1 or tempweightmu > 1 or tempweightmj > 1 or tempweightmuhad > 1:
                                print("weights larger than one")

                            if l == tau_index:
                                ffweight = ffweight * tempweight
                                ffweightmu = ffweightmu * tempweightmu
                                ffweightmj = ffweightmj * tempweightmj
                                ffweightmuhad = ffweightmuhad * tempweightmuhad
                            
                            if l != tau_index:
                                ffweight = ffweight * (1 - tempweight)
                                ffweightmu = ffweightmu * (1 - tempweightmu)
                                ffweightmj = ffweightmj * (1 - tempweightmj)
                                ffweightmuhad = ffweightmuhad * (1 - tempweightmuhad)

                        #Done with l loop
                    #Done with tau counter loop
                    if bkg != "data" and ar.tau_isTruthMatchedTau[k][tau_index] == 0 and not((ar.met[k] > 300000) and (ar.ht[k] > 800000) and (ar.met[k]/ar.meff[k] > 0.2)):
                        continue

                    current_event = ar[k]
                    keep_fields = [field for field in ak.fields(current_event) if field != "mcEventWeights"] #Sometimes problematic, remove for now
                    current_event = current_event[keep_fields]
                    current_event['ff_weight_1'] = ffweight
                    current_event['ff_weight_2'] = ffweightmu
                    current_event['ff_weight_3'] = ffweightmj
                    current_event['ff_weight_4'] = ffweightmuhad
                    selected_events = ak.concatenate([selected_events, ak.Array([current_event])], axis=0)
            #End tau index loop
        #End with uproot open - k loop
    #End year loop
#End background loop

#Saving
with uproot.recreate(f"/disk/atlas3/users/thillers/fakeTaus/test.root") as f: 
    f["NOMINAL"] = dict(
        zip(ak.fields(selected_events), ak.unzip(selected_events))
    )   
