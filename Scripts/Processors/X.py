import numpy as np
import awkward as ak


def X(output, control_threshold, validation_threshold, signal_threshold):
    SELECTIONS = ["topquarks", "wtaunu", "ztautau", "znunu", "diboson", "other"]

    dfs_out = {}
    dicts_out = {}

    output_full = output["output"]["X_test"]
    output_bkg = output["output"]["X_test"][
        output["output"]["X_test"]["selection_name"].isin(SELECTIONS)
    ]

    # def get_full_region():
    control_mask_full = (output_full["output_score_signal"] >= control_threshold) & (
        output_full["output_score_signal"] < validation_threshold
    )

    validation_mask_full = (
        output_full["output_score_signal"] >= validation_threshold
    ) & (output_full["output_score_signal"] < signal_threshold)

    signal_mask_full = output_full["output_score_signal"] >= signal_threshold

    control_region_full = output["output"]["X_test"][control_mask_full]
    validation_region_full = output["output"]["X_test"][validation_mask_full]
    signal_region_full = output["output"]["X_test"][signal_mask_full]

    CR_backgrounds_dict = {}

    for idx, label in enumerate(output["labels"]["ML"][:-1]):
        filtered_df = control_region_full[control_region_full["y_true"] == idx]
        filtered_df = filtered_df.drop(columns=["selection_name"])
        array_form = filtered_df.to_records(index=False)
        CR_backgrounds_dict[label] = ak.from_numpy(array_form)

    VR_backgrounds_dict = {}

    for idx, label in enumerate(output["labels"]["ML"][:-1]):
        filtered_df = validation_region_full[validation_region_full["y_true"] == idx]
        filtered_df = filtered_df.drop(columns=["selection_name"])
        array_form = filtered_df.to_records(index=False)
        VR_backgrounds_dict[label] = ak.from_numpy(array_form)

    SR_backgrounds_dict = {}

    for idx, label in enumerate(output["labels"]["ML"][:-1]):
        filtered_df = signal_region_full[signal_region_full["y_true"] == idx]
        filtered_df = filtered_df.drop(columns=["selection_name"])
        array_form = filtered_df.to_records(index=False)
        SR_backgrounds_dict[label] = ak.from_numpy(array_form)

    dfs_out["CR"] = {"full": control_region_full}
    dfs_out["VR"] = {"full": validation_region_full}
    dfs_out["SR"] = {"full": signal_region_full}

    dicts_out["CR"] = {"full": CR_backgrounds_dict}
    dicts_out["VR"] = {"full": VR_backgrounds_dict}
    dicts_out["SR"] = {"full": SR_backgrounds_dict}

    def get_sub_region(region):
        dfs_sub_out = {}
        dicts_sub_out = {}

        for argmax_value, selection in enumerate(SELECTIONS):
            df_argmax = region[region["argmax"] == argmax_value]
            dfs_sub_out[selection] = df_argmax

            selection_dict = {}
            for idx, label in enumerate(output["labels"]["ML"][:-1]):
                sub_df = df_argmax[df_argmax["y_true"] == idx]
                sub_df = sub_df.drop(columns=["selection_name"])
                array_form = sub_df.to_records(index=False)
                selection_dict[label] = ak.from_numpy(array_form)

            dicts_sub_out[selection] = selection_dict

        return dfs_sub_out, dicts_sub_out

    control_mask_bkg = (output_bkg["output_score_signal"] >= control_threshold) & (
        output_bkg["output_score_signal"] < validation_threshold
    )

    validation_mask_bkg = (
        output_bkg["output_score_signal"] >= validation_threshold
    ) & (output_bkg["output_score_signal"] < signal_threshold)

    signal_mask_bkg = output_bkg["output_score_signal"] >= signal_threshold

    control_region_bkg = output_bkg[control_mask_bkg]
    validation_region_bkg = output_bkg[validation_mask_bkg]
    signal_region_bkg = output_bkg[signal_mask_bkg]

    score_columns = [f"output_score_class_{i}" for i in range(6)]
    control_region_bkg["argmax"] = (
        control_region_bkg[score_columns].idxmax(axis=1).str[-1].astype(int)
    )
    validation_region_bkg["argmax"] = (
        validation_region_bkg[score_columns].idxmax(axis=1).str[-1].astype(int)
    )
    signal_region_bkg["argmax"] = (
        signal_region_bkg[score_columns].idxmax(axis=1).str[-1].astype(int)
    )

    CR_all_dfs, CR_all_dicts = get_sub_region(control_region_bkg)
    VR_all_dfs, VR_all_dicts = get_sub_region(validation_region_bkg)
    SR_all_dfs, SR_all_dicts = get_sub_region(signal_region_bkg)

    dfs_out["CR"].update(CR_all_dfs)
    dfs_out["VR"].update(VR_all_dfs)
    dfs_out["SR"].update(SR_all_dfs)

    dicts_out["CR"].update(CR_all_dicts)
    dicts_out["VR"].update(VR_all_dicts)
    dicts_out["SR"].update(SR_all_dicts)

    return dfs_out, dicts_out
