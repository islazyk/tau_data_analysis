import os, sys
from pathlib import Path

from sklearn.model_selection import train_test_split


def train_test_splitter(algorithm_type, df, test_size):
    """
    Train-test splitting function. Parameters:
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * df: [dataframe] - (dependent),
        * test_size: [number] - (dependent).
    """

    cwd = os.getcwd()
    path_features = str(Path(cwd).parents[0]) + "/Features/"

    if algorithm_type == "regression":
        sys.path.append(path_features)
        from Regression import regression_features

        if ("n" in regression_features[0]) and ("divide" in regression_features[0]):
            y = (
                df[f"{regression_features[0]['name']}_{regression_features[0]['n']-1}"]
                / regression_features[0]["divide"]
            )
            X = df.drop(
                f"{regression_features[0]['name']}_{regression_features[0]['n']-1}",
                axis=1,
            )
        elif "n" in regression_features[0]:
            y = df[f"{regression_features[0]['name']}_{regression_features[0]['n']-1}"]
            X = df.drop(
                f"{regression_features[0]['name']}_{regression_features[0]['n']-1}",
                axis=1,
            )
        elif "divide" in regression_features[0]:
            y = df[regression_features[0]["name"]] / regression_features[0]["divide"]
            X = df.drop(regression_features[0]["name"], axis=1)
        else:
            y = df[regression_features[0]["name"]]
            X = df.drop(regression_features[0]["name"], axis=1)

    elif (algorithm_type == "binary-classification") or (
        algorithm_type == "multiclass-classification"
    ):
        y = df["class"]
        X = df.drop("class", axis=1)

    X_train, X_test, y_train, y_test = train_test_split(
        X,
        y,
        test_size=test_size,
        random_state=1,
    )

    return X, y, X_train, X_test, y_train, y_test

    # elif framework_type == 'PyTorch':
    #     import numpy as np
    #     import torch
    #     from torch.utils.data import Dataset, DataLoader, random_split

    #     from sklearn.model_selection import train_test_split

    #     X = torch.from_numpy(X).type(torch.float32)
    #     y = torch.from_numpy(y).type(torch.float32)

    #     X_train, X_test, y_train, y_test = train_test_split(
    #         X, y,
    #         test_size = test_size,
    #         random_state = 1,
    #     )

    #     # X = X.astype(np.float32)
    #     # y = y.astype(np.float32)

    #     # X_tensor = torch.tensor(X.values, dtype=torch.float32)
    #     # y_tensor = torch.tensor(y.values, dtype=torch.float32)

    #     # Xy_dataset = torch.utils.data.TensorDataset(X_tensor, y_tensor)

    #     # Xy_train_dataset, Xy_test_dataset = random_split(Xy_dataset, [1-test_size, test_size])

    #     # Xy_train_dataloader = DataLoader(Xy_train_dataset, batch_size=32, shuffle=True)
    #     # Xy_test_dataloader = DataLoader(Xy_test_dataset, batch_size=32, shuffle=True)

    #     # return X, y, Xy_train_dataloader, Xy_test_dataloader

    # else:
    #     raise Exception("Unsupported type of a framework. Only 'XGBoost' or 'PyTorch' allowed.")
