import awkward as ak
import numpy as np


def ntuple_flagger(
    include,
    data_type,
    ntuple_dict,
):
    """
    Padding rectangularizing function. Parameters:
        * ntuple_dict: [dict] - (dependent),
        * data_type: [string] - ('data', 'background', 'signal'),
    """

    if include:
        for x in ntuple_dict:
            zeroes = ak.Array([0] * len(ntuple_dict[x]))
            ones = ak.Array([1] * len(ntuple_dict[x]))

            if data_type == "data":
                ntuple_dict[x] = ak.with_field(ntuple_dict[x], ones, "isData")
                ntuple_dict[x] = ak.with_field(ntuple_dict[x], zeroes, "isBackground")
                ntuple_dict[x] = ak.with_field(ntuple_dict[x], zeroes, "isSignal")
            elif data_type == "background":
                ntuple_dict[x] = ak.with_field(ntuple_dict[x], zeroes, "isData")
                ntuple_dict[x] = ak.with_field(ntuple_dict[x], ones, "isBackground")
                ntuple_dict[x] = ak.with_field(ntuple_dict[x], zeroes, "isSignal")
            elif data_type == "signal":
                ntuple_dict[x] = ak.with_field(ntuple_dict[x], zeroes, "isData")
                ntuple_dict[x] = ak.with_field(ntuple_dict[x], zeroes, "isBackground")
                ntuple_dict[x] = ak.with_field(ntuple_dict[x], ones, "isSignal")

        return ntuple_dict

    else:
        for x in ntuple_dict:
            ntuple_dict[x] = None
        return ntuple_dict
