def output_adjuster(output):
    """
    Feature adjusting function. Parameters:
        * dicts_in: [dictionary] - (dependent),
    """

    output["output"]["X_test"]["selection_name"] = output["output"][
        "X_test_selection_name"
    ]
    output["output"]["X_test"]["weight"] = output["output"]["X_test_weights"]
    output["output"]["X_test"]["y_true"] = output["output"]["dict_results"]["y_test"]
    output["output"]["X_test"]["y_pred"] = output["output"]["dict_results"][
        "y_predictions"
    ]

    for i in range(output["output"]["dict_results"]["y_probabilities"].shape[1] - 1):
        col_name = f"output_score_class_{i}"
        output["output"]["X_test"][col_name] = output["output"]["dict_results"][
            "y_probabilities"
        ][:, i]
    output["output"]["X_test"]["output_score_signal"] = output["output"][
        "dict_results"
    ]["y_probabilities"][:, -1]
