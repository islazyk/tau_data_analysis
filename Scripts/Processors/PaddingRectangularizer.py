import awkward as ak
import numpy as np


def padding_rectangularizer(merged_array, padding_threshold, nan_threshold=None):
    """
    Padding rectangularizing function. Parameters:
        * merged_array: [array] - (dependent),
        * padding_threshold: [number] - (dependent),
        * nan_threshold: [number] - (dependent).
    """
    # ---Rectangularization---
    jagged_fetures = []
    for feature in ak.fields(merged_array):
        try:  # Attempting to flatten features - catches all variable length features
            ak.flatten(merged_array[str(feature)], axis=1)
            jagged_fetures.append(
                str(feature)
            )  # Fields which need to be flattened and padded
        except Exception:
            pass

    for feature in jagged_fetures:
        merged_array[feature] = ak.pad_none(  # Padding
            merged_array[feature], target=padding_threshold, clip=True
        )
        for i in range(padding_threshold):
            merged_array[f"{feature}_{i}"] = merged_array[feature][
                :, i
            ]  # Adding a new feature on the i'th element of var-length features
        merged_array = merged_array[
            [x for x in ak.fields(merged_array) if x != feature]
        ]  # Removing jagged arrays
    # ---Rectangularization---

    # ---Constant Features Removal---
    constant_features = []
    for feature in ak.fields(merged_array):  # Tracking constant features
        if (ak.sum(merged_array[feature]) == 0) or (np.var(merged_array[feature]) == 0):
            constant_features.append(feature)
    merged_array = merged_array[
        [x for x in ak.fields(merged_array) if x not in constant_features]
    ]
    # ---Constant Features Removal---

    df = ak.to_pandas(merged_array)  # Converting to Pandas
    df = df.dropna(
        axis=1, thresh=nan_threshold * df.shape[0]
    )  # Dropping features with NaN threshold

    return df
