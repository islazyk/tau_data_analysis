import numpy as np
import pyhf
from tqdm import tqdm
import os, sys


def significance_grid_constructor(SR_df, signal_threshold, bins):
    """
    Balance plotting function. Parameters:
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * df: [dataframe] - (dependent),
        * labels: [list] - (dependent).
    """

    bins = np.linspace(signal_threshold, 1, bins + 1)
    backgrounds = ["topquarks", "wtaunu", "ztautau", "diboson", "fake"]
    df_backgrounds = SR_df[SR_df["selection_name"].isin(backgrounds)]

    bkgYield = np.histogram(
        df_backgrounds.output_score_signal, weights=df_backgrounds.weight, bins=bins
    )[0]

    bkg = list(bkgYield)
    bkg_uncert = list(bkgYield * 0.3)

    GG_CLs, GG_mass1list, GG_mass2list = [], [], []
    SS_CLs, SS_mass1list, SS_mass2list = [], [], []

    for mass_point in tqdm(np.unique(SR_df["selection_name"])):
        if mass_point in backgrounds:
            continue

        SR_df_mass_point = SR_df[SR_df["selection_name"] == mass_point]

        signalYield = np.histogram(
            SR_df_mass_point.output_score_signal,
            weights=SR_df_mass_point.weight,
            bins=bins,
        )
        signal = list(signalYield[0])

        pyhf.set_backend("numpy")
        model = pyhf.simplemodels.uncorrelated_background(
            signal=signal, bkg=bkg, bkg_uncertainty=bkg_uncert
        )
        data = bkg + model.config.auxdata
        test_mu = 1.0
        try:
            original_stderr = sys.stderr
            sys.stderr = open(os.devnull, "w")

            pyhf.infer.hypotest(
                test_mu, data, model, test_stat="qtilde", return_expected=True
            )
            CLs_obs, CLs_exp = pyhf.infer.hypotest(
                test_mu, data, model, test_stat="qtilde", return_expected=True
            )

            sys.stderr.close()
            sys.stderr = original_stderr

        except Exception:
            CLS_obs, CLs_exp = 0, 0

        if "GG" in mass_point:
            GG_CLs.append(np.round(CLs_exp, 3))
            GG_mass1list.append(int(mass_point.split("_")[1]))
            GG_mass2list.append(int(mass_point.split("_")[2]))

        if "SS" in mass_point:
            SS_CLs.append(np.round(CLs_exp, 3))
            SS_mass1list.append(int(mass_point.split("_")[1]))
            SS_mass2list.append(int(mass_point.split("_")[2]))

    return GG_CLs, GG_mass1list, GG_mass2list, SS_CLs, SS_mass1list, SS_mass2list
