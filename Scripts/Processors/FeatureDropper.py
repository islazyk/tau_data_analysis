import numpy as np
import pandas as pd


def feature_dropper(investigate_feature, df=None, amount=None, feature=None):
    """
    Feature dropping function. Parameters:
        * investigate_feature: [boolean] - (True, False),
        * df: [dataframe] - (dependent),
        * amount: [string] - ('single', 'all'),
        * feature: [string] - (dependent).
    """
    if investigate_feature:
        if amount == "single":
            df.drop(feature, axis=1, inplace=True)
            # df = df.loc[:, df.columns != feature]     # 2nd method
        elif amount == "all":
            df.drop(df.columns.difference([feature, "class"]), axis=1, inplace=True)
        else:
            raise Exception(
                "Unsupported type of a drop. Only 'single' or 'all' allowed."
            )
    else:
        print("No dropping performed.")
