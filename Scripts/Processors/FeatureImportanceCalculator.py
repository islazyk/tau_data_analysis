import shap
import numpy as np
import pandas as pd


def feature_importance_calculator(
    framework_type, algorithm_type, model, X_train, X_test, columns, events
):
    if framework_type == "XGBoost":
        explainer = shap.TreeExplainer(model)
        shap_values = explainer.shap_values(X_test[0:events], check_additivity=False)
    elif framework_type == "PyTorch":
        explainer = shap.DeepExplainer(model, X_train[0:events])
        shap_values = explainer.shap_values(X_test[0:events])
    else:
        raise Exception(
            "Unsupported type of a framework. Only 'XGBoost or 'PyTorch' allowed."
        )

    if (algorithm_type == "regression") or (algorithm_type == "binary-classification"):
        df_feature_importance_scores = pd.DataFrame(
            {
                "name": columns,
                "mean_abs_shap": np.mean(np.abs(shap_values), axis=0),
                "stdev_abs_shap": np.std(np.abs(shap_values), axis=0),
            }
        )
    elif algorithm_type == "multiclass-classification":
        df_feature_importance_scores = pd.DataFrame(
            {
                "name": columns,
                "mean_abs_shap": np.mean(np.mean(np.abs(shap_values), axis=0), axis=0),
                "stdev_abs_shap": np.std(np.abs(shap_values), axis=(0, 1)),
            }
        )
    else:
        raise Exception(
            "Unsupported type of an algorithm. Only 'regression', 'binary-classification' or 'multiclass-classification' allowed."
        )

    return explainer, shap_values, df_feature_importance_scores
