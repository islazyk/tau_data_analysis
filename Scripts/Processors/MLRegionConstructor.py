import numpy as np
import awkward as ak


def ml_region_constructor(
    output, control_threshold, validation_threshold, signal_threshold, signals
):
    regions = ["CR", "VR", "SR"]
    backgrounds = ["topquarks", "wtaunu", "ztautau", "diboson", "fake"]

    output_full = output["output"]["X_test"]
    output_bkg = output_full[output_full["selection_name"].isin(backgrounds)]
    output_sig = output_full[output_full["selection_name"].isin(signals)]

    dfs_full_out = {}
    dicts_bkg_out = {}
    dicts_sig_out = {}

    regions_full = split_into_regions(
        output_full, control_threshold, validation_threshold, signal_threshold
    )
    regions_bkg = split_into_regions(
        output_bkg, control_threshold, validation_threshold, signal_threshold
    )
    regions_sig = split_into_regions(
        output_sig, control_threshold, validation_threshold, signal_threshold
    )

    for region in regions:
        dfs_full_out[region] = {"full": regions_full[region]}
        dfs_full_out[region].update(
            split_by_selection(regions_bkg[region], backgrounds)
        )

        dicts_bkg_out[region] = {
            "full": convert_to_dict(
                regions_full[region], output["labels"][:-1], "background"
            )
        }
        dicts_bkg_out[region].update(
            convert_to_dict_by_selection(
                regions_bkg[region],
                output["labels"][:-1],
                backgrounds,
                "background",
            )
        )

        dicts_sig_out[region] = {
            "full": convert_to_dict(regions_full[region], signals, "signal")
        }
        dicts_sig_out[region].update(
            convert_to_dict_by_selection(
                regions_sig[region], signals, backgrounds, "signal"
            )
        )

    return dfs_full_out, dicts_bkg_out, dicts_sig_out


def split_into_regions(df, control_threshold, validation_threshold, signal_threshold):
    masks = {
        "CR": (df["output_score_signal"] >= control_threshold)
        & (df["output_score_signal"] < validation_threshold),
        "VR": (df["output_score_signal"] >= validation_threshold)
        & (df["output_score_signal"] < signal_threshold),
        "SR": df["output_score_signal"] >= signal_threshold,
    }
    return {region: df[mask] for region, mask in masks.items()}


def split_by_selection(df, backgrounds):
    score_columns = [f"output_score_class_{i}" for i in range(len(backgrounds))]
    df["argmax"] = df[score_columns].idxmax(axis=1).str[-1].astype(int)
    return {
        selection: df[df["argmax"] == idx] for idx, selection in enumerate(backgrounds)
    }


def convert_to_dict(df, labels, data_type):
    data_dict = {}
    if data_type == "background":
        for idx, label in enumerate(labels):
            filtered_df = df[df["y_true"] == idx]
            array_form = filtered_df.drop(columns=["selection_name"]).to_records(
                index=False
            )
            data_dict[label] = ak.from_numpy(array_form)
        return data_dict
    elif data_type == "signal":
        for label in labels:
            filtered_df = df[df["selection_name"] == label]
            array_form = filtered_df.drop(columns=["selection_name"]).to_records(
                index=False
            )
            data_dict[label] = ak.from_numpy(array_form)
        return data_dict
    else:
        pass


def convert_to_dict_by_selection(df, labels, backgrounds, data_type):
    score_columns = [f"output_score_class_{i}" for i in range(len(backgrounds))]
    df["argmax"] = df[score_columns].idxmax(axis=1).str[-1].astype(int)

    selection_dict = {}
    for idx, selection in enumerate(backgrounds):
        sub_df = df[df["argmax"] == idx]
        selection_dict[selection] = convert_to_dict(sub_df, labels, data_type)
    return selection_dict
