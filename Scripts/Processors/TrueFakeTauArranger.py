import awkward as ak


def true_fake_tau_arranger(
    include_truefaketaus=False,
    backgrounds_dict=None,
):
    """
    Analysis processing function. Parameters:
        * analysis_base: [string] - (dependent),
    """

    if not include_truefaketaus:
        print("True fake taus not arranged.")
        return None
    else:
        data_truefaketaus = {}

        for x in backgrounds_dict:
            data_truefaketaus[f"{x}-true"] = backgrounds_dict[x][
                ak.all(backgrounds_dict[x].tau_isTruthMatchedTau == 1, axis=1)
            ]
            data_truefaketaus[f"{x}-fake"] = backgrounds_dict[x][
                ak.all(backgrounds_dict[x].tau_isTruthMatchedTau == 0, axis=1)
            ]

        print("True fake taus arranged.")
        return data_truefaketaus
