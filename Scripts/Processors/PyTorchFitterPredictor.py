import torch


def pytorch_fitter_predictor(
    algorithm_type,
    pytorch_model,
    n_epochs,
    optimizer,
    criterion,
    X_train_tensor,
    y_train_tensor,
    X_test_tensor,
    y_test_tensor,
    early_stopping_rounds,
):
    """
    Normalization applying function. Parameters:
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * X_train: [dataframe] - (dependent),
        * y_train: [series] - (dependent),
        * X_test: [dataframe] - (dependent),
        * y_test: [series] - (dependent).
    """

    training_loss_history = []
    validation_loss_history = []
    best_val_loss = float("inf")
    counter = 0
    best_epoch = 0
    lowest_loss_epoch = 0

    for epoch in range(n_epochs):
        # ---Training---
        pytorch_model.train()  # Set the network to training mode
        optimizer.zero_grad()  # clear the gradients of all optimized variables / # Zero the gradients

        y_pred_train = pytorch_model(X_train_tensor)  # Forward pass

        if algorithm_type == "binary-classification":
            loss_training = criterion(
                y_pred_train, y_train_tensor.unsqueeze(1)
            )  # Compute the loss
        elif algorithm_type == "multiclass-classification":
            loss_training = criterion(y_pred_train, y_train_tensor)  # Compute the loss

        loss_training.backward()  # Backward pass: compute gradient of the loss with respect to model parameters

        optimizer.step()  # perform a single optimization step (parameter update) / # Update the weights
        training_loss_history.append(loss_training.item())  # record training loss

        # ---Validating---
        pytorch_model.eval()  # Set the network to evaluation mode
        # with torch.no_grad():                             # Disable gradient computation

        y_pred_val = pytorch_model(X_test_tensor)  # Forward pass on validation set

        if algorithm_type == "binary-classification":
            loss_validation = criterion(
                y_pred_val, y_test_tensor.unsqueeze(1)
            )  # Compute the validation loss
        elif algorithm_type == "multiclass-classification":
            loss_validation = criterion(
                y_pred_val, y_test_tensor
            )  # Compute the validation loss

        validation_loss_history.append(
            loss_validation.item()
        )  # Record the validation loss

        # Check for early stopping
        if loss_validation < best_val_loss:
            best_val_loss = loss_validation
            lowest_loss_epoch = epoch
            counter = 0
            torch.save(
                pytorch_model.state_dict(),
                "../Pickles/ML/24.2.7/R22/Preselection/1_tau/model_pytorch.pt",
            )
        else:
            counter += 1

        if epoch % 10 == 0:
            print(
                f"Epoch {epoch+1} -- Training Loss: {training_loss_history[-1]:.4f} -- Validation Loss: {validation_loss_history[-1]:.4f}"
            )

        if counter >= early_stopping_rounds:
            print(f"Early stopping after {lowest_loss_epoch+1} epochs")
            break

    best_epoch = lowest_loss_epoch - early_stopping_rounds
    return y_pred_val, training_loss_history, validation_loss_history, best_epoch
