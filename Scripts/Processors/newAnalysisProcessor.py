#Process the raw input MC into something sensible by merging, and splitting into channels
#This script only applies a preselection cut
import uproot as ur
import awkward as ak
import numpy as np
import os

def new_analysis_processor(
    run,
    channel,
    load_features,
    mc_type, #backgrounds, data, fake, signal
    mc_fakes,
):

    data_out = {}

    if run == "Run2":
        campaigns = ["MC20a", "MC20d", "MC20e"]
    elif run == "Run3":
        campaigns = ["MC23a", "MC23d"]
    else:
        raise Exception("chill and wait for Run4")
    
    bkg_types = ["diboson", "singletop", "ttbar", "wenu", "wmunu", "wtaunu", "zee", "zmumu", "znunu", "ztautau", "ttX"]

    if mc_type == "data":
        mc_inputs = ["data"]
    elif mc_type in bkg_types:
        mc_inputs = [mc_type] #convert to a list so we can loop
    elif mc_type == "fake":
        mc_inputs = ["diboson", "singletop", "ttbar", "wenu", "wmunu", "wtaunu", "zee", "zmumu", "znunu", "ztautau"] #ttX not in anti-ID fakes atm
    elif mc_type == "signal":
        mc_inputs = os.listdir("/disk/atlas1/data_MC/25.2.8/PHYS/MC23d/vector_XEplateau/")
        mc_inputs = [x for x in mc_inputs if "_" in x]
    else:
        raise Exception("unsupported mc type")

    
    for mc_input in mc_inputs:
        print(f"processing {mc_input}")
        for idx, campaign in enumerate(campaigns):

            if mc_type in bkg_types:
                path = f"/disk/atlas1/data_MC/25.2.8/PHYS/{campaign}/vector_XEplateau/{mc_input}.root"
            elif mc_type == "data":
                path = f"/disk/atlas1/data_MC/25.2.8/PHYS/{campaign}/vector_XEplateau/{mc_input}.root"
            elif mc_type == "signal":
                path = f"/disk/atlas1/data_MC/25.2.8/PHYS/{campaign}/vector_XEplateau/{mc_input}" #.root suffix already here
            elif mc_type == "fake":
                path = f"/disk/atlas1/users/thillers/fakes/new_post_summer/{campaign}/{mc_input}_faketau.root"
            else:
                raise Exception("unsupported mc type")


            with ur.open(path) as temp_file:
                tree = temp_file["NOMINAL"]
                if "2had" in channel:
                    ar = tree.arrays(load_features, library = "ak", cut = "(tau_n >= 2) & (jet_n >= 2) & (met > 200000) & (IsMETTrigPassed != 0) & \
                        (eventClean != 0) & (isBadTile == 0) & (jet_isBadTight[:,0] != True) & (jet_pt[:,0] > 120000) & (jet_pt[:,1] > 20000) & \
                            (jet_delPhiMet[:,0] >= 0.4) & (jet_delPhiMet[:,1] >= 0.4)")
                else:
                    ar = tree.arrays(load_features, library = "ak", cut = "(tau_n == 1) & (jet_n >= 2) & (met > 200000) & (IsMETTrigPassed != 0) & \
                        (eventClean != 0) & (isBadTile == 0) & (jet_isBadTight[:,0] != True) & (jet_pt[:,0] > 120000) & (jet_pt[:,1] > 20000) & \
                            (jet_delPhiMet[:,0] >= 0.4) & (jet_delPhiMet[:,1] >= 0.4)")

            #Common cuts are now redundant as we can specify these directly when converting the files to awkward arrays
            #But keeping them here for readability
            #Cleaning
            ar = ar[ar.jet_n >= 2]
            ar = ar[ar.tau_n != 0]
            ar = ar[ar.eventClean != 0]
            ar = ar[ar.isBadTile == 0]
            ar = ar[ar.jet_isBadTight[:, 0] != True]
            ar = ar[ar.IsMETTrigPassed != 0]  
            #Kinematics
            ar = ar[ar.met / 1000 >= 200]
            ar = ar[ar.jet_pt[:, 0] / 1000 >= 120]
            ar = ar[ar.jet_pt[:, 1] / 1000 >= 20]
            ar = ar[ar.jet_delPhiMet[:, 0] >= 0.4]
            ar = ar[ar.jet_delPhiMet[:, 1] >= 0.4]

            #Channel specifics:
            if channel == "1had0lep":
                ar = ar[ar.tau_n == 1]
                ar = ar[ar.ele_n == 0]
                ar = ar[ar.mu_n == 0]

                if mc_type == "fake":
                    ar = ar[ar.tau_isTruthMatchedTau[:, 0] == 0] #anti-ID
                    ar = ar[ar.tau_muonOLReta01[:, 0] == 1] #muon-tau OLR for fakes
                elif mc_fakes:
                    ar = ar[ar.tau_isTruthMatchedTau[:, 0] == 0] #anti-ID
                    ar = ar[ar.tau_JetRNNMedium[:, 0] != 0] #pass medium ID 
                else:
                    ar = ar[ar.tau_JetRNNMedium[:, 0] != 0]
                    if "data" not in mc_type:
                        ar = ar[ar.tau_isTruthMatchedTau[:, 0] == 1] #real taus for mc backgrounds

            elif channel == "1had1lep":
                ar = ar[ar.tau_n == 1]
                ar = ar[(ar.mu_n > 0) | (ar.ele_n > 0)]

                if mc_type == "fake":
                    ar = ar[ar.tau_isTruthMatchedTau[:, 0] == 0]
                    ar = ar[ar.tau_muonOLReta01[:, 0] == 1]
                elif mc_fakes:
                    ar = ar[ar.tau_isTruthMatchedTau[:, 0] == 0] #anti-ID
                    ar = ar[ar.tau_JetRNNMedium[:, 0] != 0] #pass medium ID 
                else:
                    ar = ar[ar.tau_JetRNNMedium[:, 0] != 0]
                    if "data" not in mc_type:
                        ar = ar[ar.tau_isTruthMatchedTau[:, 0] == 1]

            elif channel == "2had":
                ar = ar[ar.tau_n >= 2]

                if mc_type == "fake":
                    ar = ar[(ar.tau_isTruthMatchedTau[:, 0] == 0) | (ar.tau_isTruthMatchedTau[:, 1] == 0)]
                    ar = ar[ar.tau_muonOLReta01[:, 0] == 1]
                    ar = ar[ar.tau_muonOLReta01[:, 1] == 1]
                elif mc_fakes:
                    ar = ar[(ar.tau_isTruthMatchedTau[:, 0] == 0) | (ar.tau_isTruthMatchedTau[:, 1] == 0)]
                    ar = ar[ar.tau_JetRNNMedium[:, 0] != 0] #pass medium ID 
                    ar = ar[ar.tau_JetRNNMedium[:, 1] != 0] #pass medium ID 
                else:
                    ar = ar[ar.tau_JetRNNMedium[:, 0] != 0]
                    ar = ar[ar.tau_JetRNNMedium[:, 1] != 0]
                    if "data" not in mc_type:
                        ar = ar[ar.tau_isTruthMatchedTau[:, 0] == 1]
                        ar = ar[ar.tau_isTruthMatchedTau[:, 1] == 1]

            else:
                raise Exception("get a new channel subscription")

            #Add signal origin
            if ("GG" in mc_input) or ("SS" in mc_input):
                ar["signalOrigin"] = mc_input.rstrip(".root")
            else:
                ar["signalOrigin"] = "-999"

            if idx == 0:
                data_out[mc_input] = ar
            else:
                data_out[mc_input] = ak.concatenate([data_out[mc_input], ar], axis = 0)

    print(f"successfully processed {mc_type}")
    return data_out
