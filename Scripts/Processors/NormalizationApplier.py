import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler


def normalization_applier(
    framework_type,
    algorithm_type,
    AMP=False,
    X_train=None,
    y_train=None,
    X_test=None,
    y_test=None,
):
    """
    Normalization applying function. Parameters:
        * framework_type: [string] - ('XGBoost', 'PyTorch').
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * X_train: [dataframe] - (dependent),
        * y_train: [series] - (dependent),
        * X_test: [dataframe] - (dependent),
        * y_test: [series] - (dependent).
    """

    if framework_type == "XGBoost":
        return print("No normalization performed.")
    elif framework_type == "PyTorch":
        import torch

        if AMP:
            float_type = torch.float16
        else:
            float_type = torch.float32

        X_train_values = X_train.astype(np.float32).values
        y_train_values = y_train.astype(np.float32).values
        X_test_values = X_test.astype(np.float32).values
        y_test_values = y_test.astype(np.float32).values

        scaler = MinMaxScaler()

        X_train_values = scaler.fit_transform(X_train_values)
        X_test_values = scaler.transform(X_test_values)

        if torch.cuda.is_available():
            device = torch.device("cuda")
            torch.cuda.empty_cache()
            print("GPU enabled.")
        else:
            device = torch.device("cpu")
            print("CPU enabled.")

        if (algorithm_type == "regression") or (
            algorithm_type == "binary-classification"
        ):
            X_train_tensor = torch.tensor(X_train_values, dtype=float_type).to(device)
            y_train_tensor = torch.tensor(y_train_values, dtype=float_type).to(device)
            X_test_tensor = torch.tensor(X_test_values, dtype=float_type).to(device)
            y_test_tensor = torch.tensor(y_test_values, dtype=float_type).to(device)
        elif algorithm_type == "multiclass-classification":
            X_train_tensor = torch.tensor(X_train_values, dtype=float_type).to(device)
            y_train_tensor = torch.tensor(y_train_values, dtype=float_type)
            y_train_tensor = y_train_tensor.type(torch.LongTensor).to(device)
            X_test_tensor = torch.tensor(X_test_values, dtype=float_type).to(device)
            y_test_tensor = torch.tensor(y_test_values, dtype=float_type)
            y_test_tensor = y_test_tensor.type(torch.LongTensor).to(device)
        else:
            raise Exception(
                "Unsupported type of an algorithm. Only 'regression', 'binary-classification' or 'multiclass-classification' allowed."
            )

        print("Normalization performed.")
        return X_train_tensor, y_train_tensor, X_test_tensor, y_test_tensor, device

    else:
        raise Exception(
            "Unsupported type of a framework. Only 'XGBoost' or 'PyTorch' allowed."
        )
