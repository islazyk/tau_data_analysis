import awkward as ak
import numpy as np

def new_mc_merger(
    mc_type,
    indict,
):
    outdict = {}

    if mc_type == "background":
        outdict["topquarks"] = ak.concatenate([indict["ttbar"],indict["singletop"]], axis = 0)
        outdict["wtaunu"] = indict["wtaunu"]
        outdict["ztautau"] = indict["ztautau"]
        outdict["diboson"] = indict["diboson"]
        outdict["other"] = ak.concatenate([indict["znunu"], indict["wmunu"], indict["wenu"], indict["zmumu"], indict["zee"], indict["ttX"]], axis = 0)

    elif mc_type == "fake":
        outdict["fake"] = ak.concatenate([indict[x] for x in indict], axis = 0)

    elif mc_type == "fake_pass_mediumID":
        outdict["fake_pass_mediumID"] = ak.concatenate([indict[x] for x in indict], axis = 0)

    elif mc_type == "signal":
        outdict["signal"] = ak.concatenate([indict[x] for x in indict], axis = 0)

    else:
        raise Exception("Error in mc_type")

    print(f"finished merging {mc_type}")

    return outdict


