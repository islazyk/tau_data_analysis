import awkward as ak


def feature_adjuster(dicts_in, common_features):
    """
    Feature adjusting function. Parameters:
        * dicts_in: [dictionary] - (dependent),
        * common_features: [set] - (dependent).
    """

    for dict_in in dicts_in:
        dicts_in[dict_in] = dicts_in[dict_in][
            [x for x in dicts_in[dict_in].fields if x in common_features]
        ]

    for dict_in in dicts_in:
        dicts_in[dict_in]["Mt2"] = [
            -999 if x == -1000 else x for x in dicts_in[dict_in]["Mt2"]
        ]
        dicts_in[dict_in]["Mt2_taumu"] = [
            -999 if x == -1000 else x for x in dicts_in[dict_in]["Mt2_taumu"]
        ]
        dicts_in[dict_in]["Mt2_tauel"] = [
            -999 if x == -1000 else x for x in dicts_in[dict_in]["Mt2_tauel"]
        ]

        dicts_in[dict_in] = dicts_in[dict_in][
            [
                x
                for x in dicts_in[dict_in].fields
                if x
                not in [
                    "tau_JetRNNMedium",
                ]
            ]
        ]
