import awkward as ak 
import numpy as np
import pandas as pd 

target_length = 3

vector_systematics = [
    "mcEventWeights_alphas",
    "mcEventWeights_fsr",
    "mcEventWeights_scale", #ignore scale for now 
    #"mcEventWeights_pdf", #ignore pdf systematics for now
]

mc_info = [
    "bkgOrigin",
    "fakeOrigin",
    "signalOrigin",
]

def flatten_and_pad(ar, systs = False):

    vector_variables = []
    for variable in ak.fields(ar):
        if (variable in vector_systematics) or (variable in mc_info):
            continue
        try:
            ak.flatten(ar[str(variable)], axis = 1)
            vector_variables.append(str(variable))
        except Exception: pass

    for variable in vector_variables:
        ar[variable] = ak.pad_none(ar[variable], target = target_length, clip = True)
        for particle_idx in range(target_length):
            ar[f"{variable}_{particle_idx}"] = ar[variable][:, particle_idx]
        ar = ar[[x for x in ak.fields(ar) if x != variable]]
    
    if systs:
        syst_dict = {}
        for syst in vector_systematics:
            #syst_dict[syst] = [np.array(x) for x in ar[syst]]
            syst_dict[syst] = [x.tolist() if isinstance(x, ak.Array) else list(x) for x in ar[syst]] #this seems to work

        ar = ar[[x for x in ak.fields(ar) if x not in vector_systematics]]
        df = ak.to_dataframe(ar)

        df_syst = pd.DataFrame(index = df.index)
        for syst, entries in syst_dict.items():
            df_syst[syst] = entries

        df = pd.concat([df, df_syst], axis = 1)

    else:
        ar = ar[[x for x in ak.fields(ar) if x not in vector_systematics]]
        df = ak.to_dataframe(ar)

    df.fillna(0, inplace = True)

    return(df)




