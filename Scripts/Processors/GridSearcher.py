import xgboost as xgb


def grid_searcher(
    algorithm_type,
    kind,
    n_folds,
    parameters_grid,
    X_train,
    y_train,
    X_test,
    y_test,
    metric,
):
    """
    Grid searching function. Parameters:
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * kind: [string] - ('randomized', 'exhaustive'),
        * n_folds: [number] - (dependent),
        * parameters_grid: [dictionary] - (dependent),
        * X_train: [dataframe] - (dependent),
        * y_train: [series] - (dependent),
        * X_test: [dataframe] - (dependent),
        * y_test: [series] - (dependent),
        * metric: [string] - (dependent).
    """

    model_empty = xgb.XGBClassifier(tree_method="gpu_hist", gpu_id=0, random_state=1)

    scoring = "accuracy"

    fit_paramseters_cv = {
        "eval_set": [(X_train, y_train), (X_test, y_test)],
        "eval_metric": metric,
        "early_stopping_rounds": 100,
        "verbose": 0,
    }

    if algorithm_type == "regression":
        print("Not available for regression.")

    elif (algorithm_type == "binary-classification") or (
        algorithm_type == "multiclass-classification"
    ):
        if kind == "randomized":
            from sklearn.model_selection import RandomizedSearchCV

            model_grid_randomized = RandomizedSearchCV(
                estimator=model_empty,
                param_distributions=parameters_grid,
                cv=n_folds,
                scoring=scoring,
                random_state=1
                # n_jobs = -1,
            )

            model_grid_randomized_best = model_grid_randomized.fit(
                X_train, y_train, **fit_paramseters_cv
            )

            print(model_grid_randomized_best.best_params_)

        elif kind == "exhaustive":
            from sklearn.model_selection import GridSearchCV

            model_grid_full = GridSearchCV(
                estimator=model_empty,
                param_grid=parameters_grid,
                cv=n_folds,
                scoring=scoring,
                random_state=1
                # n_jobs = -1,
            )

            model_grid_full_best = model_grid_full.fit(
                X_train, y_train, **fit_paramseters_cv
            )

            print(model_grid_full_best.best_params_)

        else:
            raise Exception(
                "Unsupported type of a grid. Only 'randomized' or 'exhaustive' allowed."
            )

    else:
        raise Exception(
            "Unsupported type of an algorithm. Only 'regression', 'binary-classification' or 'multiclass-classification' allowed."
        )
