import awkward as ak
import pandas as pd


def padding_changer(change_padding, df=None, padding_value=None):
    """
    Padding changing function. Parameters:
        * change_padding: [boolean] - (True, False),
        * df: [dataframe] - (dependent),
        * padding_value: [string] - ('NaN', '0', '-999', 'mean').
    """
    if change_padding:
        if padding_value == "NaN":
            print("Already padded with NaN.")
        elif padding_value == "0":
            df.fillna(0, inplace=True)
            print("Padded with 0.")
        elif padding_value == "-999":
            df.fillna(-999, inplace=True)
            print("Padded with -999.")
        elif padding_value == "mean":
            [
                df[column].fillna(df[column].mean(), inplace=True)
                for column in df.columns
            ]
            print("Padded with mean value.")
            # for column in df.columns:
            #     df[column].fillna(df[column].mean(), inplace = True)
        else:
            raise Exception(
                "Unsupported type of a padding. Only 'NaN', '0', '-999', 'mean' allowed."
            )
    else:
        print("No padding performed.")
