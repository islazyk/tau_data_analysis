import torch
from torch.cuda.amp import autocast, GradScaler


def pytorch_fitter_predictor(
    algorithm_type,
    pytorch_model,
    n_epochs,
    optimizer,
    criterion,
    X_train_tensor,
    y_train_tensor,
    X_test_tensor,
    y_test_tensor,
    early_stopping_rounds,
    sample_weight=None
):
    # Initialize AMP scaler and context
    scaler = GradScaler()

    training_loss_history = []
    validation_loss_history = []
    best_val_loss = float("inf")
    counter = 0
    best_epoch = 0
    lowest_loss_epoch = 0

    for epoch in range(n_epochs):
        # ---Training---
        pytorch_model.train()
        optimizer.zero_grad()

        with autocast():  # Enable automatic mixed precision
            y_pred_train = pytorch_model(X_train_tensor)
            if algorithm_type == "binary-classification":
                loss_training = criterion(y_pred_train, y_train_tensor.unsqueeze(1))
            elif algorithm_type == "multiclass-classification":
                loss_training = criterion(y_pred_train, y_train_tensor)

            scaler.scale(loss_training).backward()
            scaler.step(optimizer)
            scaler.update()

        training_loss_history.append(loss_training.item())

        # ---Validating---
        pytorch_model.eval()

        with torch.no_grad(), autocast():
            y_pred_val = pytorch_model(X_test_tensor)
            if algorithm_type == "binary-classification":
                loss_validation = criterion(y_pred_val, y_test_tensor.unsqueeze(1))
            elif algorithm_type == "multiclass-classification":
                loss_validation = criterion(y_pred_val, y_test_tensor)

        validation_loss_history.append(loss_validation.item())

        # Check for early stopping
        if loss_validation < best_val_loss:
            best_val_loss = loss_validation
            lowest_loss_epoch = epoch
            counter = 0
            torch.save(
                pytorch_model.state_dict(),
                "../Pickles/ML/24.2.28/Run2/Preselection/1_tau/model_pytorch.pt",
            )
        else:
            counter += 1

        if epoch % 10 == 0:
            print(
                f"Epoch {epoch+1} -- Training Loss: {training_loss_history[-1]:.4f} -- Validation Loss: {validation_loss_history[-1]:.4f}"
            )

        if counter >= early_stopping_rounds:
            print(f"Early stopping after {lowest_loss_epoch+1} epochs")
            break

    best_epoch = lowest_loss_epoch - early_stopping_rounds
    return y_pred_val, training_loss_history, validation_loss_history, best_epoch
