import uproot as ur
import awkward as ak
import numpy as np
from tqdm import tqdm

'''
todo list:
-------------------
- Implement VR cuts
- Add Multijet CR
- Add a toggle to change delPhiMet if Znunu CR is chosen
- Add a toggle which enables Nikolai mode
'''

def analysis_processor(
    analysis_base,
    campaigns,
    region,
    channel,
    subject,
    sub_subject,
    load_features,
    data_type,
    data_sets,
):
    """
    Analysis processing function. Parameters:
        * analysis_base: [string] - (dependent),
        * campaigns: [list] - (dependent),
        * region: [string] - ('Preselection', 'SR', 'CR', 'VR'),
        * channel: [string] - ('0' ,'1had0lep', '1had1lep', '1hadInclusive', '2tau'),
        * sub_subject: [string] - (dependent),
        * load_data_background: [list] (dependent),
        * load_signal: [list] (dependent),
        * data_type: [string] ('data', 'background', 'signal', 'SUSY2016'),
        * data_sets: [list] (dependent),
        * load_signal: [string] ('gluinos', 'squarks'),
        * luminosity: [number] (dependent),
        * decoupling_signal: [dictionary] (dependent),
        * xsec_signal: [dictionary] (dependent).
    """

    if channel == "2tau":
        ntau = "2"
    elif channel == "1had0lep" or channel == "1had1lep" or channel == "1hadInclusive":
        ntau = "1"
    elif channel == "0":
        ntau = "0"
    else:
        print("Kurwa bobr")

    if data_sets == None:
        return print(f"The {data_type} has not been processed.")

    print(
        f"Applying cuts for the following: region: {region} | channel: {channel} | subject: {subject} | sub_subject: {sub_subject}."
    )

    # ---Data Containers---
    data_out = {}
    # ---Data Containers---

    # ---Loop Over Provided Data Sets---
    for i, data_set in tqdm(enumerate(data_sets), total=len(data_sets)):
        for j, campaign in enumerate(campaigns):
            print(f"considering {campaign }{data_set}")
            # ---File Opening---
            #We have different input paths for the analysis: nominal ntuples, fake taus and signal samples
            if (data_type == "data") or (data_type == "background"):

                if (data_type == "data") and ("MC23" in campaign): #Special treatment for Run3 data, select the reproccessed part 
                    path = f"/disk/atlas3/data_MC/{analysis_base}/PHYS/{campaign}/vector_XEplateau/{data_set}_repro.root"
                else:
                    path = f"/disk/atlas3/data_MC/{analysis_base}/PHYS/{campaign}/vector_XEplateau/{data_set}.root"
                    
                print(path)
            elif data_type == "fake":
                path = f"/disk/atlas3/users/thillers/fakeTaus/R24.2.28/{campaign}/{data_set}.root"
            elif data_type == "signal":
                path = f"/disk/atlas3/data_MC/{analysis_base}/PHYS/{campaign}/vector_XEplateau/{data_set}.root"
            else:
                raise Exception(
                    "Unsupported data type. Only 'data', 'background' or 'signal'  allowed."
                )

            with ur.open(path) as temp_file:
                tree = temp_file["NOMINAL"]
                ar = tree.arrays(load_features, library="ak")
            # ---File Opening---

            # ---Fixes---
            ar = ar[ar.tau_n != 0]  # Dropping tau empty lists
            ar = ar[ar.jet_n != 0]  # Dropping jet empty lists
            # ---Fixes---

            # ---Channel---
            if region != "CR":
                if ntau == "0":
                    ar = ar[ar.tau_n == 0]
                    ar = ar[ar.mu_n == 1]
                elif ntau == "1":
                    ar = ar[ar.tau_n == 1]
                elif ntau == "2":
                    ar = ar[ar.tau_n >= 2]
                else:
                    raise Exception("Unsupported channel.")

            #Define the 1 exclusive channels based on a cut on the leptonVeto
            if channel == "1had0lep":
                ar = ar[ar.LeptonVeto == 1]
            elif channel == "1had1lep":
                ar = ar[ar.LeptonVeto == 0]
            else:
                pass
            

            # ---Cleaning Cuts---
            ar = ar[ar.eventClean != 0]
            ar = ar[ar.isBadTile == 0]
            ar = ar[ar.jet_isBadTight[:, 0] != True]
            ar = ar[ar.IsMETTrigPassed != 0]  # Disable for smearing

            if data_type != "fake":
                if ntau == "0":
                    pass
                elif ntau == "1":
                    ar = ar[ar.tau_n == 1]
                    ar = ar[ar.tau_JetRNNMedium[:, 0] != 0]
                elif ntau == "2":
                    ar = ar[ar.tau_n >= 2]
                    ar = ar[ar.tau_JetRNNMedium[:, 0] != 0]
                    ar = ar[ar.tau_JetRNNMedium[:, 1] != 0]
                else:
                    print("kurwa bobr")
        
            # ---Cleaning Cuts---

            # ---Background Tau Truth Matching Cuts---
            if ntau == "1":
                if data_type == "fake":
                    ar = ar[(ar.tau_isTruthMatchedTau[:, 0] == 0) | (ar.tau_truthType[:, 0] != 10)]

                elif data_type == "background":
                    ar = ar[ar.tau_isTruthMatchedTau[:, 0] == 1]
                    #MC23 diboson fix
                    if "diboson" in data_set and "MC23" in campaign:
                        print("Encountered dibosons in MC23, skipping truth type")
                        pass
                    else:
                        ar = ar[ar.tau_truthType[:, 0] == 10]
                else:
                    pass

            elif ntau == "2":
                ar = ar[ar.tau_n >= 2]
                if data_type == "fake":
                    ar = ar[(ar.tau_isTruthMatchedTau[:, 0] == 0) | (ar.tau_truthType[:, 0] != 10) | (ar.tau_isTruthMatchedTau[:, 1] == 0) | (ar.tau_truthType[:, 1] != 10)]

                elif data_type == "background":
                    
                    ar = ar[ar.tau_isTruthMatchedTau[:, 0] == 1]
                    ar = ar[ar.tau_isTruthMatchedTau[:, 1] == 1]
                    #MC23 diboson fix
                    if "diboson" in data_set and "MC23" in campaign:
                        print("Encountered dibosons in MC23, skipping truth type")
                        pass
                    else:
                        ar = ar[ar.tau_truthType[:, 0] == 10]
                        ar = ar[ar.tau_truthType[:, 1] == 10]
                        
                else:
                    pass

            else:
                print("No taus selected - hence no truth-matching")
                pass 
                
                
                
            # ---Background Tau Truth Matching Cuts---

            # ---Kinematic Cuts---
            ar = ar[ar.jet_n >= 2]
            ar = ar[ar.met / 1000 >= 200]
            ar = ar[ar.jet_pt[:, 0] / 1000 >= 120]
            ar = ar[ar.jet_pt[:, 1] / 1000 >= 20]

            ################ NON-NIKOLAI VR
            if subject != "QCD":
                ar = ar[ar.jet_delPhiMet[:, 0] >= 0.4]
                ar = ar[ar.jet_delPhiMet[:, 1] >= 0.4]
            ################ NON-NIKOLAI VR

           
            # ---------------------------------------------------------
            # ---Signal Region (SR) Cuts-------------------------------
            # ---------------------------------------------------------
            if region == "SR":
                if ntau == "1":
                    if subject == "compressed":
                        ar = ar[ar.tau_pt[:, 0] / 1000 < 45]
                        ar = ar[ar.met / 1000 > 400]
                        ar = ar[ar.tau_mtMet[:, 0] / 1000 > 80]
                        # print('Applied: SR | 1-tau | compressed.')
                    elif subject == "medium-mass":
                        ar = ar[ar.tau_pt[:, 0] / 1000 > 45]
                        ar = ar[ar.met / 1000 > 400]
                        ar = ar[ar.tau_mtMet[:, 0] / 1000 > 250]
                        ar = ar[ar.ht / 1000 > 1000]
                        # print('Applied: SR | 1-tau | medium-mass.')
                    else:
                        raise Exception("Unsupported subject.")
                elif ntau == "2":
                    if subject == "compressed":
                        ar = ar[ar.Mt2 / 1000 > 70]
                        ar = ar[ar.ht / 1000 < 1100]
                        ar = ar[
                            ar.sumMT / 1000 > 1600
                        ]  # !!!!!!!!!!!!!!! check if / 1000
                        # print('Applied: SR | 2-tau | compressed.')
                    elif subject == "high-mass":
                        ar = ar[ar.tau_mtMet_0_and_1 / 1000 > 350]
                        ar = ar[ar.ht / 1000 > 1100]
                        # print('Applied: SR | 2-tau | high-mass.')
                    elif subject == "multibin":
                        ar = ar[ar.tau_mtMet_0_and_1 / 1000 > 150]
                        ar = ar[ar.ht / 1000 > 800]
                        ar = ar[ar.jet_n >= 3]
                        #!!!!!! 7 bins in mTtau1 + mTtau2       # check how to do that
                        # print('Applied: SR | 2-tau | multibin.')
                    else:
                        raise Exception("Unsupported subject.")
                else:
                    raise Exception("Unsupported channel.")
            # ---------------------------------------------------------
            # ---Signal Region (SR) Cuts-------------------------------
            # ---------------------------------------------------------

            # ---------------------------------------------------------
            # ---Control Region (CR) Cuts------------------------------
            # ---------------------------------------------------------
            elif region == "CR":
                #Do top and W CRs simultaneously as they have 90% of the same cuts
                if (subject == "W(taunu)") or (subject == "top"):
                    if subject == "W(taunu)":
                        ar = ar[ar.jet_n_btag == 0]
                    elif subject == "top":
                        ar = ar[ar.jet_n_btag >= 1]
                    else:
                        raise Exception("Unsupported subject.")

                    ar = ar[ar.ht / 1000 < 800]
                    ar = ar[ar.met / 1000 < 300]

                    if sub_subject == "kinematic":
                        ar = ar[ar.tau_n == 0]
                        ar = ar[ar.jet_n >= 3]
                        ar = ar[ar.mu_n == 1]
                        ar = ar[ar.mu_mtMet[:, 0] / 1000 < 100]
                
                    elif sub_subject == "true-tau":
                        if channel == "1had0lep": #no muons by construction
                            pass 
                        elif channel == "1had1lep" or channel == "1hadInclusive":
                            ar = ar[ar.mu_n == 0]
                        else:
                            print("Not defined")

                        ar = ar[ar.jet_n >= 3]
                        ar = ar[ar.tau_mtMet[:, 0] / 1000 < 80]

                    elif sub_subject == "fake-tau":
                        ar = ar[ar.mu_n == 1]
                        ar = ar[ar.mu_mtMet[:, 0] / 1000 < 100]
                        if subject == "W(taunu)":
                            ar = ar[ar.Mtaumu / 1000 > 60]
                
                    else:
                        raise Exception("Unsupported sub_subject.")
                elif subject == "Z(nunu)":
                    if channel == "1had0lep": #LeptonVeto applied meaning no muons to cut on, so just pass
                        pass 
                    elif channel == "1had1lep" or channel == "1hadInclusive":
                        ar = ar[ar.mu_n == 0]
                    else:
                        print("The znunu CR is only defined in the 1 tau channel, please check input channel for typos")

                    ar = ar[ar.tau_n == 1]
                    ar = ar[ar.jet_n_btag == 0]
                    ar = ar[ar.ht / 1000 < 800]
                    ar = ar[ar.met / 1000 < 300]
                    ar = ar[ar.tau_mtMet[:, 0] / 1000 >= 100]
                    ar = ar[ar.tau_mtMet[:, 0] / 1000 < 200]
                    ar = ar[ar.met / ar.meff > 0.3]
                    ar = ar[ar.jet_delPhiMet[:, 0] >= 2.0]
                    ar = ar[ar.tau_delPhiMet[:, 0] >= 1.0]

                elif subject == "Z(tautau)":
                    ar = ar[ar.tau_n >= 2]
                    ar = ar[ar.tau_charge[:,0] * ar.tau_charge[:,1] < 0]
                    ar = ar[ar.jet_n_btag == 0]
                    ar = ar[ar.ht /1000 < 800]
                    ar = ar[(ar.tau_mtMet[:, 0] / 1000 + ar.tau_mtMet[:, 1] / 1000) < 100]
                    ar = ar[ar.Mt2 / 1000 < 70]

                
                elif subject == "QCD":
                    ar = ar[ar.tau_n == 1]
                    ar = ar[(ar.jet_delPhiMet[:, 0] < 0.3) | (ar.jet_delPhiMet[:, 1] < 0.3)] #Logical OR, not reflected in the paper, but it's in the INT note page 55
                    ar = ar[ar.tau_mtMet[:, 0] / 1000 > 100]
                    ar = ar[ar.tau_mtMet[:, 0] / 1000 < 200]
                    ar = ar[ar.met / ar.meff < 0.2]



            # ---------------------------------------------------------
            # ---Control Region (CR) Cuts------------------------------
            # ---------------------------------------------------------

            # ---------------------------------------------------------
            # ---Validation Region (VR) Cuts---------------------------
            # ---------------------------------------------------------
            elif region == "VR":
                # print('Performing: VR.')
                pass
            # ---------------------------------------------------------
            # ---Validation Region (VR) Cuts---------------------------
            # ---------------------------------------------------------
            else:
                # print('Applied: Preselection. Ignoring: SR, CR, VR.')
                pass

            # ---Event Weight Calculator---
            if (
                (data_type == "data")
                or (data_type == "background")
                or (data_type == "signal")
            ):
                ar["weight"] = (
                    ar.lumiweight
                    * ar.mcEventWeight
                    * ar.pileupweight
                    * ar.beamSpotWeight
                    * ar.jvt_weight
                    * ar.bjet_weight
                    * ar.tau_medium_weight
                    * ar.ele_weight
                    * ar.mu_weight
                )
            elif data_type == "fake":
                if ntau == '1':
                    ar["weight"] = (
                        ar.lumiweight
                        * ar.mcEventWeight
                        * ar.pileupweight
                        * ar.beamSpotWeight
                        * ar.jvt_weight
                        * ar.bjet_weight
                        * ar.tau_medium_weight
                        * ar.ele_weight
                        * ar.mu_weight
                        * 1
                        / 2
                        * (ar.ff_weight_1[:,0] + ar.ff_weight_4[:,0])
                    )
                elif ntau == '2':
                    ar["weight"] = (
                        ar.lumiweight
                        * ar.mcEventWeight
                        * ar.pileupweight
                        * ar.beamSpotWeight
                        * ar.jvt_weight
                        * ar.bjet_weight
                        * ar.tau_medium_weight
                        * ar.ele_weight
                        * ar.mu_weight
                        * 1/2 * (ar.ff_weight_1[:, 0] + ar.ff_weight_4[:, 0] + ar.ff_weight_1[:, 1] + ar.ff_weight_4[:, 1])
                    )
                else:
                    pass
            else:
                raise Exception("Unsupported data type.")
            # ---Event Weight Calculator---
            # print('Iteration', i, j)
            if j == 0:
                data_out[data_set] = ar
            else:
                data_out[data_set] = ak.concatenate([data_out[data_set], ar], axis=0)
    # ---Loop Over Provided Data Sets---

    # ---Return---
    return data_out
    # ---Return---
