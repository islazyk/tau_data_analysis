import numpy as np
import pandas as pd


def min_max_checker(df, feature_name):
    """
    Min and max checking function. Parameters:
        * df: [dataframe] - (dependent),
        * feature_name: [string] - (dependent).
    """

    df_min_max = pd.DataFrame(columns=["class", "min", "max"])

    for i in np.unique(df["class"]):
        min = np.format_float_scientific(
            np.min(df[df["class"] == i][feature_name]), precision=1
        )
        max = np.format_float_scientific(
            np.max(df[df["class"] == i][feature_name]), precision=1
        )

        df_min_max = df_min_max.append(
            {"class": i + 1, "min": min, "max": max}, ignore_index=True
        )

    return df_min_max
