import awkward as ak
import numpy as np

def mc_merger(
    merge,
    analysis_base=None,
    run=None,
    include_background=None,
    include_signal=None,
    include_fake=None,
    data_type=None,
    dicts_in=None,
    background_format=None,
    signal_format=None,
):
    """
    Background merging function. Parameters:
        * merge: [boolean] - ('True', 'False'),
        * analysis_base: [string] - ('24.2.28'),
        * dicts_in: [dictionary] (dependent).
    """

    dicts_new = {}
    labels = []
    #Update backgrounds once Higgs and ttX are ready!
    if merge:
        if (include_background) and (data_type == "background"):
            if background_format == "background_as_one":
                dicts_new["background"] = ak.concatenate(
                    [dicts_in[x] for x in dicts_in], axis=0
                )
                labels = [
                    "background",
                ]

            elif background_format == "background_primary":
                if analysis_base == "24.2.28":
                    dicts_new["topquarks"] = ak.concatenate(
                        [
                            dicts_in["ttbar"], 
                            dicts_in["singletop"]
                        ],
                        axis=0,
                    )
                    dicts_new["wtaunu"] = dicts_in["wtaunu"]
                    dicts_new["ztautau"] = dicts_in["ztautau"]
                    dicts_new["diboson"] = dicts_in["diboson"]
                    # dicts_new["other"] = ak.concatenate(
                    #     [
                    #         dicts_in["znunu"],
                    #         dicts_in["wmunu"],
                    #         dicts_in["wenu"],
                    #         dicts_in["zmumu"],
                    #         dicts_in["zee"],
                    #     ],
                    #     axis=0,
                    # )
                else:
                    raise Exception(
                        "Unsupported analysis base. Only 24.2.28 allowed"
                    )

                if include_fake:
                    labels = [
                        "top quarks",
                        r"$W \rightarrow \tau\nu$",
                        r"$Z \rightarrow \tau\tau$",
                        "diboson",
                        # "other",
                    ]
                else:
                    labels = [
                        "top quarks",
                        r"$W \rightarrow \tau\nu$",
                        r"$Z \rightarrow \tau\tau$",
                        # r"$Z \rightarrow \nu\nu$",
                        "diboson",
                        # "other",
                    ]

            elif background_format == "background_initial":
                dicts_new = dicts_in

                if analysis_base == "24.2.28":
                    labels = [
                        r"$t \bar{t}$",
                        r"$W \rightarrow \tau\nu$",
                        r"$W \rightarrow \mu\nu$",
                        r"$W \rightarrow e \nu$",
                        r"$Z \rightarrow \tau\tau$",
                        r"$Z \rightarrow \mu\mu$",
                        r"$Z \rightarrow ee$",
                        r"$Z \rightarrow \nu\nu$",
                        "diboson",
                        "higgs",
                        "singletop",
                        "ttX",
                    ]
                    if run == "Run3":
                        labels.remove("higgs")
                        labels.remove("ttX")
            else:
                raise Exception(
                    "Unsupported background format. Only 'background_as_one', 'background_primary' or 'background_initial' allowed"
                )

        elif (include_background) and (data_type == "fake"):
            if background_format == "fake_as_one":
                dicts_new["fake"] = ak.concatenate(
                    [dicts_in[x] for x in dicts_in], axis=0
                )
                labels = [
                    "fake taus",
                ]
        elif (include_signal) and (data_type == "signal"):
            if analysis_base == "24.2.28":
                if signal_format == "signal_as_one":
                    dicts_new["signal"] = ak.concatenate(
                        [dicts_in[x] for x in dicts_in], axis=0
                    )
                    labels = ["signal"]
                elif signal_format == "signal_type":
                    pass
                elif signal_format == "signal_mass":
                    pass
                elif signal_format == "signal_mass":
                    pass

                # if signal_format == 'signal_mode':
                #     dicts_new.update(dicts_in[0])
                #     dicts_new.update(dicts_in[1])
                #     dicts_new.update(dicts_in[2])
                #     dicts_new.update(dicts_in[3])
                #     dicts_new.update(dicts_in[4])
                #     dicts_new.update(dicts_in[5])

                #     labels = None

                # elif signal_format == 'signal_mass':
                #     dicts_new['gg-LM'] = ak.concatenate([dicts_in[0][x] for x in dicts_in[0]], axis=0)
                #     dicts_new['gg-MM'] = ak.concatenate([dicts_in[1][x] for x in dicts_in[1]], axis=0)
                #     dicts_new['gg-HM'] = ak.concatenate([dicts_in[2][x] for x in dicts_in[2]], axis=0)

                #     dicts_new['ss-LM'] = ak.concatenate([dicts_in[3][x] for x in dicts_in[3]], axis=0)
                #     dicts_new['ss-MM'] = ak.concatenate([dicts_in[4][x] for x in dicts_in[4]], axis=0)
                #     dicts_new['ss-HM'] = ak.concatenate([dicts_in[5][x] for x in dicts_in[5]], axis=0)

                #     labels = [
                #         '$\\breve g \\breve g$ LM',
                #         '$\\breve g \\breve g$ MM',
                #         '$\\breve g \\breve g$ HM',
                #         '$\\breve s \\breve s$ LM',
                #         '$\\breve s \\breve s$ MM',
                #         '$\\breve s \\breve s$ HM'
                #     ]

                # elif signal_format == 'signal_type':
                #     dicts_new['gg-LM'] = ak.concatenate([dicts_in[0][x] for x in dicts_in[0]], axis=0)
                #     dicts_new['gg-MM'] = ak.concatenate([dicts_in[1][x] for x in dicts_in[1]], axis=0)
                #     dicts_new['gg-HM'] = ak.concatenate([dicts_in[2][x] for x in dicts_in[2]], axis=0)

                #     dicts_new['ss-LM'] = ak.concatenate([dicts_in[3][x] for x in dicts_in[3]], axis=0)
                #     dicts_new['ss-MM'] = ak.concatenate([dicts_in[4][x] for x in dicts_in[4]], axis=0)
                #     dicts_new['ss-HM'] = ak.concatenate([dicts_in[5][x] for x in dicts_in[5]], axis=0)

                #     dicts_new['gg'] = ak.concatenate([dicts_new['gg-LM'], dicts_new['gg-MM'], dicts_new['gg-HM']], axis=0)
                #     dicts_new['ss'] = ak.concatenate([dicts_new['ss-LM'], dicts_new['ss-MM'], dicts_new['ss-HM']], axis=0)

                #     keys_delete = ['gg-LM', 'gg-MM', 'gg-HM', 'ss-LM', 'ss-MM', 'ss-HM']
                #     dicts_new = {key: value for key, value in dicts_new.items() if key not in keys_delete}

                #     labels = [
                #         '$\\breve g \\breve g$',
                #         '$\\breve s \\breve s$'
                #     ]

                # elif signal_format == 'signal_as_one':
                #     dicts_new['gg-LM'] = ak.concatenate([dicts_in[0][x] for x in dicts_in[0]], axis=0)
                #     dicts_new['gg-MM'] = ak.concatenate([dicts_in[1][x] for x in dicts_in[1]], axis=0)
                #     dicts_new['gg-HM'] = ak.concatenate([dicts_in[2][x] for x in dicts_in[2]], axis=0)

                #     dicts_new['ss-LM'] = ak.concatenate([dicts_in[3][x] for x in dicts_in[3]], axis=0)
                #     dicts_new['ss-MM'] = ak.concatenate([dicts_in[4][x] for x in dicts_in[4]], axis=0)
                #     dicts_new['ss-HM'] = ak.concatenate([dicts_in[5][x] for x in dicts_in[5]], axis=0)

                #     dicts_new['signal'] = ak.concatenate(
                #         [
                #             dicts_new['gg-LM'],
                #             dicts_new['gg-MM'],
                #             dicts_new['gg-HM'],
                #             dicts_new['ss-LM'],
                #             dicts_new['ss-MM'],
                #             dicts_new['ss-HM']
                #         ],
                #         axis = 0
                #     )

                #     keys_delete = ['gg-LM', 'gg-MM', 'gg-HM', 'ss-LM', 'ss-MM', 'ss-HM']
                #     dicts_new = {key: value for key, value in dicts_new.items() if key not in keys_delete}

                #     labels = [
                #         'signal'
                #     ]

                else:
                    raise Exception(
                        "Unsupported signal format. Only 'signal_as_one', 'signal_type', 'signal_mass' or 'signal_mode' allowed"
                    )
            else:
                raise Exception("Unsupported analysis base. Only '24.2.28' allowed")
        else:
            raise Exception("Only backgrounds and signals allowed to be merged.")

        print(f"{data_type.capitalize()}s merged.")

        return dicts_new, labels

    else:
        print("No merging performed.")
