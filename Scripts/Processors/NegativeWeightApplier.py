import numpy as np
import pandas as pd


def negative_weight_applier(
    apply_negative_weight,
    normalize=False,
    X_train=None,
    y_train=None,
    X_test=None,
    y_test=None,
):
    """
    Weight applying function. Parameters:
        * apply_weight: [boolean] - (True, False),
        * normalize: [boolean] - (True, False),
        * X_train: [dataframe] - (dependent),
        * y_train: [series] - (dependent),
        * X_test: [dataframe] - (dependent),
        * y_test: [series] - (dependent).
    """

    if apply_negative_weight:
        # Locating events with negative weights
        indices_negative_weight = X_train[X_train["weight"] < 0].index

        # Moving events with negative weights from train to test sample.
        X_test = pd.concat([X_test, X_train.loc[indices_negative_weight]])
        y_test = pd.concat([y_test, y_train.loc[indices_negative_weight]])

        # Dropping events with negative wiehts in train sample.
        X_train = X_train.drop(X_train.loc[indices_negative_weight].index)
        y_train = y_train.drop(y_train.loc[indices_negative_weight].index)

        # Creating a weight series
        X_train_weight = X_train["weight"]
        X_test_weight = X_test["weight"]

        # Converting a series to numpy arrays
        X_train_weight = X_train_weight.to_numpy()
        X_test_weight = X_test_weight.to_numpy()

        if normalize:
            X_train_weight_normalized = (X_train_weight - np.min(X_train_weight)) / (
                np.max(X_train_weight) - np.min(X_train_weight)
            )
            X_test_weight_normalized = (X_test_weight - np.min(X_test_weight)) / (
                np.max(X_test_weight) - np.min(X_test_weight)
            )

            print("Weight applied.")
            print("Normalization performed.")
            return (
                X_train,
                X_test,
                y_train,
                y_test,
                X_train_weight_normalized,
                X_test_weight_normalized,
            )
        else:
            print("Weight applied.")
            print("No normalization performed.")
            return (X_train, X_test, y_train, y_test, X_train_weight, X_test_weight)
    else:
        print("No weight applied.")
