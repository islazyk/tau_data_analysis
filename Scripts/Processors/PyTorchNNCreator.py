import torch
import torch.nn as nn


class PyTorchModel(nn.Module):
    """
    PyTorch Neural Networks creating class. Parameters:
        * input_size: [number] - (dependent),
        * hidden_size: [list] - (dependent),
        * output_size: [number] - (dependent),
        * activation: [function] - (dependent),
        * dropout_prob: [float] - (dependent)

    """

    def __init__(self, input_size, hidden_size, output_size, activation, dropout):
        super(PyTorchModel, self).__init__()

        # Input Layer
        self.input_layer = nn.Linear(input_size, hidden_size[0])

        # Hidden Layers
        self.hidden_layers = nn.ModuleList()
        for i in range(len(hidden_size) - 1):
            self.hidden_layers.append(nn.Linear(hidden_size[i], hidden_size[i + 1]))
            self.hidden_layers.append(activation)
            # Dropout
            if dropout:
                self.hidden_layers.append(nn.Dropout(p=dropout))

        # Output Layer
        self.output_layer = nn.Linear(hidden_size[-1], output_size)

    def forward(self, x):
        # Pass input through input layer
        x = self.input_layer(x)

        # Pass input through each hidden layer
        for hidden_layer in self.hidden_layers:
            x = hidden_layer(x)

        # Pass input through output layer
        x = self.output_layer(x)

        return x
