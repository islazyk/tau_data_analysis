import numpy as np
import pandas as pd


def results_presenter(
    framework_type, algorithm_type, y_test, predictions, predictions_proba=None
):
    """
    Objective defining function. Parameters:
        * framework_type: [string] - ('XGBoost', 'PyTorch'),
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * y_test: [list] - (dependent),
        * predictions: [list] - (dependent),
        * predictions_proba: [list] - (dependent).
    """

    if framework_type == "XGBoost":
        y_test_numpy = np.array(y_test)
        y_probabilities_numpy = predictions_proba
        y_probabilities_list = np.round(
            y_probabilities_numpy.astype(np.float64), 2
        ).tolist()
        y_predictions_numpy = predictions

        dict_results = {
            "y_test": y_test_numpy,
            "y_probabilities": y_probabilities_numpy,
            "y_predictions": y_predictions_numpy,
        }

        df_results = pd.DataFrame(
            {
                "y_test": y_test_numpy,
                "y_probabilities": y_probabilities_list,
                "y_predictions": y_predictions_numpy,
            }
        )

    elif framework_type == "PyTorch":
        import torch

        y_test_tensor = y_test
        y_test_numpy = y_test_tensor.cpu().numpy().astype(int)

        if algorithm_type == "binary-classification":
            y_logits_tensor = predictions
            y_logits_numpy = y_logits_tensor.squeeze(1).cpu().detach().numpy()
            y_logits_list = np.round(y_logits_numpy.astype(np.float64), 2)

            y_probabilities_one_tensor = torch.sigmoid(y_logits_tensor)
            y_probabilities_tensor = torch.cat(
                [1 - y_probabilities_one_tensor, y_probabilities_one_tensor], dim=1
            )
            y_probabilities_numpy = (
                y_probabilities_tensor.cpu().detach().squeeze(1).numpy()
            )
            y_probabilities_list = np.round(
                y_probabilities_numpy.astype(np.float64), 2
            ).tolist()

            y_predictions_tensor = torch.round(y_probabilities_one_tensor)
            y_predictions_numpy = (
                y_predictions_tensor.cpu().detach().squeeze(1).numpy().astype(int)
            )

        elif algorithm_type == "multiclass-classification":
            y_logits_tensor = predictions
            y_logits_numpy = y_logits_tensor.cpu().detach().numpy()
            y_logits_list = np.round(y_logits_numpy.astype(np.float64), 2).tolist()

            y_probabilities_tensor = torch.nn.functional.softmax(y_logits_tensor, dim=1)
            y_probabilities_numpy = y_probabilities_tensor.cpu().detach().numpy()
            y_probabilities_list = np.round(
                y_probabilities_numpy.astype(np.float64), 2
            ).tolist()

            y_predictions_tensor = torch.argmax(y_probabilities_tensor, dim=1)
            y_predictions_numpy = (
                y_predictions_tensor.cpu().detach().numpy().astype(int)
            )

        else:
            pass

        dict_results = {
            "y_test": y_test_numpy,
            "y_logits": y_logits_numpy,
            "y_probabilities": y_probabilities_numpy,
            "y_predictions": y_predictions_numpy,
        }

        df_results = pd.DataFrame(
            {
                "y_test": y_test_numpy,
                "y_logits": y_logits_list,
                "y_probabilities": y_probabilities_list,
                "y_predictions": y_predictions_numpy,
            }
        )

    else:
        raise Exception(
            "Unsupported type of a framework. Only 'XGBoost' or 'PyTorch' allowed."
        )

    return dict_results, df_results
