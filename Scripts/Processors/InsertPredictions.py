#Small script to insert the ML-predictions back into the dataframe as well as the channel
#The input is the dataframe in which to append these values, and predictions_proba is the array of scores per class per event
import pandas as pd 
import numpy as np 


def insert_preds(df, y_prob, channel):
    print(f"Inserting predictions for the {channel} channel")

    for i in range(y_prob.shape[1]):
        df[f"output_score_class_{i}"] = y_prob[:,i]

    if "1had0lep" in channel:
        df.rename(columns = {"output_score_class_5" : "output_score_signal"}, inplace = True)
        columns = ["output_score_class_0", "output_score_class_1", "output_score_class_2", "output_score_class_3", "output_score_class_4"]
        class_dict = {
            0 : 0,
            1 : 1, 
            2 : 2, 
            3 : 3, 
            4 : 4,
        }

    else: #Wtaunu is negligble in 1had1lep and 2had 
        df.rename(columns= {"output_score_class_4" : "output_score_signal"}, inplace = True)
        columns = ["output_score_class_0", "output_score_class_1", "output_score_class_2", "output_score_class_3"]
        class_dict = {
            0 : 0,
            1 : 1, 
            2 : 2,
            3 : 3,
        }

    df["max"] = df[columns].idxmax(axis = 1)
    df['y_pred'] = df['max'].apply(lambda x: class_dict[int(x.split('_')[-1])])
    df.drop(columns=["max"], inplace = True)

    if channel == "1had0lep":
        df["channel"] = 0
    elif channel == "1had1lep":
        df["channel"] = 1
    elif channel == "2had":
        df["channel"] = 2
    else:
        raise Exception("Error in channel input")

    return df 

