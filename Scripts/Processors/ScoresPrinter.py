import numpy as np


def scores_printer(algorithm_type, y_test, predictions):
    """
    Scores printing function. Parameters:
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * y_test: [series] - (dependent),
        * predictions: [series] - (dependent),
    """

    if algorithm_type == "regression":
        from sklearn.metrics import mean_squared_error

        rmse = np.sqrt(mean_squared_error(y_test, predictions))
        print("RMSE: %f" % (rmse))

    elif algorithm_type == "binary-classification":
        from sklearn.metrics import (
            accuracy_score,
            precision_score,
            recall_score,
            f1_score,
            roc_auc_score,
        )

        print(f"Accuracy Score: {np.round(accuracy_score(y_test, predictions)*100,2)}%")
        print(
            f"Precision Score: {np.round(precision_score(y_test, predictions)*100,2)}%"
        )
        print(f"Recall Score: {np.round(recall_score(y_test, predictions)*100,2)}%")
        print(f"F1 Score: {np.round(f1_score(y_test, predictions)*100,2)}%")
        # print(f"ROC AUC Score: {np.round(roc_auc_score(y_test, predictions_proba[:, 1])*100,2)}%")

    elif algorithm_type == "multiclass-classification":
        from sklearn.metrics import (
            accuracy_score,
            precision_score,
            recall_score,
            f1_score,
            roc_auc_score,
        )

        print(
            f"Precision Score | Accuracy (Micro Average): {np.round(precision_score(y_test, predictions, average='micro')*100,2)}%"
        )
        print(
            f"Precision Score | Macro Average: {np.round(precision_score(y_test, predictions, average='macro')*100,2)}%"
        )
        print(
            f"Precision Score | Weighted Average: {np.round(precision_score(y_test, predictions, average='weighted')*100,2)}%"
        )
        print(
            f"Recall Score | Accuracy (Micro Average): {np.round(recall_score(y_test, predictions, average='micro')*100,2)}%"
        )
        print(
            f"Recall Score | Macro Average: {np.round(recall_score(y_test, predictions, average='macro')*100,2)}%"
        )
        print(
            f"Recall Score | Weighted Average: {np.round(recall_score(y_test, predictions, average='weighted')*100,2)}%"
        )
        print(
            f"F1 Score | Accuracy (Micro Average): {np.round(f1_score(y_test, predictions, average='micro')*100,2)}%"
        )
        print(
            f"F1 Score | Macro Average: {np.round(f1_score(y_test, predictions, average='macro')*100,2)}%"
        )
        print(
            f"F1 Score | Weighted Average: {np.round(f1_score(y_test, predictions, average='weighted')*100,2)}%"
        )

    else:
        raise Exception(
            "Unsupported type of an algorithm. Only 'regression', 'binary-classification' or 'multiclass-classification' allowed."
        )
