import awkward as ak
import numpy as np


def ntuple_preparer(
    include,
    merged_array,
    padding_threshold,
):
    """
    Padding rectangularizing function. Parameters:
        * merged_array: [array] - (dependent),
        * padding_threshold: [number] - (dependent),
    """

    if include:
        # ---Rectangularization---
        jagged_fetures = []
        for feature in ak.fields(merged_array):
            try:  # Attempting to flatten features - catches all variable length features
                ak.flatten(merged_array[str(feature)], axis=1)
                jagged_fetures.append(
                    str(feature)
                )  # Fields which need to be flattened and padded
            except Exception:
                merged_array = merged_array

        for feature in jagged_fetures:
            merged_array[feature] = ak.pad_none(
                merged_array[feature], target=padding_threshold, clip=True
            )  # Padding
            for i in range(padding_threshold):
                merged_array[f"{feature}_{i}"] = merged_array[feature][
                    :, i
                ]  # Adding a new feature on the i'th element of var-length features
            merged_array = merged_array[
                [x for x in ak.fields(merged_array) if x != feature]
            ]  # Removing jagged arrays
        # ---Rectangularization---

        # ---To ROOT---
        df = ak.to_numpy(merged_array)
        # ---to ROOT---

        return df
    else:
        return None
