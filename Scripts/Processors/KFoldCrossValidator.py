import numpy as np


def k_fold_cross_validator(
    algorithm_type,
    n_folds,
    stopping,
    xgb_model,
    X_train,
    y_train,
    X_test,
    y_test,
    metric,
):
    """
    K-fold cross validating function. Parameters:
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * n_folds: [number] - (dependent),
        * stopping: [boolean] - (True, False),
        * xgb_model: [xgboost] - (dependent),
        * X_train: [dataframe] - (dependent),
        * y_train: [series] - (dependent),
        * X_test: [dataframe] - (dependent),
        * y_test: [series] - (dependent),
        * metric: [string] - (dependent).
    """

    from sklearn.model_selection import RepeatedStratifiedKFold, cross_val_score

    cv = RepeatedStratifiedKFold(n_splits=n_folds, n_repeats=1, random_state=1)

    if algorithm_type == "regression":
        print("Not available for regression.")

    elif (algorithm_type == "binary-classification") or (
        algorithm_type == "multiclass-classification"
    ):
        scoring = "accuracy"

        if stopping:
            fit_paramseters_cv = {
                "eval_set": [(X_train, y_train), (X_test, y_test)],
                "eval_metric": metric,
                "early_stopping_rounds": 50,
                "verbose": 0,
            }

            xgb_cv = cross_val_score(
                estimator=xgb_model,
                X=X_train,
                y=y_train,
                cv=cv,
                scoring=scoring,
                # n_jobs = -1,
                fit_params=fit_paramseters_cv,
            )
            print(
                f"Mean {scoring} score equals {np.round(np.mean(xgb_cv)*100,2)}% for the {n_folds}-folds with early-stopping-round functionality."
            )

        else:
            xgb_cv = cross_val_score(
                estimator=xgb_model,
                X=X_train,
                y=y_train,
                cv=cv,
                scoring=scoring,
                # n_jobs = -1,
            )
            print(
                f"Mean {scoring} score equals {np.round(np.mean(xgb_cv)*100,2)}% for the {n_folds}-folds without early-stopping-round functionality."
            )

    else:
        raise Exception(
            "Unsupported type of an algorithm. Only 'regression', 'binary-classification' or 'multiclass-classification' allowed."
        )
