import numpy as np

import xgboost as xgb
import matplotlib.pyplot as plt


def tree_plotter(model, nth_tree, width, height):
    """
    Tree plotting function. Parameters:
        * model: [xgboost] - (dependent),
        * nth_tree: [number] - (dependent <0 - last tree>),
        * width: [number] - (dependent),
        * height: [number] - (dependent).
    """

    fig, ax = plt.subplots(figsize=(width, height))
    xgb.plot_tree(
        booster=model,
        num_trees=nth_tree,
        ax=ax,
    )

    plt.show()


def logistic_converter(x):
    return 1 / (1 + np.exp(-(x)))
