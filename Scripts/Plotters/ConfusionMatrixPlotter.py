import numpy as np

import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns


def confusion_matrix_plotter(algorithm_type, y_test, predictions, labels, save, savepath):
    """
    Confusion matrix plotting function. Parameters:
      * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
      * y_test: [series] - (dependent),
      * predictions: [series] - (dependent),
      * labels: [list] - (dependent).
    """

    if algorithm_type == "regression":
        print("Not available for regression.")

    elif algorithm_type == "binary-classification":
        from sklearn.metrics import confusion_matrix

        c_matrix = confusion_matrix(y_test, predictions)

        groups = ["True Negative", "False Positive", "False Negative", "True Positive"]

        counts = np.array([x for x in c_matrix.flatten()])

        percentages = []
        percentages_matrix = []
        for x in range(len(labels)):
            percentages.append(c_matrix[x] / y_test.value_counts()[x] * 100)
        percentages_matrix = percentages
        percentages = np.round(np.array(percentages).flatten(), 2)
        percentages = np.array(list(map("{}\%".format, percentages)))

        annot = [f"{v1}\n{v2}\n{v3}" for v1, v2, v3 in zip(groups, counts, percentages)]
        annot = np.asarray(annot).reshape(2, 2)

        # ax= plt.subplot()
        # fig, ax = plt.subplots(figsize=(16, 8), gridspec_kw={'width_ratios': [4, 1]})
        fig, ax = plt.subplots(1, 1, figsize=(10, 10))

        sns.heatmap(
            percentages_matrix,
            ax=ax,
            annot=annot,
            cmap="Greens",
            linewidths=1.0,
            linecolor="black",
            square=True,
            fmt="",
            cbar=True,
            vmin=0,
            vmax=100,
            cbar_kws={"format": "%.0f\%%", "shrink": 0.8},
            xticklabels=labels,
            yticklabels=labels,
        )

        ax.set_title("Confusion Matrix")

        ax.set_xlabel("Predicted Labels")
        ax.set_ylabel("True Labels")

        ax.xaxis.set_ticklabels(["False", "True"])
        ax.yaxis.set_ticklabels(["False", "True"])

        plt.show()

    elif algorithm_type == "multiclass-classification":
        from sklearn.metrics import confusion_matrix

        c_matrix = confusion_matrix(y_test, predictions)

        counts = np.array([x for x in c_matrix.flatten()])

        percentages = []
        percentages_matrix = []
        for x in range(len(labels)):
            percentages.append(c_matrix[x] / y_test.value_counts()[x] * 100)
        percentages_matrix = percentages
        percentages = np.round(np.array(percentages).flatten(), 2)
        percentages = np.array(list(map("{}\%".format, percentages)))

        annot = [f"{v1}\n{v2}" for v1, v2 in zip(counts, percentages)]
        annot = np.asarray(annot).reshape(len(labels), len(labels))

        # ax= plt.subplot()
        # fig, ax = plt.subplots(figsize=(16, 8), gridspec_kw={'width_ratios': [4, 1]})

        if len(labels) <= 8:
            fig, ax = plt.subplots(1, 1, figsize=(14, 14))
        else:
            fig, ax = plt.subplots(1, 1, figsize=(20, 20))

        sns.heatmap(
            percentages_matrix,
            ax=ax,
            annot=annot,
            cmap="Greens",
            linewidths=1.0,
            linecolor="black",
            square=True,
            fmt="",
            cbar=True,
            vmin=0,
            vmax=100,
            cbar_kws={"format": "%.0f\%%", "shrink": 0.8},
            xticklabels=labels,
            yticklabels=labels,
        )

        ax.set_title("Confusion Matrix")

        ax.set_ylabel("True Labels")
        ax.set_xlabel("Predicted Labels")

        if save:
            plt.savefig(savepath)
        
        else:
            plt.show()

    else:
        raise Exception(
            "Unsupported type of an algorithm. Only 'regression', 'binary-classification' or 'multiclass-classification' allowed."
        )
