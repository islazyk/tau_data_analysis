import numpy as np

import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns


def curve_plotter(
    framework_type,
    algorithm_type,
    curve_type,
    model,
    y_test,
    X_test,
    predictions,
    predictions_proba,
    labels,
):
    """
    Curve plotting function. Parameters:
        * framework_type: [string] - ('XGBoost', 'PyTorch').
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * curve_type: [string] - ('roc', 'pr'),
        * model: [xgboost model | none] - (dependent),
        * y_test: [series] - (dependent),
        * X_test: [pandas dataframe] - (dependent),
        * predictions: [numpy array] - (dependent),
        * predictions_proba: [numpy array] - (dependent),
        * labels: [list] - (dependent).
    """

    if algorithm_type == "regression":
        print("Not available for regression.")

    elif algorithm_type == "binary-classification":
        fig, ax = plt.subplots(1, 2, figsize=(16, 6))

        if curve_type == "roc":
            if model:
                from sklearn.metrics import RocCurveDisplay

                RocCurveDisplay.from_predictions(
                    y_test, predictions, ax=ax[0], linewidth=3
                )
                RocCurveDisplay.from_estimator(
                    model, X_test, y_test, ax=ax[1], linewidth=3
                )

            else:
                from sklearn.preprocessing import label_binarize
                from sklearn.metrics import roc_curve, auc

                # ---Three-Point---
                fpr_3_point, tpr_3_point, _ = roc_curve(y_test, predictions)

                auc_3_point_results = auc(fpr_3_point, tpr_3_point)
                # ---Three-Point---

                # ---Many-Points---
                y_test_binarized = label_binarize(
                    y_test, classes=np.arange(len(labels))
                )
                fpr = dict()
                tpr = dict()
                roc_auc = dict()

                fpr[0], tpr[0], _ = roc_curve(
                    y_test_binarized[:, 0], predictions_proba[:, 1]
                )

                auc_results = auc(fpr[0], tpr[0])

                # from sklearn.metrics import roc_auc_score
                # auc_results = roc_auc_score(                  # another way to comput auc
                #     y_test_binarized[:, 0],
                #     predictions_proba[:, 0]
                # )

                # ---Many-Points---

                ax[0].plot(
                    fpr_3_point,
                    tpr_3_point,
                    color="green",
                    linewidth=3,
                    label=f"{framework_type} (AUC = {np.round(auc_3_point_results, 2)})",
                )

                ax[1].plot(
                    fpr[0],
                    tpr[0],
                    color="green",
                    linewidth=3,
                    label=f"{framework_type} (AUC = {np.round(auc_results, 2)})",
                )

            ax[0].plot(
                [0, 1], [0, 1], "--", color="red", label="Random Guess (AUC = 0.50)"
            )
            ax[0].plot(
                [0, 0], [0, 1], "--", color="blue", label="Perfect Model (AUC = 1.00)"
            )
            ax[0].plot([0, 1], [1, 1], "--", color="blue")

            ax[1].plot(
                [0, 1], [0, 1], "--", color="red", label="Random Guess (AUC = 0.50)"
            )
            ax[1].plot(
                [0, 0], [0, 1], "--", color="blue", label="Perfect Model (AUC = 1.00)"
            )
            ax[1].plot([0, 1], [1, 1], "--", color="blue")

            ax[0].set_title("Receiver Operating Characteristic Curve (three-point)")
            ax[0].set_xlabel("False Positive Rate")
            ax[0].set_ylabel("True Positive Rate")

            ax[1].set_title("Receiver Operating Characteristic Curve")
            ax[1].set_xlabel("False Positive Rate")
            ax[1].set_ylabel("True Positive Rate")

            ampl.draw_atlas_label(-1.18, 0.97)
            ampl.draw_atlas_label(0.02, 0.97)

        elif curve_type == "pr":
            if model:
                from sklearn.metrics import PrecisionRecallDisplay

                PrecisionRecallDisplay.from_predictions(
                    y_test, predictions, ax=ax[0], linewidth=3
                )
                PrecisionRecallDisplay.from_estimator(
                    model, X_test, y_test, ax=ax[1], linewidth=3
                )

            else:
                from sklearn.preprocessing import label_binarize
                from sklearn.metrics import (
                    precision_recall_curve,
                    average_precision_score,
                )

                # ---Three-Point---
                precision_3_point, recall_3_point, _ = precision_recall_curve(
                    y_test, predictions
                )

                ap_3_point_results = average_precision_score(y_test, predictions)
                # ---Three-Point---

                # ---Many-Points---
                y_test_binarized = label_binarize(
                    y_test, classes=np.arange(len(labels))
                )
                precision = dict()
                recall = dict()

                precision[0], recall[0], _ = precision_recall_curve(
                    y_test_binarized[:, 0], predictions_proba[:, 1]
                )

                ap = average_precision_score(
                    y_test_binarized[:, 0], predictions_proba[:, 1]
                )
                # ---Many-Points---

                # ax[0].plot(
                #     recall_3_point,
                #     precision_3_point,
                #     color='green',
                #     linewidth=3,
                #     label=f'Classifier (AP: {np.round(ap_3_point_results, 2)})'
                # )

                from sklearn.metrics import PrecisionRecallDisplay

                pr_display = PrecisionRecallDisplay(
                    precision=precision_3_point,
                    recall=recall_3_point,
                )
                pr_display.plot(
                    ax=ax[0],
                    color="green",
                    linewidth=3,
                    label=f"{framework_type} (AP = {np.round(ap_3_point_results, 2)})",
                )

                ax[1].plot(
                    recall[0],
                    precision[0],
                    color="green",
                    linewidth=3,
                    label=f"{framework_type} (AP = {np.round(ap, 2)})",
                )

            ax[0].plot(
                [0, 1], [0.5, 0.5], "--", color="red", label="Random Guess (AP = 0.50)"
            )
            ax[0].plot(
                [1, 1], [0, 1], "--", color="blue", label="Perfect Model (AP = 1.00)"
            )
            ax[0].plot([0, 1], [1, 1], "--", color="blue")

            ax[1].plot(
                [0, 1], [0.5, 0.5], "--", color="red", label="Random Guess (AP = 0.50)"
            )
            ax[1].plot(
                [1, 1], [0, 1], "--", color="blue", label="Perfect Model (AP = 1.00)"
            )
            ax[1].plot([0, 1], [1, 1], "--", color="blue")

            ax[0].set_title("Precision-Recall Curve (three-point)")
            ax[0].set_xlabel("Recall")
            ax[0].set_ylabel("Precision")

            ax[1].set_title("Precision-Recall Curve")
            ax[1].set_xlabel("Recall")
            ax[1].set_ylabel("Precision")

            ampl.draw_atlas_label(-1.18, 0.97)
            ampl.draw_atlas_label(0.02, 0.97)

        else:
            raise Exception("Unsupported type of a curve. Only 'roc' or 'pr' allowed.")

        ax[0].set_xlim([-0.1, 1.1])
        ax[0].set_ylim([-0.1, 1.1])
        ax[1].set_xlim([-0.1, 1.1])
        ax[1].set_ylim([-0.1, 1.1])

        ax[0].legend(fontsize=12)
        ax[1].legend(fontsize=12)

    elif algorithm_type == "multiclass-classification":
        from sklearn.preprocessing import label_binarize

        y_test_binarized = label_binarize(y_test, classes=np.arange(len(labels)))
        # y_test_binarized = label_binarize(y_test, classes=[*range(len(np.unique(y)))])

        fig, ax = plt.subplots(1, 1, figsize=(9, 7))
        colors = sns.color_palette("Paired", n_colors=len(labels))

        if curve_type == "roc":
            from sklearn.metrics import roc_curve, auc

            fpr = dict()
            tpr = dict()

            roc_auc = dict()

            for i in range(len(labels)):
                fpr[i], tpr[i], _ = roc_curve(
                    y_test_binarized[:, i], predictions_proba[:, i]
                )

                roc_auc = auc(fpr[i], tpr[i])

                from sklearn.metrics import roc_auc_score

                roc_auc_score_results = roc_auc_score(
                    y_test_binarized[:, i], predictions_proba[:, i]
                )

                plt.plot(
                    fpr[i],
                    tpr[i],
                    color=colors[i],
                    label=f"{labels[i]} (AUC: {np.round(roc_auc_score_results, 2)})"
                    # label=f'{labels[i]} (AUC: {np.round(roc_auc, 2)}, {np.round(roc_auc_score_results, 2)})'
                )

            ax.plot(
                [0, 1], [0, 1], "--", color="red", label="Random Guess (AUC = 0.50)"
            )
            ax.plot(
                [0, 0], [0, 1], "--", color="blue", label="Perfect Model (AUC = 1.00)"
            )
            ax.plot([0, 1], [1, 1], "--", color="blue")

            ax.set_title("ROC Curve")
            ax.set_xlabel("False Positive Rate")
            ax.set_ylabel("True Positive Rate")

        elif curve_type == "pr":
            from sklearn.metrics import precision_recall_curve, average_precision_score

            precision = dict()
            recall = dict()

            for i in range(len(labels)):
                precision[i], recall[i], _ = precision_recall_curve(
                    y_test_binarized[:, i], predictions_proba[:, i]
                )

                ap = average_precision_score(
                    y_test_binarized[:, i], predictions_proba[:, i]
                )

                plt.plot(
                    recall[i],
                    precision[i],
                    color=colors[i],
                    label=f"{labels[i]} (AP: {np.round(ap, 2)})",
                )

            ax.set_title("PR Curve")
            ax.set_xlabel("Recall")
            ax.set_ylabel("Precision")

            ax.plot(
                [0, 1], [0.5, 0.5], "--", color="red", label="Random Guess (AP = 0.50)"
            )
            ax.plot(
                [1, 1], [0, 1], "--", color="blue", label="Perfect Model (AP = 1.00)"
            )
            ax.plot([0, 1], [1, 1], "--", color="blue")

        else:
            raise Exception("Unsupported type of a curve. Only 'roc' or 'pr' allowed.")

        leg = ax.legend(
            fontsize=12,
            bbox_to_anchor=(1.01, 1.015),
            # loc='upper left',
            # borderaxespad=0,
            frameon=True,
        )
        leg.get_frame().set_edgecolor("black")
        leg.get_frame().set_linewidth(1.0)

        ax.set_xlim([-0.1, 1.1])
        ax.set_ylim([-0.1, 1.1])

    else:
        raise Exception(
            "Unsupported type of an algorithm. Only 'regression', 'binary-classification' or 'multiclass-classification' allowed."
        )

    # ---ATLAS Label---
    ampl.draw_atlas_label(0.02, 0.97)
    # ---ATLAS Label---

    plt.show()
