import awkward as ak
import numpy as np

import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.backends.backend_pdf import PdfPages

import scipy

def fixedPlotter(
    plot_features,
    savepath,
    run,
    region,
    subject,
    sub_subject,
    datas_dict,
    backgrounds_dict,
    labels_data,
    labels_background,
    scale_type,
):
    color = sns.color_palette("Set3", n_colors = len(backgrounds_dict.keys()))

    with PdfPages(savepath) as pdf:

        for i in range(0, len(plot_features)):
            
            bins = np.linspace(plot_features[i]["xaxis_start"], plot_features[i]["xaxis_end"], plot_features[i]["bins"] + 1)
  
            fig, ax = plt.subplots(2, 1, gridspec_kw = {"height_ratios" : [3, 1]}, figsize = (8,6))
            ax[0].set_xlim([plot_features[i]["xaxis_start"], plot_features[i]["xaxis_end"]])
            ax[1].set_xlim([plot_features[i]["xaxis_start"], plot_features[i]["xaxis_end"]])



            #Start with the variables

            #Vector variables need to be indexed as for each event they are sorted by pT. Let n be this index.
            #The base energy scale is MeV, hence variables with units of energy need will be divided by 1000 to get GeV
            if ("n" in plot_features[i]) and ("divide" in plot_features[i]):

                datas_array = datas_dict["data"][plot_features[i]["name"]][:, plot_features[i]["n"] -1] / plot_features[i]["divide"]

                backgrounds_merged_array = ak.concatenate([backgrounds_dict[x] for x in backgrounds_dict], axis=0)[plot_features[i]["name"]][:, plot_features[i]["n"] - 1] / [plot_features[i]["divide"]]
                backgrounds_array = [x for x in [backgrounds_dict[x][plot_features[i]["name"]][:, plot_features[i]["n"] - 1] / [plot_features[i]["divide"]] for x in backgrounds_dict][::-1]]

            elif "n" in plot_features[i]:

                datas_array = datas_dict["data"][plot_features[i]["name"]][:, plot_features[i]["n"] -1]

                backgrounds_merged_array = ak.concatenate([backgrounds_dict[x] for x in backgrounds_dict], axis=0)[plot_features[i]["name"]][:, plot_features[i]["n"] - 1]
                backgrounds_array = [x for x in [backgrounds_dict[x][plot_features[i]["name"]][:, plot_features[i]["n"] - 1] for x in backgrounds_dict][::-1]]

            elif "divide" in plot_features[i]:

                datas_array = datas_dict["data"][plot_features[i]["name"]] / [plot_features[i]["divide"]]

                backgrounds_merged_array = ak.concatenate([backgrounds_dict[x] for x in backgrounds_dict], axis=0)[plot_features[i]["name"]] / [plot_features[i]["divide"]]
                backgrounds_array = backgrounds_array = [x for x in [backgrounds_dict[x][plot_features[i]["name"]] / [plot_features[i]["divide"]] for x in backgrounds_dict][::-1]]

            else:

                datas_array = datas_dict["data"][plot_features[i]["name"]]
                backgrounds_merged_array = ak.concatenate([backgrounds_dict[x] for x in backgrounds_dict], axis=0)[plot_features[i]["name"]]
                backgrounds_array = [x for x in [backgrounds_dict[x][plot_features[i]["name"]]for x in backgrounds_dict][::-1]]

            
            #Start with the plotting - we need data, a total background - which is the sum of all backgrounds, and then all background separately
            n_datas, bins_datas, patches_datas = ax[0].hist(datas_array, weights = datas_dict["data"].weight, bins = bins, alpha = 0)

            n_backgrounds_merged, bins_backgrounds_merged, patches_backgrounds_merged = ax[0].hist(backgrounds_merged_array, weights = ak.concatenate([backgrounds_dict[x] for x in backgrounds_dict], axis = 0).weight, bins = bins, histtype = "step", stacked = True, color = "red", linewidth = 2)

            ax[0].hist(backgrounds_array, weights = [x for x in [backgrounds_dict[x].weight for x in backgrounds_dict][::-1]], bins = bins, histtype = "bar", stacked = True, label = labels_background[::-1], color = color)

            #Define the statistical error, while keeping track of bins with zero yield:
            sigma = np.where(n_backgrounds_merged > 0, np.sqrt(n_backgrounds_merged) / n_backgrounds_merged, 0)
            sigma_masked = np.ma.masked_where(n_backgrounds_merged == 0, sigma)

            bins_center = bins[:-1] + 0.5 * (bins[1:] - bins[:-1])

            #ATLAS data 
            ax[0].scatter(bins_center, n_datas, marker = "o", c = "black", s = 40, alpha = 1, label = labels_data[0], zorder = 2)

            #Data/MC ratio
            line = ax[1].axhline(y = 1, color = "red", linewidth = 2)
            ax[1].scatter(bins_center, n_datas/ n_backgrounds_merged, marker = "o", c = "black", s = 40, alpha = 1, zorder = 2)

            #Errorbars - Needed a good collab with chatGPT to make this last bin work
            ax[1].errorbar(bins_center, n_datas / n_backgrounds_merged, yerr = np.sqrt(n_datas) / n_datas, fmt = ".", c = "black")
            extended_bins = np.append(bins, 2 * bins[-1] - bins[-2])
            extended_sigma_masked = np.append(sigma_masked, sigma_masked[-1])
            fill = ax[1].fill_between(extended_bins[:-1], 1 - extended_sigma_masked, 1 + extended_sigma_masked, step="post", alpha=0.25, color="red")


            #Plot details - touch at own risk
            ymin, ymax = ax[0].get_ylim()
            if scale_type == "linear":
                ax[0].set_ylim([0, ymax * 1.3])
            elif scale_type == "log":
                ax[0].set_ylim([1, ymax * 10])
            else:
                raise Exception("Unsupported scale type, try linear or log")

            ax[0].set_ylabel(plot_features[i]["yaxis_label"], loc = "top", fontsize = 12)
            ax[0].locator_params(nbins = 10, axis = "y")
            ax[0].tick_params(axis = "y", labelsize = 12)

            ax[1].set_ylim([0.7, 1.3])
            ax[1].set_ylabel("Data / SM", fontsize = 12)
            ax[1].set_xlabel(plot_features[i]["xaxis_label"], loc = "right", fontsize = 12)

            fig.align_ylabels([ax[0], ax[1]])

            ax[1].locator_params(nbins = 10, axis = "x")
            ax[1].tick_params(axis = "x", labelsize = 12)
            ax[1].tick_params(axis = "y", labelsize = 12)


            ax[0].set_xticklabels([])

            if scale_type == "linear":
                ax[0].ticklabel_format(axis = "y", style = "sci", scilimits = (0,7))
            elif scale_type == "log":
                ax[0].set_yscale("log")
            else:
                raise Exception("Unsupported scale type, try linear or log")

            plt.subplots_adjust(hspace = 0.1)

            handles, labels = ax[0].get_legend_handles_labels()

            first_legend_handles = [(line, fill),]
            first_legend_labels = [r"SM $\pm 1 \sigma$"]

            
            second_legend_handles = handles 
            second_legend_labels = labels 

            first_legend = ax[0].legend(first_legend_handles, first_legend_labels, bbox_to_anchor = [0.21, 0.8, 1, 0.2], loc = "upper center", prop = {"size": 12}, labelspacing = 0.1, handlelength = 1, handleheight = 1)
            second_legend = ax[0].legend(second_legend_handles[::-1], second_legend_labels[::-1], loc = "upper right", prop = {"size": 12}, labelspacing = 0.1, handlelength = 1, handleheight = 1)
            ax[0].add_artist(first_legend)

            ampl.draw_atlas_label(0.02, 4.1, status = "int")

            if run == "Run2":
                upper_text = r"$\sqrt{s}=13$" + " TeV, 140.1 fb" + r"$^{-1}$" "\n"
            elif run == "Run3":
                upper_text = r"$\sqrt{s}=13.6$" + " TeV, 51.8 fb" + r"$^{-1}$" "\n"
            else:
                raise Exception("Run4 has not started yet")

            middle_text = "\n"

            if subject == "W(tautau)":
                subject = r"$W(\tau\tau)$"
            elif subject == "Z(nunu)":
                subject = r"$Z(\nu\nu)$"
            elif subject == "Z(tautau)":
                subject = r"$Z(\tau\tau)$"

            if sub_subject == "true-tau":
                sub_subject = r"$true-\tau$"
            if sub_subject == "fake-tau":
                sub_subject = r"$fake-\tau$"

            if region == "Preselection":
                lower_text = "Preselection"
            elif sub_subject:
                lower_text = f"{subject} {sub_subject} {region}"
            else:
                lower_text = f"{subject} {region}"

            full_text = upper_text + middle_text + lower_text

            ax[0].text(0.33, 0.80, full_text, fontsize=12, transform=ax[0].transAxes)

            pdf.savefig(bbox_inches = "tight")

            




        

            




    