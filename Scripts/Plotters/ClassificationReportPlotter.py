import numpy as np
import pandas as pd

import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns

import matplotlib.ticker as ticker


def classification_report_plotter(algorithm_type, y_test, predictions, labels):
    """
    Classification report plotting function. Parameters:
      * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
      * y_test: [series] - (dependent),
      * predictions: [series] - (dependent),
      * labels: [list] - (dependent).
    """

    if algorithm_type == "regression":
        print("Not available for regression.")

    elif (algorithm_type == "binary-classification") or (
        algorithm_type == "multiclass-classification"
    ):
        from sklearn.metrics import classification_report

        clf_report = classification_report(
            y_test,
            predictions,
            labels=range(len(labels)),
            target_names=labels,
            output_dict=True,
        )

        fig, ax = plt.subplots(
            1, 2, figsize=(16, 8), gridspec_kw={"width_ratios": [4, 1]}
        )

        sns.heatmap(
            pd.DataFrame(clf_report).iloc[:-1, :].T * 100,
            ax=ax[0],
            annot=True,
            cmap="Greens",
            linewidth=0.5,
            linecolor="black",
            fmt=".2f",
            annot_kws={"va": "center_baseline"},
            cbar=True,
            # vmin=0,
            vmax=100,
            cbar_kws={"format": "%.0f\%%"},
        )

        ax[0].hlines(
            ax[0].get_ylim()[0],
            ax[0].get_xlim()[0],
            ax[0].get_xlim()[1],
            color="black",
            lw=10,
        )
        ax[0].hlines(
            ax[0].get_ylim()[1],
            ax[0].get_xlim()[0],
            ax[0].get_xlim()[1],
            color="black",
            lw=10,
        )
        ax[0].hlines(
            [len(labels)], ax[0].get_xlim()[0], ax[0].get_xlim()[1], color="black", lw=5
        )
        ax[0].hlines(
            [len(labels) + 1],
            ax[0].get_xlim()[0],
            ax[0].get_xlim()[1],
            color="black",
            lw=5,
        )
        ax[0].vlines(
            ax[0].get_xlim()[0],
            ax[0].get_ylim()[0],
            ax[0].get_ylim()[1],
            color="black",
            lw=10,
        )
        ax[0].vlines(
            ax[0].get_xlim()[1],
            ax[0].get_ylim()[0],
            ax[0].get_ylim()[1],
            color="black",
            lw=10,
        )

        ax[0].set_title("Classification Report")

        sns.heatmap(
            pd.DataFrame(clf_report).iloc[-1::, [x for x in (range(len(labels)))]].T,
            ax=ax[1],
            annot=True,
            cmap="Blues",
            linewidth=0.5,
            linecolor="black",
            fmt=".0f",
            annot_kws={"va": "center_baseline"},
            cbar=True,
            cbar_kws={"format": "%.0f"},
        )

        ax[1].hlines(
            ax[1].get_ylim()[0],
            ax[1].get_xlim()[0],
            ax[1].get_xlim()[1],
            color="black",
            lw=10,
        )
        ax[1].hlines(
            ax[1].get_ylim()[1],
            ax[1].get_xlim()[0],
            ax[1].get_xlim()[1],
            color="black",
            lw=10,
        )
        ax[1].vlines(
            ax[1].get_xlim()[0],
            ax[1].get_ylim()[0],
            ax[1].get_ylim()[1],
            color="black",
            lw=10,
        )
        ax[1].vlines(
            ax[1].get_xlim()[1],
            ax[1].get_ylim()[0],
            ax[1].get_ylim()[1],
            color="black",
            lw=10,
        )

        ax[1].set_title("Number of Entries")

        plt.show()

    else:
        raise Exception(
            "Unsupported type of an algorithm. Only 'regression', 'binary-classification' or 'multiclass-classification' allowed."
        )
