import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns


def significance_grid_plotter(
    gg_grid, gg_x_ticks, gg_y_ticks, ss_grid, ss_x_ticks, ss_y_ticks, save, savepath
):
    """
    Balance plotting function. Parameters:
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * df: [dataframe] - (dependent),
        * labels: [list] - (dependent).
    """

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(34, 12))

    # Plotting GG Grid on the left subplot (ax1)
    sns.heatmap(
        gg_grid[::-1],
        ax=ax1,
        xticklabels=gg_x_ticks,
        yticklabels=gg_y_ticks[::-1],
        cmap="coolwarm",
        # linewidths=0.5,
        # linecolor='black',
        square=False,
        annot=True,
        fmt=".2f",
        cbar_kws={"ticks": [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]},
        vmin=0,
        vmax=1,
    )
    ax1.set_title(
        r"$\tilde{g}\tilde{g}$ production, $\tilde{g} \to q q \tau \nu {\tilde{\chi}^{0}_{1}}$ / $q q \tau \tau {\tilde{\chi}^{0}_{1}}$ / $q q \nu \nu {\tilde{\chi}^{0}_{1}}$",
        loc="left",
    )
    ax1.set_xlabel(r"$m_{\tilde{g}}$ [GeV]", loc="right", fontsize=32)
    ax1.set_ylabel(r"$m_{\tilde{\chi}^{0}_{1}}$ [GeV]", loc="top", fontsize=32)

    # Plotting SS Grid on the right subplot (ax2)
    sns.heatmap(
        ss_grid[::-1],
        ax=ax2,
        xticklabels=ss_x_ticks,
        yticklabels=ss_y_ticks[::-1],
        cmap="coolwarm",
        # linewidths=0.5,
        # linecolor='black',
        square=False,
        cbar=True,
        annot=True,
        fmt=".2f",
        cbar_kws={"ticks": [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]},
        vmin=0,
        vmax=1,
    )

    ax2.set_title(
        r"$\tilde{s}\tilde{s}$ production, $\tilde{s} \to q \tau \nu {\tilde{\chi}^{0}_{1}}$ / $q \tau \tau {\tilde{\chi}^{0}_{1}}$ / $q \nu \nu {\tilde{\chi}^{0}_{1}}$",
        loc="left",
    )
    ax2.set_xlabel(r"$m_{\tilde{s}}$ [GeV]", loc="right", fontsize=32)
    ax2.set_ylabel(r"$m_{\tilde{\chi}^{0}_{1}}$ [GeV]", loc="top", fontsize=32)

    ax1.text(
        0.22,
        0.95,
        r"$\sqrt{s}=13$" + " TeV, 140.1 fb" + r"$^{-1}$",
        transform=ax1.transAxes,
    )
    ax2.text(
        0.22,
        0.95,
        r"$\sqrt{s}=13$" + " TeV, 140.1 fb" + r"$^{-1}$",
        transform=ax2.transAxes,
    )

    ampl.draw_atlas_label(0.02, 0.97, status="int", ax=ax1)
    ampl.draw_atlas_label(0.02, 0.97, status="int", ax=ax2)

    plt.tight_layout()  # Adjust spacing between plots for better layout

    if save:
        plt.savefig(savepath)
    else:
        plt.show()
