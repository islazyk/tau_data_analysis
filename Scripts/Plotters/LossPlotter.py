import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns


def loss_plotter(framework_type, model, metric, save, savepath):
    """
    Loss plotting function. Parameters:
        * framework_type: [string] - ('XGBoost', 'PyTorch').
        * model: [xgboost model or pytorch list] - (dependent),
        * metric: [string] - (dependent).
    """

    fig, ax = plt.subplots(1, 1, figsize=(8, 6))

    if framework_type == "XGBoost":
        ax.plot(model.evals_result()["validation_0"][metric], label="Training loss")
        ax.plot(model.evals_result()["validation_1"][metric], label="Validation loss")
        ax.axvline(
            model.best_ntree_limit,
            color="blue",
            label=f"Optimal tree number: {model.best_ntree_limit}",
        )
        ax.set_xlabel("Number of Trees")
    elif framework_type == "PyTorch":
        ax.plot(model["training_loss_history"], label="Training loss")
        ax.plot(model["validation_loss_history"], label="Validation loss")
        ax.axvline(
            model["early_stopping_rounds"],
            color="blue",
            label=f"Optimal epoch number: {model['early_stopping_rounds']}",
        )
        ax.set_xlabel("Number of Epochs")
    else:
        raise Exception(
            "Unsupported type of a framework. Only 'XGBoost or 'PyTorch' allowed."
        )

    ax.set_ylabel("Loss")

    ax.legend(
        loc="upper right",
        prop={"size": 12},
        labelspacing=0.1,
        handlelength=1,
        handleheight=1,
    )

    ax.set_title("Loss Plot")

    # ---ATLAS Label---
    ampl.draw_atlas_label(0.02, 0.97)
    # ---ATLAS Label---
    if save:
        plt.savefig(savepath)
    else:
        plt.show()
