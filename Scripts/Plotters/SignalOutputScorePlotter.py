import awkward as ak
import numpy as np

import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.backends.backend_pdf import PdfPages


def signal_output_score_plotter(
    output,
    plot_type,
    bins,
    weighted,
    scale_type,
    save,
    savepath,
    threshold=None,
    reverse=None,
):
    y_probs_signal = output["output"]["X_test"]["output_score_signal"]
    y_true = output["output"]["X_test"]["y_true"]
    weights = output["output"]["X_test"]["weight"].values

    bin_size = 1 / bins
    n_classes = len(np.unique(output["output"]["X_test"]["y_true"]))
    bins = np.linspace(0, 1, bins + 1)

    if plot_type == "binary":
        classes_array = [
            y_probs_signal[np.isin(y_true, list(range(0, n_classes - 1)))],
            y_probs_signal[y_true == n_classes - 1],
        ]
        weights_array = [
            weights[np.isin(y_true, list(range(0, n_classes - 1)))],
            weights[y_true == n_classes - 1],
        ]
        n_colors = 2
        labels = ["background", "signal"]
    elif plot_type == "multiclass":
        classes_array = [y_probs_signal[y_true == i] for i in range(n_classes)]
        weights_array = [weights[y_true == i] for i in range(n_classes)]
        n_colors = n_classes
        labels = output["labels"]
    else:
        raise Exception("Unsupported plot type. Only 'binary' or 'multiclass' allowed.")

    color = sns.color_palette("Set3", n_colors=n_colors)

    if reverse:
        classes_array = classes_array[::-1]
        weights_array = weights_array[::-1]
        labels = labels[::-1]
        color = color[::-1]

    fig, ax = plt.subplots(1, 1, figsize=(8, 6))

    if weighted:
        ax.hist(
            classes_array,
            bins=bins,
            weights=weights_array,
            histtype="bar",
            stacked=True,
            label=labels,
            color=color[::-1],
        )
    else:
        ax.hist(
            classes_array,
            bins=bins,
            histtype="bar",
            stacked=True,
            label=labels,
            color=color[::-1],
        )
    if threshold:
        ax.axvline(threshold[0], color="red", label=f"CR threshold: {threshold[0]}")
        ax.axvline(threshold[1], color="blue", label=f"VR threshold: {threshold[1]}")
        ax.axvline(threshold[2], color="green", label=f"SR threshold: {threshold[2]}")

        ax.axvspan(threshold[0], threshold[1], facecolor="red", alpha=0.2)
        ax.axvspan(threshold[1], threshold[2], facecolor="blue", alpha=0.2)
        ax.axvspan(threshold[2], 1, facecolor="green", alpha=0.2)

        ax.text(
            (threshold[0] + threshold[1]) / 2,
            0.5,
            "CR",
            fontsize=20,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )
        ax.text(
            (threshold[1] + threshold[2]) / 2,
            0.5,
            "VR",
            fontsize=20,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )
        ax.text(
            threshold[2] + (1 - threshold[2]) / 2,
            0.5,
            "SR",
            fontsize=20,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )

    ax.set_xlabel(
        "Signal Output Score",
        fontsize=12,
        loc="right",
    )
    ax.set_ylabel(
        f"Events / {bin_size}",
        fontsize=12,
        loc="top",
    )

    ax.set_xlim([0, 1])

    ymin, ymax = ax.get_ylim()

    if scale_type == "linear":
        ax.set_ylim([0, ymax * 1.3])
        ax.ticklabel_format(axis="y", style="sci", scilimits=(0, 8))
        ax.locator_params(nbins=10, axis="y")
    elif scale_type == "logarithmic":
        ax.set_yscale("log")
        ax.set_ylim([1, ymax * 10])
    else:
        raise Exception("Unsupported scale type.")

    ax.locator_params(nbins=10, axis="x")
    ax.tick_params(axis="y", labelsize=12)
    ax.tick_params(axis="x", labelsize=12)

    ax.legend(
        loc="upper right",
        prop={"size": 12},
        labelspacing=0.1,
        handlelength=1,
        handleheight=1,
    )

    ampl.draw_atlas_label(0.02, 0.97, status="int")

    if save:
        plt.savefig(savepath)

    # plt.tight_layout()
    # plt.show()
