import awkward as ak
import numpy as np

import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.backends.backend_pdf import PdfPages

import scipy


def region_plotter(
    run,
    release,
    analysis_base,
    campaigns,
    architecture,
    region,
    channel,
    subject,
    sub_subject,
    plot_features,
    include_data,
    datas_dict,
    include_background,
    backgrounds_dict,
    include_truefaketau,
    truefaketaus_dict,
    include_signal,
    gluinos_dict,
    squarks_dict,
    labels_background=None,
    labels_gluino=None,
    labels_squark=None,
    scale_type="linear",
    scale_factor=1,
):
    """
    Analysis plotting function. Parameters:
        * run: [string] - ('Run3', 'Run2'),
        * release: [string]- ('R22', 'R21', 'SUSY2016'),
        * analysis_base: [string] - (dependent),
        * campaigns: [list] - (dependent),
        * architecture: [string] ('RNN', 'DeepSet'),
        * region: [string] - ('Preselection', 'SR', 'CR', 'VR'),
        * channel: [string] - ('1', '2'),
        * subject: [string] - (dependent),
        * sub_subject: [string] - (dependent),
        * plot_features: [list] - (dependent),
        * include_data: [boolean] - (True, False),
        * datas_dict: [dictionary] - (dependent),
        * include_background: [boolean] - (True, False),
        * backgrounds_dict: [dictionary] - (dependent),
        * include_truefaketau: [boolean] - (True, False),
        * truefaketaus_dict: [dictionary] - (dependent),
        * include_signal: [boolean] - (True, False),
        * gluinos_dict: [dictionary] - (dependent),
        * squarks_dict: [dictionary] - (dependent),
        * labels_background: [list] - (dependent),
        * labels_gluino: [list] - (dependent),
        * labels_squark: [list] - (dependent),
        * scale_type: [string] - ('linear', 'logarithmic'),
        * scale_factor: [number] - (dependent).

    """

    # ---Colors---
    color = sns.color_palette("Set3", n_colors=len(backgrounds_dict.keys()))

    if include_signal:
        color_gluinos = sns.color_palette("rocket", n_colors=len(gluinos_dict.keys()))
        color_squarks = sns.color_palette("dark", n_colors=len(squarks_dict.keys()))
    # ---Colors---

    with PdfPages(
        f"../Plots/{run}_{release}_{analysis_base}_{campaigns}_{architecture}_{region}_{channel}_{subject}_{sub_subject}.pdf"
    ) as pdf:
        for i in range(0, len(plot_features)):
            # ---Plots & Limits---
            bins = np.linspace(
                plot_features[i]["xaxis_start"],
                plot_features[i]["xaxis_end"],
                plot_features[i]["bins"] + 1,
            )
            if include_data:
                fig, ax = plt.subplots(
                    2, 1, gridspec_kw={"height_ratios": [3, 1]}, figsize=(8, 6)
                )
                ax[0].set_xlim(
                    [plot_features[i]["xaxis_start"], plot_features[i]["xaxis_end"]]
                )
                ax[1].set_xlim(
                    [plot_features[i]["xaxis_start"], plot_features[i]["xaxis_end"]]
                )
            else:
                fig, ax = plt.subplots(1, 1, figsize=(8, 6))
                ax.set_xlim(
                    [plot_features[i]["xaxis_start"], plot_features[i]["xaxis_end"]]
                )
            # ---Plots & Limits---

            # ---Features---
            if ("n" in plot_features[i]) and ("divide" in plot_features[i]):
                if include_data:
                    datas_array = datas_dict["data"][plot_features[i]["name"]][
                        :, plot_features[i]["n"] - 1
                    ] / [plot_features[i]["divide"]]
                if include_background:
                    backgrounds_merged_array = np.concatenate(
                        [backgrounds_dict[x] for x in backgrounds_dict], axis=0
                    )[plot_features[i]["name"]][:, plot_features[i]["n"] - 1] / [
                        plot_features[i]["divide"]
                    ]
                    backgrounds_array = [
                        x
                        for x in [
                            backgrounds_dict[x][plot_features[i]["name"]][
                                :, plot_features[i]["n"] - 1
                            ]
                            / [plot_features[i]["divide"]]
                            for x in backgrounds_dict
                        ][::-1]
                    ]
                if include_truefaketau:
                    truefaketaus_array = [
                        x
                        for x in [
                            truefaketaus_dict[x][plot_features[i]["name"]][
                                :, plot_features[i]["n"] - 1
                            ]
                            / [plot_features[i]["divide"]]
                            for x in truefaketaus_dict
                        ][::-1]
                    ]
                if include_signal:
                    gluinos_array = [
                        x
                        for x in [
                            gluinos_dict[x][plot_features[i]["name"]][
                                :, plot_features[i]["n"] - 1
                            ]
                            / [plot_features[i]["divide"]]
                            for x in gluinos_dict
                        ][::-1]
                    ]
                    squarks_array = [
                        x
                        for x in [
                            squarks_dict[x][plot_features[i]["name"]][
                                :, plot_features[i]["n"] - 1
                            ]
                            / [plot_features[i]["divide"]]
                            for x in squarks_dict
                        ][::-1]
                    ]
            elif "n" in plot_features[i]:
                if include_data:
                    datas_array = datas_dict["data"][plot_features[i]["name"]][
                        :, plot_features[i]["n"] - 1
                    ]
                if include_background:
                    backgrounds_merged_array = np.concatenate(
                        [backgrounds_dict[x] for x in backgrounds_dict], axis=0
                    )[plot_features[i]["name"]][:, plot_features[i]["n"] - 1]
                    backgrounds_array = [
                        x
                        for x in [
                            backgrounds_dict[x][plot_features[i]["name"]][
                                :, plot_features[i]["n"] - 1
                            ]
                            for x in backgrounds_dict
                        ][::-1]
                    ]
                if include_truefaketau:
                    truefaketaus_array = [
                        x
                        for x in [
                            truefaketaus_dict[x][plot_features[i]["name"]][
                                :, plot_features[i]["n"] - 1
                            ]
                            for x in truefaketaus_dict
                        ][::-1]
                    ]
                if include_signal:
                    gluinos_array = [
                        x
                        for x in [
                            gluinos_dict[x][plot_features[i]["name"]][
                                :, plot_features[i]["n"] - 1
                            ]
                            for x in gluinos_dict
                        ][::-1]
                    ]
                    squarks_array = [
                        x
                        for x in [
                            squarks_dict[x][plot_features[i]["name"]][
                                :, plot_features[i]["n"] - 1
                            ]
                            for x in squarks_dict
                        ][::-1]
                    ]
            elif "divide" in plot_features[i]:
                if include_data:
                    datas_array = datas_dict["data"][plot_features[i]["name"]] / [
                        plot_features[i]["divide"]
                    ]
                if include_background:
                    backgrounds_merged_array = np.concatenate(
                        [backgrounds_dict[x] for x in backgrounds_dict], axis=0
                    )[plot_features[i]["name"]] / [plot_features[i]["divide"]]
                    backgrounds_array = [
                        x
                        for x in [
                            backgrounds_dict[x][plot_features[i]["name"]]
                            / [plot_features[i]["divide"]]
                            for x in backgrounds_dict
                        ][::-1]
                    ]
                if include_truefaketau:
                    truefaketaus_array = [
                        x
                        for x in [
                            truefaketaus_dict[x][plot_features[i]["name"]]
                            / [plot_features[i]["divide"]]
                            if len(truefaketaus_dict[x]) != 0
                            else truefaketaus_dict[x][plot_features[i]["name"]]
                            for x in truefaketaus_dict
                        ][::-1]
                    ]
                if include_signal:
                    gluinos_array = [
                        x
                        for x in [
                            gluinos_dict[x][plot_features[i]["name"]]
                            / [plot_features[i]["divide"]]
                            for x in gluinos_dict
                        ][::-1]
                    ]
                    squarks_array = [
                        x
                        for x in [
                            squarks_dict[x][plot_features[i]["name"]]
                            / [plot_features[i]["divide"]]
                            for x in squarks_dict
                        ][::-1]
                    ]
            else:
                if include_data:
                    datas_array = datas_dict["data"][plot_features[i]["name"]]
                if include_background:
                    backgrounds_merged_array = np.concatenate(
                        [backgrounds_dict[x] for x in backgrounds_dict], axis=0
                    )[plot_features[i]["name"]]
                    backgrounds_array = [
                        x
                        for x in [
                            backgrounds_dict[x][plot_features[i]["name"]]
                            for x in backgrounds_dict
                        ][::-1]
                    ]
                if include_truefaketau:
                    truefaketaus_array = [
                        x
                        for x in [
                            truefaketaus_dict[x][plot_features[i]["name"]]
                            for x in truefaketaus_dict
                        ][::-1]
                    ]
                if include_signal:
                    gluinos_array = [
                        x
                        for x in [
                            gluinos_dict[x][plot_features[i]["name"]]
                            for x in gluinos_dict
                        ][::-1]
                    ]
                    squarks_array = [
                        x
                        for x in [
                            squarks_dict[x][plot_features[i]["name"]]
                            for x in squarks_dict
                        ][::-1]
                    ]
            # ---Features---

            axis = ax[0] if include_data else ax

            # ---Signals---
            if include_signal:
                # ---Gluinos---
                axis.hist(
                    gluinos_array,
                    weights=[
                        x for x in [gluinos_dict[x].weight for x in gluinos_dict][::-1]
                    ],
                    bins=bins,
                    histtype="step",
                    label=labels_gluino[::-1],
                    color=color_gluinos,
                    linestyle="dashed",
                    linewidth=2,
                )
                # ---Gluinos---

                # ---Squarks---
                axis.hist(
                    squarks_array,
                    weights=[
                        x for x in [squarks_dict[x].weight for x in squarks_dict][::-1]
                    ],
                    bins=bins,
                    histtype="step",
                    label=labels_squark[::-1],
                    color=color_squarks,
                    linestyle="dashed",
                    linewidth=2,
                )
                # ---Squarks---
            # ---Signals---

            if include_data:
                # ---Datas (invisible)---
                n_datas, bins_datas, patches_datas = ax[0].hist(
                    datas_array,
                    weights=datas_dict["data"].weight,
                    bins=bins,
                    alpha=0,
                )
            # ---Datas (invisible)---

            # ---Backgrounds---
            # ---Merged Backgrounds (red)---
            (
                n_backgrounds_merged,
                bins_backgrounds_merged,
                patches_backgrounds_merged,
            ) = axis.hist(
                backgrounds_merged_array,
                weights=np.concatenate(
                    [backgrounds_dict[x] for x in backgrounds_dict], axis=0
                ).weight
                * scale_factor,
                bins=bins,
                histtype="step",
                stacked=True,
                color="red",
                linewidth=2,
            )
            # ---Merged Backgrounds (red)---

            # ---Stacked Backgrounds---
            axis.hist(
                backgrounds_array,
                weights=[
                    x
                    for x in [
                        backgrounds_dict[x].weight * scale_factor
                        for x in backgrounds_dict
                    ][::-1]
                ],
                bins=bins,
                histtype="bar",
                stacked=True,
                label=labels_background[::-1],
                color=color,
            )
            # ---Stacked Backgrounds---

            if include_truefaketau:
                # ---True & Fake Taus---
                n_truefaketaus, bins_truefaketaus, patches_truefaketaus = axis.hist(
                    truefaketaus_array,
                    weights=[
                        x
                        for x in [
                            truefaketaus_dict[x].weight * scale_factor
                            for x in truefaketaus_dict
                        ][::-1]
                    ],
                    bins=bins,
                    histtype="bar",
                    stacked=True,
                )

                plt.setp(
                    [x for i, x in enumerate(patches_truefaketaus) if i % 2 == 1],
                    alpha=0,
                )

                plt.setp(
                    [x for i, x in enumerate(patches_truefaketaus) if i % 2 == 0],
                    facecolor="none",
                    edgecolor="black",
                    fill=False,
                    hatch="/////",
                    alpha=1,
                    linewidth=0.0,
                )

                legend_truefaketaus = mpatches.Patch(
                    facecolor="none",
                    edgecolor="black",
                    fill=False,
                    hatch="/////",
                    alpha=1,
                    linewidth=0.0,
                )
                # ---True & Fake Taus---
            # ---Backgrounds---

            if include_data:
                # ---Tweak (0 to None)---
                n_by_zero = n_datas / n_datas
                n_datas = n_datas * n_by_zero
                # ---Tweak (0 to None)---

                # ---Standard Deviation Uncertainty (Not Working?)---
                std_unweighted = scipy.stats.binned_statistic(
                    backgrounds_merged_array,
                    backgrounds_merged_array,
                    statistic="std",
                    bins=bins,
                )[0]
                sqrtn_unweighted = np.sqrt(
                    np.histogram(backgrounds_merged_array, bins=bins)[0]
                )
                sigma = std_unweighted / sqrtn_unweighted
                # ---Standard Deviation Uncertainty (Not Working?)---

            # ---Bins Center---
            bins_center = bins[:-1] + 0.5 * (bins[1:] - bins[:-1])
            # ---Bins Center---

            if include_data:
                # ---ATLAS Data---
                ax[0].scatter(
                    bins_center,
                    n_datas,
                    marker="o",
                    c="black",
                    s=40,
                    alpha=1,
                    label="Data",
                    zorder=2,
                )
                # ---ATLAS Data---

                # ---ATLAS/MC Data---
                line = ax[1].axhline(y=1, color="red", linewidth=2)
                ax[1].scatter(
                    bins_center,
                    n_datas / n_backgrounds_merged,
                    marker="o",
                    c="black",
                    s=40,
                    alpha=1,
                    zorder=2,
                )
                # ---ATLAS/MC Data---

                # ---Errors---
                ax[1].errorbar(
                    # bins[:-1] + 0.5*(bins[1:] - bins[:-1]),
                    bins_center,
                    n_datas / n_backgrounds_merged,
                    yerr=np.sqrt(n_datas) / n_datas,
                    fmt=".",
                    c="black",
                )
                fill = ax[1].fill_between(
                    bins[:-1], 1 - sigma, 1 + sigma, step="mid", alpha=0.25, color="red"
                )
                # ---Errors---

            # ---Axes Ranges---
            ymin, ymax = axis.get_ylim()

            if scale_type == "linear":
                axis.set_ylim([0, ymax * 1.3])
            elif scale_type == "logarithmic":
                axis.set_ylim([1, ymax * 10])
            else:
                raise Exception("Unsupported scale type.")

            if include_data:
                ax[1].set_ylim([0.7, 1.3])
            # ax[0].set_xlim([plot_features[i]['xaxis_start'], plot_features[i]['xaxis_end']])
            # ---Axes Ranges---

            # ---Axes Labels & Spacing & Format---
            axis.set_ylabel(plot_features[i]["yaxis_label"], loc="top", fontsize=12)
            axis.locator_params(nbins=10, axis="y")
            axis.tick_params(axis="y", labelsize=12)

            if include_data:
                ax[1].set_ylabel("Data / SM", fontsize=12)
                ax[1].set_xlabel(
                    plot_features[i]["xaxis_label"], loc="right", fontsize=12
                )

                fig.align_ylabels([ax[0], ax[1]])

                ax[1].locator_params(nbins=10, axis="x")

                ax[1].tick_params(axis="x", labelsize=12)
                ax[1].tick_params(axis="y", labelsize=12)
            else:
                axis.set_xlabel(
                    plot_features[i]["xaxis_label"], loc="right", fontsize=12
                )

                axis.locator_params(nbins=10, axis="x")
                axis.tick_params(axis="x", labelsize=12)

            # ax[0].tick_params('y', length=20, width=1, which='major')
            # ax[0].tick_params('y', length=10, width=1, which='minor')

            if include_data:
                axis.set_xticklabels([])

            if scale_type == "linear":
                axis.ticklabel_format(axis="y", style="sci", scilimits=(0, 7))
            elif scale_type == "logarithmic":
                axis.set_yscale("log")
            else:
                raise Exception("Unsupported scale type.")

            # ---Axes Labels & Spacing & Format---

            # ---Spacing Between Plots---
            plt.subplots_adjust(hspace=0.1)
            # ---Spacing Between Plots---

            # ---Legend---
            handles, labels = axis.get_legend_handles_labels()

            if include_data:
                if include_truefaketau:
                    first_legend_handles = [
                        (line, fill),
                        legend_truefaketaus,
                    ]

                    first_legend_labels = [
                        r"SM $\pm 1 \sigma$",
                        r"Fake $\tau$",
                    ]
                else:
                    first_legend_handles = [
                        (line, fill),
                    ]

                    first_legend_labels = [
                        r"SM $\pm 1 \sigma$",
                    ]
            else:
                if include_truefaketau:
                    first_legend_handles = [
                        legend_truefaketaus,
                    ]

                    first_legend_labels = [
                        r"Fake $\tau$",
                    ]
                else:
                    first_legend_handles = []

                    first_legend_labels = []

            second_legend_handles = handles
            second_legend_labels = labels

            if include_signal:
                first_legend_handles.extend(
                    handles[0 : len(gluinos_dict) + len(squarks_dict)]
                )
                first_legend_labels.extend(
                    labels[0 : len(gluinos_dict) + len(squarks_dict)]
                )

                second_legend_handles = handles[len(gluinos_dict) + len(squarks_dict) :]
                second_legend_labels = labels[len(gluinos_dict) + len(squarks_dict) :]

            first_legend = axis.legend(
                first_legend_handles,
                first_legend_labels,
                bbox_to_anchor=[0.21, 0.8, 1, 0.2],
                loc="upper center",
                prop={"size": 12},
                labelspacing=0.1,
                handlelength=1,
                handleheight=1,
            )
            second_legend = axis.legend(
                second_legend_handles[::-1],
                second_legend_labels[::-1],
                loc="upper right",
                prop={"size": 12},
                labelspacing=0.1,
                handlelength=1,
                handleheight=1,
            )
            axis.add_artist(first_legend)
            # ---Legend---

            # ---ATLAS Label---
            if include_data:
                ampl.draw_atlas_label(0.02, 4.1, status="int")
            else:
                ampl.draw_atlas_label(0.02, 0.97, status="int")
            # ---ATLAS Label---

            # ---Caption---
            if run == "Run3":
                upper_text = r"$\sqrt{s}=13.6$" + " TeV, 29.0 fb" + r"$^{-1}$" "\n"
            elif run == "Run2":
                upper_text = r"$\sqrt{s}=13$" + " TeV, 140.1 fb" + r"$^{-1}$" "\n"
            else:
                pass

            if channel:
                middle_text = f"{channel}" + r"$\tau$ " + "channel" + "\n"
            else:
                middle_text = "\n"

            if subject == "W(tautau)":
                subject = r"$W(\tau\tau)$"
            elif subject == "Z(nunu)":
                subject = r"$Z(\nu\nu)$"
            elif subject == "Z(tautau)":
                subject = r"$Z(\tau\tau)$"

            if sub_subject == "true-tau":
                sub_subject = r"$true-\tau$"
            if sub_subject == "fake-tau":
                sub_subject = r"$fake-\tau$"
            elif sub_subject == "ETmiss":
                sub_subject = r"$E_{T}^{miss}$"
            elif sub_subject == "HT":
                sub_subject = r"$H_{T}$"
            elif sub_subject == "mTtau":
                sub_subject = r"$m_{T}^{\tau}$"

            if region == "Preselection":
                lower_text = "Preselection"
            elif sub_subject:
                lower_text = f"{subject} {sub_subject} {region}"
            else:
                lower_text = f"{subject} {region}"

            full_text = upper_text + middle_text + lower_text

            if include_data:
                axis.text(0.33, 0.80, full_text, fontsize=12, transform=axis.transAxes)
            else:
                axis.text(0.33, 0.85, full_text, fontsize=12, transform=axis.transAxes)
            # ---Caption---

            # ---Tight Layout---
            pdf.savefig(bbox_inches="tight")
            # ---Tight Layout---

            # ---Just One Plot in JupyterNotebook---
            # if i == 0:
            #    plt.show()
            # else:
            #    plt.close()
            # ---Just One Plot in JupyterNotebook---
