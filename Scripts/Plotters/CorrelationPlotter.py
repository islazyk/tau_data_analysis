import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns


def correlation_plotter(df, y, direction, n_features, algorithm_type):
    """
    Correlation plotting function. Parameters:
        * df: [dataframe] - (dependent),
        * y: [series] - (dependent),
        * direction: [string] - ('vertical', 'horizontal'),
        * n_features: [number] - (dependent).
    """

    if algorithm_type == 'binary-classification':

        correlations = df.corr()[y.name][0 : n_features + 1].sort_values(ascending=False)[
            1:
        ]

        if direction == "vertical":
            fig, ax = plt.subplots(1, 1, figsize=((n_features + 1) * 0.4, 10))
            correlations.plot(kind="bar")
            ax.set_xlabel("Features")
            ax.set_ylabel("Correlation Score")
            ax.ticklabel_format(axis="y", style="sci", scilimits=(-3, 5))

        elif direction == "horizontal":
            fig, ax = plt.subplots(1, 1, figsize=(8, (n_features + 1) * 0.5))
            correlations.plot(kind="barh")
            plt.ylim(-1, len(correlations) + 0.05 * len(correlations))
            ax.set_xlabel("Feature Correlation Score")
            ax.set_ylabel("Features")
            ax.ticklabel_format(axis="x", style="sci", scilimits=(-3, 5))

        else:
            raise Exception(
                "Unsupported type of a direction. Only 'vertical' or 'horizontal' allowed."
            )
    
    elif algorithm_type == 'multiclass-classification':

        plt.figure(figsize=(15, 8))
        for class_label in sorted(y.unique()):
            class_data = df[y == class_label].iloc[:, :n_features].corr()
            sns.heatmap(data=class_data, annot=True, cmap='coolwarm', fmt=".2f", vmin=-1, vmax=1)
            plt.title(f"Feature Correlation Heatmap - Class {class_label}")

    ax.set_title("Feature Correlation Plot")

    # ---ATLAS Label---
    ampl.draw_atlas_label(0.02, 0.98)
    # ---ATLAS Label---
