import numpy as np

import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns


def balance_plotter(algorithm_type, df, labels):
    """
    Balance plotting function. Parameters:
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * df: [dataframe] - (dependent),
        * labels: [list] - (dependent).
    """

    if algorithm_type == "regression":
        print("Not available for regression.")

    elif (algorithm_type == "binary-classification") or (
        algorithm_type == "multiclass-classification"
    ):
        if algorithm_type == "binary-classification":
            bins = [-0.25, 0.25, 0.5, 0.75, 1.25]
        elif algorithm_type == "multiclass-classification":
            bins = np.linspace(-0.25, len(labels) - 0.75, len(labels) * 2)
        else:
            raise Exception("Bins could not be determined.")

        fig, ax = plt.subplots(1, 2, figsize=(24, 6))  # (36, 6)

        sns.histplot(data=df, ax=ax[0], x="class", bins=bins)

        sns.histplot(data=df, ax=ax[1], x="class", weights="class_weight", bins=bins)

        ax[0].set_xlim([bins[0] - 0.25, bins[-1] + 0.25])
        ax[1].set_xlim([bins[0] - 0.25, bins[-1] + 0.25])

        ax[0].ticklabel_format(axis="y", style="sci", scilimits=(0, 9))
        ax[1].ticklabel_format(axis="y", style="sci", scilimits=(0, 9))

        ax[0].set_title("Unweighted Countplot")
        ax[1].set_title("Weighted Countplot")

        ax[0].set_xticks(range(len(np.unique(df["class"]))))
        ax[1].set_xticks(range(len(np.unique(df["class"]))))

        ax[0].set_xticklabels(labels, fontsize=14)
        ax[1].set_xticklabels(labels, fontsize=14)

        plt.subplots_adjust(wspace=0.2)

        # ---ATLAS Label---
        ampl.draw_atlas_label(-1.18, 0.98)
        ampl.draw_atlas_label(0.02, 0.98)
        # ---ATLAS Label---

        plt.show()

    else:
        raise Exception(
            "Unsupported type of an algorithm. Only 'binary-classification' or 'multiclass-classification' allowed."
        )
