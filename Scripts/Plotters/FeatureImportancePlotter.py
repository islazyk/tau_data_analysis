import numpy as np

import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns


def feature_importance_plotter(
    framework_type=None,
    model=None,
    X_train=None,
    X_test=None,
    direction=None,
    shap=None,
    classes=None,
    shap_values=None,
    n_features=None,
    columns=None,
    labels=None,
):
    """
    Feature importance plotting function. Parameters:
        * framework_type: [string] - ('XGBoost', 'PyTorch').
        * model: [xgboost model or pytorch df] - (dependent),
        * X_train: [dataframe] - (dependent),
        * direction: [string] - ('vertical', 'horizontal'),
        * n_features: [number] - (dependent).
    """

    if shap == "No-SHAP":
        if classes == "as_one":
            if framework_type == "XGBoost":
                importances = model.feature_importances_
                indices = np.argsort(importances)

                if direction == "vertical" and n_features == None:
                    fig, ax = plt.subplots(1, 1, figsize=(8, 10))
                    ax.bar(range(len(importances)), importances[indices])
                    plt.xlim(0, len(importances) + 0.1 * len(importances))
                    ax.set_xlabel("Features")
                    ax.set_ylabel("Feature Importance Score")
                    ax.ticklabel_format(axis="y", style="sci", scilimits=(-3, 5))
                elif direction == "horizontal" and n_features == None:
                    fig, ax = plt.subplots(1, 1, figsize=(8, 10))
                    ax.barh(range(len(importances)), importances[indices])
                    plt.ylim(0, len(importances) + 0.1 * len(importances))
                    ax.set_xlabel("Feature Importance Score")
                    ax.set_ylabel("Features")
                    ax.ticklabel_format(axis="x", style="sci", scilimits=(-3, 5))

                elif direction == "vertical" and n_features:
                    fig, ax = plt.subplots(1, 1, figsize=(n_features * 0.4, 10))
                    ax.bar(
                        range(0, n_features),
                        importances[indices][-1 : -n_features - 1 : -1][::-1],
                    )
                    plt.xlim(
                        -1,
                        len(importances[indices][-1 : -n_features - 1 : -1][::-1])
                        + 0.1
                        * len(importances[indices][-1 : -n_features - 1 : -1][::-1]),
                    )
                    ax.set_xticks(range(0, n_features))
                    ax.set_xticklabels(
                        np.array(X_train.columns)[indices][-1 : -n_features - 1 : -1][
                            ::-1
                        ]
                    )
                    plt.xticks(rotation="vertical")
                    ax.set_xlabel("Features")
                    ax.set_ylabel("Feature Importance Score")
                    ax.ticklabel_format(axis="y", style="sci", scilimits=(-3, 5))
                elif direction == "horizontal" and n_features:
                    fig, ax = plt.subplots(1, 1, figsize=(8, n_features * 0.5))
                    ax.barh(
                        range(0, n_features),
                        importances[indices][-1 : -n_features - 1 : -1][::-1],
                    )
                    plt.ylim(
                        -1,
                        len(importances[indices][-1 : -n_features - 1 : -1][::-1])
                        + 0.1
                        * len(importances[indices][-1 : -n_features - 1 : -1][::-1]),
                    )
                    ax.set_yticks(range(0, n_features))
                    ax.set_yticklabels(
                        np.array(X_train.columns)[indices][-1 : -n_features - 1 : -1][
                            ::-1
                        ]
                    )
                    ax.set_xlabel("Feature Importance Score")
                    ax.set_ylabel("Features")
                    ax.ticklabel_format(axis="x", style="sci", scilimits=(-3, 5))
                else:
                    raise Exception(
                        "Unsupported type of a direction. Only 'vertical' or 'horizontal' allowed."
                    )

            elif framework_type == "PyTorch":
                raise Exception(
                    "No-SHAP is not available in PyTorch framework, please use SHAP."
                )

            else:
                raise Exception(
                    "Unsupported type of a framework. Only 'XGBoost' or 'PyTorch' allowed."
                )

        elif classes == "as_many":
            raise Exception("as_many not supported in No-SHAP.")

        else:
            raise Exception(
                "Unsupported type of a bar classes. Only 'as_one' or 'as_many' allowed."
            )

    elif shap == "SHAP":
        if classes == "as_one":
            if (framework_type == "XGBoost") or (framework_type == "PyTorch"):
                if direction == "vertical" and n_features == None:
                    fig, ax = plt.subplots(1, 1, figsize=(8, 10))
                    ax.bar(
                        range(len(model)),
                        model.sort_values("mean_abs_shap", ascending=True)[
                            "mean_abs_shap"
                        ],
                    )
                    plt.xlim(0, len(model) + 0.1 * len(model))
                    ax.set_xlabel("Features")
                    ax.set_ylabel("Feature Importance Score")
                    ax.ticklabel_format(axis="y", style="sci", scilimits=(-3, 5))
                elif direction == "horizontal" and n_features == None:
                    fig, ax = plt.subplots(1, 1, figsize=(8, 10))
                    ax.barh(
                        range(len(model)),
                        model.sort_values("mean_abs_shap", ascending=True)[
                            "mean_abs_shap"
                        ],
                    )
                    plt.ylim(0, len(model) + 0.1 * len(model))
                    ax.set_xlabel("Feature Importance Score")
                    ax.set_ylabel("Features")
                    ax.ticklabel_format(axis="x", style="sci", scilimits=(-3, 5))

                elif direction == "vertical" and n_features:
                    fig, ax = plt.subplots(1, 1, figsize=(n_features * 0.4, 10))
                    ax.bar(
                        model.sort_values("mean_abs_shap", ascending=False)[
                            :n_features
                        ]["name"][::-1],
                        model.sort_values("mean_abs_shap", ascending=False)[
                            :n_features
                        ]["mean_abs_shap"][::-1],
                    )
                    plt.xlim(
                        -1,
                        len(
                            model.sort_values("mean_abs_shap", ascending=False)[
                                :n_features
                            ]["mean_abs_shap"][::-1]
                        )
                        + 0.1
                        * len(
                            model.sort_values("mean_abs_shap", ascending=False)[
                                :n_features
                            ]["mean_abs_shap"][::-1]
                        ),
                    )
                    ax.set_xticks(range(0, n_features))
                    plt.xticks(rotation="vertical")
                    ax.set_xlabel("Features")
                    ax.set_ylabel("Feature Importance Score")
                    ax.ticklabel_format(axis="y", style="sci", scilimits=(-3, 5))
                elif direction == "horizontal" and n_features:
                    fig, ax = plt.subplots(1, 1, figsize=(8, n_features * 0.5))
                    ax.barh(
                        model.sort_values("mean_abs_shap", ascending=False)[
                            :n_features
                        ]["name"][::-1],
                        model.sort_values("mean_abs_shap", ascending=False)[
                            :n_features
                        ]["mean_abs_shap"][::-1],
                    )
                    plt.ylim(
                        -1,
                        len(
                            model.sort_values("mean_abs_shap", ascending=False)[
                                :n_features
                            ]["mean_abs_shap"][::-1]
                        )
                        + 0.1
                        * len(
                            model.sort_values("mean_abs_shap", ascending=False)[
                                :n_features
                            ]["mean_abs_shap"][::-1]
                        ),
                    )
                    ax.set_yticks(range(0, n_features))
                    ax.set_xlabel("Feature Importance Score")
                    ax.set_ylabel("Features")
                    ax.ticklabel_format(axis="x", style="sci", scilimits=(-3, 5))

                else:
                    raise Exception(
                        "Unsupported type of a direction. Only 'vertical' or 'horizontal' allowed."
                    )

            else:
                raise Exception(
                    "Unsupported type of a framework. Only 'XGBoost or 'PyTorch' allowed."
                )

        elif classes == "as_many":
            fig, ax = plt.subplots(1, 1, figsize=(8, 20 * 0.5))

            import shap

            shap.summary_plot(
                shap_values,
                X_test,
                plot_type="bar",
                feature_names=columns,
                class_names=labels,
                max_display=20,
                show=False,
            )

            plt.ylim(
                -1,
                len(
                    model.sort_values("mean_abs_shap", ascending=False)[:n_features][
                        "mean_abs_shap"
                    ][::-1]
                )
                + 0.1
                * len(
                    model.sort_values("mean_abs_shap", ascending=False)[:n_features][
                        "mean_abs_shap"
                    ][::-1]
                ),
            )
            ax.set_xlabel("Feature Importance Score")
            ax.set_ylabel("Features")

        else:
            raise Exception(
                "Unsupported type of a bar classes. Only 'as_one' or 'as_many' allowed."
            )

    ax.tick_params(axis="x", labelsize=12)
    ax.tick_params(axis="y", labelsize=12)

    ax.set_title("Feature Importance Plot")

    # ---ATLAS Label---
    ampl.draw_atlas_label(0.02, 0.98)
    # ---ATLAS Label---
