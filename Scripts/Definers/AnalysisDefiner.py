import os


def analysis_definer(
    run=None,
    release=None,
    analysis_base=None,
    campaigns=None,
    architecture=None,
    data_type=None,
    include_data=None,
    datas=None,
    include_background=None,
    backgrounds=None,
    include_fake=None,
    fakes=None,
    include_signal=None,
    signals=None,
):
    """
    Analysis defining function. Parameters:
        * run: [string] - ('Run3', 'Run2'),
        * release: [string]- ('R22', 'R21', 'SUSY2016'),
        * analysis_base: [string] - (dependent),
        * campaigns: [list] - (dependent),
        * architecture: [string] - ('RNN', 'DeepSet'),

        * include_data / include_background / include_signal: [boolean] - (True, False),
        * datas / backgrounds / signals: [list] - (dependent),
    """

    # ---Data---
    if (include_data) and (data_type == "data"):
        datas = ["data"]
        labels_data = ["data"]
        print(
            f"Run: {run}, \nRelease: {release}, \nAnalysis Base: {analysis_base}, \nCampaigns: {campaigns}, \nArchitecture: {architecture}, \nDatas: {datas}."
        )
        return datas, labels_data
    elif not (include_data) and (data_type == "data"):
        datas = None
        labels_data = None

        print("No data included.")
        return datas, labels_data
    # ---Data---

    # ---Background---
    elif (include_background) and (data_type == "background"):
        if analysis_base == "24.2.28":
            labels_background = [
                r"$t \bar{t}$",
                r"$W \rightarrow \tau\nu$",
                r"$W \rightarrow \mu\nu$",
                r"$W \rightarrow e \nu$",
                r"$Z \rightarrow \tau\tau$",
                r"$Z \rightarrow \mu\mu$",
                r"$Z \rightarrow ee$",
                r"$Z \rightarrow \nu\nu$",
                "diboson",
                "higgs",
                "singletop",
                "ttX",
            ]
            #Remove ttX and Higgs temporary until they are available 
            backgrounds.remove("ttX")
            labels_background.remove("ttX")
            backgrounds.remove("higgs")
            labels_background.remove("higgs")
        else:
            raise Exception(
                "Unsupported analysis base. Only '24.2.28' allowed."
            )

        print(
            f"Run: {run}, \nRelease: {release}, \nAnalysis Base: {analysis_base}, \nCampaigns: {campaigns}, \nArchitecture: {architecture}, \nBackgrounds: {backgrounds}."
        )
        return backgrounds, labels_background
    elif not (include_background) and (data_type == "background"):
        backgrounds = None
        labels_background = None

        print("No background included.")
        return backgrounds, labels_background
    # ---Background---

    # ---Fake---
    elif (include_fake) and (data_type == "fake"):
        labels_fake = ["fake taus"]

        #Temporary fix until ttX and Higgs are available
        fakes.remove("ttX_faketau")
        fakes.remove("higgs_faketau")
        

        print(
            f"Run: {run}, \nRelease: {release}, \nAnalysis Base: {analysis_base}, \nCampaigns: {campaigns}, \nArchitecture: {architecture}, \nFakes: {fakes}."
        )
        return fakes, labels_fake

    elif not (include_background) and (data_type == "fake"):
        fakes = None
        labels_fake = None

        print("No fake included.")
        return fakes, labels_fake
    # ---Fake---

    # ---Signal---
    elif (include_signal) and (data_type == "signal"):
        if analysis_base == "24.2.28":
            if signals == "all":
                signals = []
                labels_signal = []
                #Signal samples are now in the same path as backgrounds 
                path = f"/disk/atlas3/data_MC/{analysis_base}/PHYS/{campaigns[0]}/vector_XEplateau/"
                signal_samples = os.listdir(path)

                for signal_sample in signal_samples:
                    if ("SS" in signal_sample) or ("GG" in signal_sample):
                        #Usual stuff
                        #if signal_sample.startswith(signal_type):
                        # signal_sample = signal_sample[:-5]

                        signal_name = signal_sample.split(".")[0]
                        signal_label = "_".join(signal_name.split("_")[0:3])

                        # signal_name_list = signal_sample.split("_")[0:3]

                        # signal_type = signal_name_list[0].lower()
                        # signal_mass_pair = signal_name_list[1]
                        # signal_mass_LSP = signal_name_list[2]

                        # signal_name = signal_type + "_" + signal_mass_pair + "_" + signal_mass_LSP

                        signals.append(signal_name)
                        labels_signal.append(signal_label)
                        # labels_signal.append(signal_name)

                        # dm = mass1 - mass2

                        # if 250 >= dm:
                        #     signal_LM.append(signal_sample)
                        # elif 250 < dm <= 1000:
                        #     signal_MM.append(signal_sample)
                        # elif dm > 1000:
                        #     signal_HM.append(signal_sample)


                # signals.sort(key=lambda x: int(x.split("_")[1]) - int(x.split("_")[2]))
                # labels_signal.sort(key=lambda x: int(x.split("_")[1]) - int(x.split("_")[2]))

                # signal_MM.sort(key=lambda x: int(x.split("_")[1]) - int(x.split("_")[2]))
                # signal_HM.sort(key=lambda x: int(x.split("_")[1]) - int(x.split("_")[2]))

            elif signals != "all":
                signals = signals
                labels_signal = []

                for signal_sample in signals:
                    signal_name_list = signal_sample.split(".")[0]
                    signal_name_list = signal_name_list.split("_")[0:3]

                    signal_type = signal_name_list[0].lower()
                    signal_mass_pair = signal_name_list[1]
                    signal_mass_LSP = signal_name_list[2]

                    signal_label = rf"$\breve {signal_type[0]} \breve {signal_type[1]}$ - {signal_mass_pair} - {signal_mass_LSP}"

                    # signal_name = signal_type + "_" + signal_mass_pair + "_" + signal_mass_LSP

                    labels_signal.append(signal_label)

                # signal_LM = [signals[0]]
                # signal_MM = [signals[1]]
                # signal_HM = [signals[2]]
            else:
                raise Exception(
                    "Unsupported signals grouping. Only 'all' or 3 specific allowed."
                )
        else:
            raise Exception("Unsupported analysis base. Only '24.2.8' allowed.")

        # if signal_type == 'GG':
        #     labels_signal = [
        #         r'$\breve g \breve g$ LM',
        #         r'$\breve g \breve g$ MM',
        #         r'$\breve g \breve g$ HM'
        #     ]
        # elif signal_type == 'SS':
        #     labels_signal = [
        #         r'$\breve s \breve s$ LM',
        #         r'$\breve s \breve s$ MM',
        #         r'$\breve s \breve s$ HM'
        #     ]

        print(
            f"Run: {run}, \nRelease: {release}, \nAnalysis Base: {analysis_base}, \nCampaigns: {campaigns}, \nArchitecture: {architecture}, \nSignals: {signals}."
        )
        return signals, labels_signal
    elif not (include_signal) and (data_type == "signal"):
        signals = None
        labels_signal = None

        print("No signal included.")
        return signals, labels_signal
    # ---Signal---

    else:
        raise Exception(
            "Unsupported data type. Only 'data', 'background' or 'signal' allowed."
        )
