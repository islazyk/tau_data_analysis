def region_definer(region, channel, subject=None, sub_subject=None):
    """
    Region defining function. Parameters:
        * region: [string] - ('Preselection', 'SR', 'CR', 'VR'),
        * channel: [string] - ('1', '2', '1&2'),
        * subject: [string] - (dependent),
        * sub_subject: [string] - (dependent).
    """

    if region == "Preselection":
        if channel == "0":
            print("Region: Preselection Region, \nChannel: 0.")
        elif channel == "1":
            print("Region: Preselection Region, \nChannel: 1.")
        elif channel == "2":
            print("Region: Preselection Region, \nChannel: 2.")
        elif channel == "1&2":
            print("Region: Preselection Region, \nChannel: 1&2.")
        else:
            raise Exception("Unsupported channel. Only '1', '2', '1&2' allowed.")
    elif region == "SR":
        if channel == "1":
            if subject == "compressed":
                print("Region: Signal Region, \nChannel: 1, \nSubject: Compressed.")
            elif subject == "medium-mass":
                print("Region: Signal Region, \nChannel: 1, \nSubject: Medium-mass.")
            else:
                raise Exception(
                    "Unsupported subject. Only 'compressed', 'medium-mass' allowed."
                )
        elif channel == "2":
            if subject == "compressed":
                print("Region: Signal Region, \nChannel: 2, \nSubject: Compressed.")
            elif subject == "high-mass":
                print("Region: Signal Region, \nChannel: 2, \nSubject: High-mass.")
            elif subject == "multibin":
                print("Region: Signal Region, \nChannel: 2, \nSubject: Multibin.")
            else:
                raise Exception(
                    "Unsupported subject. Only 'compressed', 'medium-mass', 'multibin' allowed."
                )
        else:
            raise Exception("Unsupported channel. Only '1', '2' allowed.")
    elif region == "CR":
        if subject == "W(tautau)":
            if sub_subject == "kinematic":
                print(
                    "Region: Control Region, \nSubject: W(tautau), \nSub-subject: Kinematic."
                )
            elif sub_subject == "true-tau":
                print(
                    "Region: Control Region, \nSubject: W(tautau), \nSub-subject: True-tau."
                )
            elif sub_subject == "fake-tau":
                print(
                    "Region: Control Region, \nSubject: W(tautau), \nSub-subject: Fake-tau."
                )
            else:
                raise Exception(
                    "Unsupported sub-subject. Only 'kinematic', 'true-tau', 'fake-tau' allowed."
                )
        elif subject == "top":
            if sub_subject == "kinematic":
                print(
                    "Region: Control Region, \nSubject: Top, \nSub-subject: Kinematic."
                )
            elif sub_subject == "true-tau":
                print(
                    "Region: Control Region, \nSubject: Top, \nSub-subject: True-tau."
                )
            elif sub_subject == "fake-tau":
                print(
                    "Region: Control Region, \nSubject: Top, \nSub-subject: Fake-tau."
                )
            else:
                raise Exception(
                    "Unsupported sub-subject. Only 'kinematic', 'true-tau', 'fake-tau' allowed."
                )
        elif subject == "Z(nunu)":
            print("Region: Control Region, \nSubject: Z(nunu).")
        elif subject == "Z(tautau)":
            print("Region: Control Region, \nSubject: Z(tautau).")
        elif subject == "multijet":
            print("Region: Control Region, \nSubject: Multijet.")
        else:
            raise Exception(
                "Unsupported subject. Only 'W(tautau)', 'top', 'Z(nunu)', 'Z(tautau)', 'multijet' allowed."
            )
    elif region == "VR":
        if channel == "1":
            if subject == "compressed":
                if sub_subject == "ETmiss":
                    print(
                        "Region: Validation Region, \nChannel: 1, \nSubject: Compressed, \nSub-subject: ETmiss."
                    )
                elif sub_subject == "mTtau":
                    print(
                        "Region: Validation Region, \nChannel: 1, \nSubject: Compressed, \nSub-subject: mTtau."
                    )
                else:
                    raise Exception(
                        "Unsupported sub-subject. Only 'kinematic', 'true-tau', 'fake-tau' allowed."
                    )
            elif subject == "medium-mass":
                if sub_subject == "HT":
                    print(
                        "Region: Validation Region, \nChannel: 1, \nSubject: Medium-mass, \nSub-subject: HT."
                    )
                elif sub_subject == "ETmiss":
                    print(
                        "Region: Validation Region, \nChannel: 1, \nSubject: Medium-mass, \nSub-subject: ETmiss."
                    )
                elif sub_subject == "mTtau":
                    print(
                        "Region: Validation Region, \nChannel: 1, \nSubject: Medium-mass, \nSub-subject: mTtau."
                    )
                else:
                    raise Exception(
                        "Unsupported sub-subject. Only 'HT', 'ETmiss', 'mTtau' allowed."
                    )
            else:
                raise Exception(
                    "Unsupported subject. Only 'compressed', 'medium-mass' allowed."
                )
        elif channel == "2":
            if subject == "W(tautau)":
                print("Region: Validation Region, \nChannel: 2, \nSubject: W(tautau).")
            elif subject == "top":
                print("Region: Validation Region, \nChannel: 2, \nSubject: Top.")
            elif subject == "Z(tautau)":
                print("Region: Validation Region, \nChannel: 2, \nSubject: Z(tautau).")
            else:
                raise Exception(
                    "Unsupported subject. Only 'W(tautau)', 'top', 'Z(tautau)' allowed."
                )
        else:
            raise Exception("Unsupported channel. Only '1', '2' allowed.")
    else:
        raise Exception(
            "Unsupported region. Only 'Preselection', 'SR', 'CR', 'VR' allowed."
        )

    return region, channel, subject, sub_subject
