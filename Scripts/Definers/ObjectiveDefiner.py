def objective_definer(algorithm_type, framework_type):
    """
    Objective defining function. Parameters:
        * algorithm_type: [string] - ('regression', 'binary-classification', 'multiclass-classification'),
        * framework_type: [string] - ('XGBoost', 'PyTorch').
    """

    if framework_type == "XGBoost":
        if algorithm_type == "regression":
            objective = "reg:squarederror"  # Possibilities: reg:squarederror, reg:squaredlogerror, reg:logistic, reg:pseudohubererror
            metric = "rmse"  # Possibilities: rmse, rmsle, mae, mape, mphe
        elif algorithm_type == "binary-classification":
            objective = "binary:logistic"  # Possibilities: binary:logistic, binary:logitraw, binary:hinge
            metric = "logloss"  # Possibilities: logloss, error, error@t, auc
        elif algorithm_type == "multiclass-classification":
            objective = (
                "multi:softproba"  # Possibilities: multi:softmax, multi:softproba
            )
            metric = "mlogloss"  # Possibilities: mlogloss, merror, auc
        else:
            raise Exception(
                "Unsupported type of an algorithm. Only 'regression', 'binary-classification' or 'multiclass-classification' allowed."
            )
        return objective, metric

    elif framework_type == "PyTorch":
        import torch.nn as nn

        if algorithm_type == "regression":
            criterion = (
                nn.MSELoss()
            )  # Possibilities: nn.MSELoss(), nn.L1Loss(), nn.SmoothL1Loss(), nn.PoissonNLLLoss(), nn.KLDivLoss()
        elif algorithm_type == "binary-classification":
            criterion = (
                nn.BCEWithLogitsLoss()
            )  # Possibilities: nn.BCELoss(), nn.BCEWithLogitsLoss(), nn.HingeEmbeddingLoss()
        elif algorithm_type == "multiclass-classification":
            criterion = (
                nn.CrossEntropyLoss()
            )  # Possibilities: nn.CrossEntropyLoss(), nn.NLLLoss(), nn.MultiLabelSoftMarginLoss(), nn.MultiLabelMarginLoss()
        else:
            raise Exception(
                "Unsupported type of an algorithm. Only 'regression', 'binary-classification' or 'multiclass-classification' allowed."
            )
        return criterion
    else:
        raise Exception(
            "Unsupported type of a framework. Only 'XGBoost', or 'PyTorch' allowed."
        )
