import os
from pathlib import Path
import sys


def paths_definer():
    cwd = os.getcwd()

    path_features = str(Path(cwd).parent) + "/Features/"
    path_plots = str(Path(cwd).parent) + "/Plots/"
    path_pickles = str(Path(cwd).parent) + "/Pickles/"

    path_scripts_definers = str(Path(cwd).parent) + "/Scripts/Definers/"
    path_scripts_processors = str(Path(cwd).parent) + "/Scripts/Processors/"
    path_scripts_plotters = str(Path(cwd).parent) + "/Scripts/Plotters/"

    sys.path.append(path_features)
    sys.path.append(path_plots)
    sys.path.append(path_pickles)

    sys.path.append(path_scripts_definers)
    sys.path.append(path_scripts_processors)
    sys.path.append(path_scripts_plotters)

    return path_pickles
