import pickle


def pickle_saver(
    file,
    run,
    release,
    analysis_base,
    campaigns,
    architecture,
    region,
    channel,
    subject,
    sub_subject,
):
    """
    Pickle saving function. Parameters:
        * file [string] - (dependent),
        * run: [string] - ('Run3', 'Run2'),
        * release: [string]- ('R22', 'R21', 'SUSY2016'),
        * analysis_base: [string] - (dependent),
        * campaigns: [list] - (dependent),
        * architecture: [string] ('RNN', 'DeepSet'),
        * region: [string] - ('Preselection', 'SR', 'CR', 'VR'),
        * channel: [string] - ('1', '2'),
        * subject: [string] - (dependent),
    """

    with open(
        f"../Pickles/{file}_{run}_{release}_{analysis_base}_{campaigns}_{architecture}_{region}_{channel}_{subject}_{sub_subject}.pickle",
        "wb",
    ) as handle:
        pickle.dump(file, handle, protocol=pickle.HIGHEST_PROTOCOL)


def pickle_loader(
    file,
    run,
    release,
    analysis_base,
    campaigns,
    architecture,
    region,
    channel,
    subject,
    sub_subject,
):
    """
    Pickle saving function. Parameters:
        * file [string] - (dependent),
        * run: [string] - ('Run3', 'Run2'),
        * release: [string]- ('R22', 'R21', 'SUSY2016'),
        * analysis_base: [string] - (dependent),
        * campaigns: [list] - (dependent),
        * architecture: [string] ('RNN', 'DeepSet'),
        * region: [string] - ('Preselection', 'SR', 'CR', 'VR'),
        * channel: [string] - ('1', '2'),
        * subject: [string] - (dependent),
        * sub_subject: [string] - (dependent),
    """

    with open(
        f"../Pickles/{file}_{run}_{release}_{analysis_base}_{campaigns}_{architecture}_{region}_{channel}_{subject}_{sub_subject}.pickle",
        "rb",
    ) as handle:
        return pickle.load(handle)
