def load_flat_ntuples(channel):

    baseline = [
        "selection_name",
        #----Leading tau--------
        "tau_pt_0",
        "tau_eta_0",
        "tau_phi_0",
        "tau_charge_0",
        "tau_delPhiMet_0",
        "tau_mtMet_0",
        #------Leading jet------------
        "jet_n",
        "jet_n_btag",
        "jet_pt_0",
        "jet_eta_0",
        "jet_phi_0",
        "jet_delPhiMet_0",
        #------Subleading jet------------
        "jet_pt_1",
        "jet_eta_1",
        "jet_phi_1",
        "jet_delPhiMet_1",
        #--------MET stuff-------------
        "met",
        "met_phi",
        "meff",
        "ht",
        "output_score_class_0",
        "output_score_class_1",
        "output_score_class_2",
        "output_score_class_3",
        "output_score_signal",
        "weight",
        "y_true",
        "y_pred"
    ]

    if channel == "1had0lep":
        baseline.extend([
            "output_score_class_4",
        ])

    if channel == "1had1lep":
        baseline.extend([
            "mu_n",
            "mu_mtMet_0",
            "Mt2_taumu",
            "Mtaumu",
            "output_score_class_4",

        ])

    if channel == "2tau":
        baseline.extend([
            "mu_n",
            "tau_n",
            "sumMT",
            "Mt2",
            #----Subleading tau--------
            "tau_pt_1",
            "tau_eta_1",
            "tau_phi_1",
            "tau_charge_1",
            "tau_delPhiMet_1",
            "tau_mtMet_1",
        ])

    if channel == "all":
        baseline.extend([
            "output_score_class_4",
            "mu_n",
            "mu_mtMet_0",
            "Mt2_taumu",
            "Mtaumu",
            "mu_n",
            "tau_n",
            "sumMT",
            "Mt2",
            #----Subleading tau--------
            "tau_pt_1",
            "tau_eta_1",
            "tau_phi_1",
            "tau_charge_1",
            "tau_delPhiMet_1",
            "tau_mtMet_1",
        ])

    return(baseline)
