#List of variables to load for each channel

def loadVariables(channel, fakeWeights = False):

    baseline = [
    # ---Cleaning---
    "eventClean",
    "isBadTile",
    "jet_isBadTight",
    "IsMETTrigPassed",
    # ---Truth---
    "tau_isTruthMatchedTau",
    "tau_truthMatchedType",
    # ---Weights---
    "mcEventWeight", #nominal 
    "pileupweight", #nominal
    "beamSpotWeight",
    "tau_weight",
    "ele_weight",
    "mu_weight",
    "jvt_weight",
    "bjet_weight",
    "jet_btag_weight",
    "lumiweight", #nominal
    "tau_JetRNNMedium",
    #---------Systematics-------------
    "mcEventWeights_pdf", #vector 100
    "mcEventWeights_scale", #vector 6
    "mcEventWeights_fsr", #vector 2
    "mcEventWeights_alphas", #vector 2
    "pileupweightUp",
    "pileupweightDown",
    "lumiweight_up",
    "lumiweight_down",
    # ---Kinematics--------------------------------------
    "nVtx",
    "LeptonVeto",
    #-------Taus----------------
    "tau_n",
    "tau_pt",
    "tau_eta",
    "tau_phi",
    "tau_charge",
    "tau_ntracks",
    "tau_nIsolatedTracks",
    "tau_nAllTracks",
    "tau_width",
    "tau_trackWidth",
    "tau_jetpt",
    "tau_delPhiMet",
    "tau_mtMet",
    "tau_NNDecayMode",
    "tau_ptCalo_D_pt",
    #----------Jets--------------------
    "jet_n",
    "jet_n_btag",
    "jet_pt",
    "jet_eta",
    "jet_phi",
    "jet_mtMet",
    "jet_isBjet",
    "jet_jvt",
    "jet_delPhiMet",
    "jet_width",
    #----------Met and masses---------------
    "sumMTJet",
    "sumMTTauJet",
    "sumMT",
    "met",
    "met_phi",
    "METSig",
    "meff",
    "ht",
    ]

    if channel == "1had1lep":
        baseline.extend([
            #--------Electrons-----------------
            "ele_n",
            "ele_pt",
            "ele_eta",
            "ele_phi",
            "ele_mtMet",
            "ele_charge",
            "ele_topoetcone20",
            "ele_ptvarcone30",
            "ele_delPhiMet",
            #---------Muons--------------------
            "mu_n",
            "mu_pt",
            "mu_eta",
            "mu_phi",
            "mu_mtMet",
            "mu_charge",
            #"mu_topoetcone20", replaced by mu_neflowisol20? idk what this is
            "mu_neflowisol20",
            "mu_ptvarcone30",
            "mu_delPhiMet",
            #--------Functions of Tau + mu/ele----------
            "Mt2_taumu",
            "Mt2_tauel",
            "Mtaumu",
            "Mtauel",
            ])

    if channel == "1had0lep":
        baseline.extend([
            #lepton counts
            "ele_n",
            "mu_n",
        ])
    
    if channel == "2had":
        baseline.extend([
            "Mt2",
            "ele_n",
            "mu_n",
        ])

    if channel == "all":
        baseline.extend([
           #--------Electrons-----------------
            "ele_n",
            "ele_pt",
            "ele_eta",
            "ele_phi",
            "ele_mtMet",
            "ele_charge",
            "ele_topoetcone20",
            "ele_ptvarcone30",
            "ele_delPhiMet",
            #---------Muons--------------------
            "mu_n",
            "mu_pt",
            "mu_eta",
            "mu_phi",
            "mu_mtMet",
            "mu_charge",
            "mu_topoetcone20",
            "mu_ptvarcone30",
            "mu_delPhiMet",
            #--------Functions of Tau + mu/ele----------
            "Mt2_taumu",
            "Mt2_tauel",
            "Mtaumu",
            "Mtauel",
            #--------Functions requiring two taus-------
            "Mt2",
            "ele_n",
            "mu_n",
        ])

    if fakeWeights:
        baseline.extend([
        "zmm_hjvt_weight",
        "muhad_ljvt_weight",
        "mj_hjvt_weight",
        "tau_assoc_width", 
        "tau_RNNJetScoreSigTrans",
        "tau_muonOLReta01",
        ])

    return baseline


def trainingVariables(channel):

    baseline = [
        "class", #y_true - to predict on
        "class_weight",
        "nVtx",
        "jet_n",
        "jet_n_btag",
        #------leading tau----------
        "tau_pt_0",
        "tau_eta_0",
        "tau_charge_0",
        "tau_delPhiMet_0",
        "tau_ntracks_0",
        "tau_nIsolatedTracks_0",
        "tau_nAllTracks_0",
        "tau_mtMet_0",
        "tau_NNDecayMode_0",
        #------leading jet------------
        "jet_pt_0",
        "jet_eta_0",
        "jet_mtMet_0",
        "jet_isBjet_0",
        "jet_jvt_0",
        "jet_delPhiMet_0",
        "jet_width_0",
        #------subleading jet--------
        "jet_pt_1",
        "jet_eta_1",
        "jet_mtMet_1",
        "jet_isBjet_1",
        "jet_jvt_1",
        "jet_delPhiMet_1",
        "jet_width_1",
        #--------third leading jet--------
        "jet_pt_2",
        "jet_eta_2",
        "jet_mtMet_2",
        "jet_isBjet_2",
        "jet_jvt_2",
        "jet_delPhiMet_2",
        "jet_width_2",
        #-------met and masses--------
        "sumMTJet",
        "met",
        "METSig",
        "ht",
    ]

    if channel == "1had1lep":
        baseline.extend([
            #electron kinematics
            "ele_pt_0",
            "ele_eta_0",
            "ele_charge_0",
            "ele_mtMet_0",
            "ele_delPhiMet_0",
            #muon kinematics
            "mu_pt_0",
            "mu_eta_0",
            "mu_charge_0",
            "mu_mtMet_0",
            "mu_delPhiMet_0",
            #bilinears
            "Mt2_taumu",
            "Mt2_tauel",
            "Mtaumu",
            "Mtauel",
            "sumMT",
            #lepton counts
            "ele_n",
            "mu_n",
        ]) 

    if channel == "2had":
        baseline.extend([
            #----subleading tau-----
            "tau_pt_1",
            "tau_eta_1",
            "tau_charge_1",
            "tau_delPhiMet_1",
            "tau_ntracks_1",
            "tau_nIsolatedTracks_1",
            "tau_nAllTracks_1",
            "tau_mtMet_1",
            "tau_NNDecayMode_1",
            #Misc
            "LeptonVeto",
            "tau_n",
            "ele_n",
            "mu_n",
            "Mt2",
            "sumMT"
        ])

    return baseline 
        
