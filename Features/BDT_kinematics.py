#List of variables to plot in each channel along with plotting details and binning

def loadKinematics(include_ele, include_mu, include_tau_lep, inlcude_second_tau, include_third_jet):

    plot_kinematics = [
        
        #---------------------Object multiplicities-----------------------------------
        {
            "name" : "nVtx",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "num Vertices",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 30, 
        },

        {
            "name" : "tau_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of taus",
            "bins" : 5,
            "xaxis_start": 0,
            "xaxis_end" : 5,
        },

        {
            "name" : "jet_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of jets",
            "bins" : 15,
            "xaxis_start" : 0,
            "xaxis_end" : 15,
        },

        {
            "name" : "jet_n_btag",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of b-jets",
            "bins" : 10,
            "xaxis_start" : 0,
            "xaxis_end" : 10,
        },

        {
            "name" : "mu_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of muons",
            "bins" : 5,
            "xaxis_start" : 0,
            "xaxis_end" : 5,
        },

        {
            "name" : "ele_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of electrons",
            "bins" : 5,
            "xaxis_start" : 0,
            "xaxis_end" : 5,
        },

    #---------------------Taus-----------------------------
        {
            "name": "tau_pt",
            "yaxis_label": "Number of Events",
            "xaxis_label": "tau1 $p_{T}$ [GeV]",
            "bins": 30,
            "xaxis_start": 0,
            "xaxis_end": 300,
            "n": 1,
            "divide": 1000,
        },

        {
            'name': 'tau_eta',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau1 $\eta$',
            'bins': 30,
            'xaxis_start': -3,
            'xaxis_end': 3,
            'n': 1,         
        },

        {
            'name': 'tau_phi',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau1 $\phi$',
            'bins': 30,
            'xaxis_start': -3.5,
            'xaxis_end': 3.5,
            'n': 1,
        },

        {
            'name': 'tau_charge',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau1 charge',
            'bins': 5,
            'xaxis_start': -2.5,
            'xaxis_end': 2.5,
            'n': 1,
        },

        {
            'name': 'tau_ntracks',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau1 num tracks',
            'bins': 5,
            'xaxis_start': -0.5,
            'xaxis_end': 4.5,
            "n" : 1,
        },

        {
            "name" : "tau_nIsolatedTracks",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau1 num isolated tracks",
            "bins" : 5,
            "xaxis_start" : -0.5,
            "xaxis_end" : 4.5,
            "n" : 1,
        },

        {
            "name" : "tau_nAllTracks",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau1 num all tracks",
            "bins" : 5,
            "xaxis_start" : -0.5,
            "xaxis_end" : 4.5,
            "n" : 1,
        },

        {
            "name" : "tau_width",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau1 width",
            "bins" : 30, 
            "xaxis_start": 0,
            "xaxis_end" : 0.5,
            "n" : 1,
        },

        {
            "name" : "tau_delPhiMet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau1 delPhiMet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 2,
            "n" : 1,
        },

        {
            "name" : "tau_mtMet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mT(tau1, met)",
            "bins" : 30,
            "xaxis_start": 0,
            "xaxis_end" : 200,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "tau_NNDecayMode",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau1 NN Decay Mode",
            "bins" : 5,
            "xaxis_start" : -0.5,
            "xaxis_end" : 5,
            "n" : 1,
        },
        #---------------------Leading Jet-----------------------------
        {
            "name": "jet_pt",
            "yaxis_label": "Number of Events",
            "xaxis_label": "jet1 $p_{T}$ [GeV]",
            "bins": 30,
            "xaxis_start": 0,
            "xaxis_end": 500,
            "n": 1,
            "divide": 1000,
        },

        {
            'name': 'jet_eta',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'jet1 $\eta$',
            'bins': 30,
            'xaxis_start': -3,
            'xaxis_end': 3,
            'n': 1,         
        },

        {
            'name': 'jet_phi',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'jet1 $\phi$',
            'bins': 30,
            'xaxis_start': -3.5,
            'xaxis_end': 3.5,
            'n': 1,
        },

        {
            "name" : "jet_width",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet1 width",
            "bins" : 30, 
            "xaxis_start": 0,
            "xaxis_end" : 0.5,
            "n" : 1,
        },

        {
            "name" : "jet_delPhiMet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet1 delPhiMet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 3,
            "n" : 1,
        },

        {
            "name" : "jet_mtMet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mT(jet1, met)",
            "bins" : 30,
            "xaxis_start": 0,
            "xaxis_end" : 1000,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "jet_isBjet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet1_is_bjet",
            "bins" : 5,
            "xaxis_start" : -2.5,
            "xaxis_end" : 2.5,
            "n" : 1,
        },

        {
            "name" : "jet_jvt",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet1 jvt",
            "bins" : 10,
            "xaxis_start" : 0,
            "xaxis_end" : 1,
            "n" : 1,
        },
        #-------------------Subleading Jet-----------------------------
        {
            "name": "jet_pt",
            "yaxis_label": "Number of Events",
            "xaxis_label": "jet2 $p_{T}$ [GeV]",
            "bins": 30,
            "xaxis_start": 0,
            "xaxis_end": 500,
            "n": 2,
            "divide": 1000,
        },

        {
            'name': 'jet_eta',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'jet2 $\eta$',
            'bins': 30,
            'xaxis_start': -3,
            'xaxis_end': 3,
            'n': 2,         
        },

        {
            'name': 'jet_phi',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'jet2 $\phi$',
            'bins': 30,
            'xaxis_start': -3.5,
            'xaxis_end': 3.5,
            'n': 2,
        },

        {
            "name" : "jet_width",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet2 width",
            "bins" : 30, 
            "xaxis_start": 0,
            "xaxis_end" : 0.5,
            "n" : 2,
        },

        {
            "name" : "jet_delPhiMet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet2 delPhiMet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 3,
            "n" : 2,
        },

        {
            "name" : "jet_mtMet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mT(jet2, met)",
            "bins" : 30,
            "xaxis_start": 0,
            "xaxis_end" : 1000,
            "n" : 2,
            "divide" : 1000,
        },

        {
            "name" : "jet_isBjet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet2_is_bjet",
            "bins" : 5,
            "xaxis_start" : -2.5,
            "xaxis_end" : 2.5,
            "n" : 2,
        },

        {
            "name" : "jet_jvt",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet2 jvt",
            "bins" : 10,
            "xaxis_start" : 0,
            "xaxis_end" : 1,
            "n" : 2,
        },
        #----------------------MET stuff-----------------------------
        {
            "name" : "sumMTJet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "sum MTJet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 3000,
            "divide" : 1000,
        },

        {
            "name" : "sumMTTauJet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "sum MT TauJet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 3000,
            "divide" : 1000,
        },

        {
            "name" : "met",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "MET",
            "bins" : 30,
            "xaxis_start" : 150,
            "xaxis_end" : 350,
            "divide" : 1000,
        },

        {
            "name" : "met_phi",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "met $\phi$",
            "bins" : 30,
            "xaxis_start" : -3.5,
            "xaxis_end" : 3.5,
        },

        {
            "name" : "METSigSoftTrk",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "MET SigSoftTrk",
            "bins" : 30,
            "xaxis_start": 0,
            "xaxis_end": 20,
        },

        {
            "name": "METSig",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "MET sig",
            "bins": 30,
            "xaxis_start" : 0,
            "xaxis_end" : 20,
        },

        {
            "name" : "METSigPow3",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "MET sig pow3",
            "bins" : 30,
            "xaxis_start" : 10,
            "xaxis_end" : 50,
        },

        {
            "name" : "meff",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "meff",
            "bins" : 30,
            "xaxis_start" : 300,
            "xaxis_end" : 1500,
            "divide" : 1000,
        },

        {
            "name" : "ht",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "ht",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 1000,
            "divide" : 1000,
        },
        
    ]


    if include_third_jet:
        plot_kinematics.extend([  
            #-------------------Third Jet-----------------------------
            {
                "name": "jet_pt",
                "yaxis_label": "Number of Events",
                "xaxis_label": "jet3 $p_{T}$ [GeV]",
                "bins": 30,
                "xaxis_start": 0,
                "xaxis_end": 500,
                "n": 3,
                "divide": 1000,
            },

            {
                'name': 'jet_eta',
                'yaxis_label': 'Number of Events',
                'xaxis_label': 'jet3 $\eta$',
                'bins': 30,
                'xaxis_start': -3,
                'xaxis_end': 3,
                'n': 3,         
            },

            {
                'name': 'jet_phi',
                'yaxis_label': 'Number of Events',
                'xaxis_label': 'jet3 $\phi$',
                'bins': 30,
                'xaxis_start': -3.5,
                'xaxis_end': 3.5,
                'n': 3,
            },

            {
                "name" : "jet_width",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "jet3 width",
                "bins" : 30, 
                "xaxis_start": 0,
                "xaxis_end" : 0.5,
                "n" : 3,
            },

            {
                "name" : "jet_delPhiMet",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "jet3 delPhiMet",
                "bins" : 30,
                "xaxis_start" : 0,
                "xaxis_end" : 3,
                "n" : 3,
            },

            {
                "name" : "jet_mtMet",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "mT(jet3, met)",
                "bins" : 30,
                "xaxis_start": 0,
                "xaxis_end" : 1000,
                "n" : 3,
                "divide" : 1000,
            },

            {
                "name" : "jet_isBjet",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "jet3_is_bjet",
                "bins" : 5,
                "xaxis_start" : -2.5,
                "xaxis_end" : 2.5,
                "n" : 3,
            },

            {
                "name" : "jet_jvt",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "jet3 jvt",
                "bins" : 10,
                "xaxis_start" : 0,
                "xaxis_end" : 1,
                "n" : 3,
            },
        ])


    if include_ele:
        plot_kinematics.extend([
            #------------------------ Electrons----------------------------
            {
                "name" : "ele_eta",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "ele1 $\eta$",
                "bins" : 30,
                "xaxis_start" : -3,
                "xaxis_end" : 3,
                "n" : 1,
            },

            {
                "name" : "ele_pt",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "ele1 $p_{T}$",
                "bins" : 30,
                "xaxis_start" : 0,
                "xaxis_end" : 500,
                "n" : 1,
                "divide" : 1000,
            },

            {
                "name" : "ele_phi",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "ele1 $\phi$",
                "bins" : 30,
                "xaxis_start" : -3.5,
                "xaxis_end" : -3.5,
                "n" : 1,
            },

            {
                "name" : "ele_mtMet",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "mT(ele1, MET)",
                "bins" : 30,
                "xaxis_start" : 0,
                "xaxis_end" : 1000,
                "n" : 1,
                "divide" : 1000,
            },

            {
                "name" : "ele_charge",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "ele1 charge",
                "bins" : 5,
                "xaxis_start" : -2.5,
                "xaxis_end" : -2.5,
                "n" : 1,
            },
        ])

    if include_mu:
        plot_kinematics.extend([
            #-----------------------Muons------------------------------
            {
                "name" : "mu_pt",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "mu1 $p_{T}$",
                "bins" : 30,
                "xaxis_start" : 0,
                "xaxis_end" : 500,
                "n" : 1,
                "divide" : 1000,
            },

            {
                "name" : "mu_eta",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "mu1 $\eta$",
                "bins" : 30,
                "xaxis_start" : -3,
                "xaxis_end" : 3,
                "n" : 1,
            },

            {
                "name" : "mu_phi",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "mu1 $\phi$",
                "bins" : 30,
                "xaxis_start" : -3.5,
                "xaxis_end" : -3.5,
                "n" : 1,
            },

            {
                "name" : "mu_mtMet",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "mT(mu1, MET)",
                "bins" : 30,
                "xaxis_start" : 0,
                "xaxis_end" : 1000,
                "n" : 1,
                "divide" : 1000,
            },

            {
                "name" : "mu_charge",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "mu1 charge",
                "bins" : 5,
                "xaxis_start" : -2.5,
                "xaxis_end" : -2.5,
                "n" : 1,
            },

            {
                "name" : "mu_author",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "mu1 author",
                "bins" : 5,
                "xaxis_start" : -2.5,
                "xaxis_end" : 2.5,
                "n" : 1
            },
        ])

    if include_tau_lep:
        plot_kinematics.extend([
            #--------------------Functions of tau and leptons------------------------
            {
                "name" : "Mt2_taumu",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "Mt2(tau,mu)",
                "bins" : 30,
                "xaxis_start" : -10,
                "xaxis_end" : 1000,
                "divide" : 1000,
            },

            {
                "name" : "Mt2_tauel",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "Mt2(tau,el)",
                "bins" : 30,
                "xaxis_start" : -10,
                "xaxis_end" : 1000,
                "divide" : 1000,
            },

            {
                "name" : "Mtaumu",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "M(tau,mu)",
                "bins" : 30,
                "xaxis_start" : -10,
                "xaxis_end" : 1000,
                "divide" : 1000,
            },

            {
                "name" : "Mtauel",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "M(tau,el)",
                "bins" : 30,
                "xaxis_start" : -10,
                "xaxis_end" : 1000,
                "divide" : 1000,
            },
        ])

    if inlcude_second_tau:
        plot_kinematics.extend([
            #--------------------Subleading tau------------------------------------
            {
                "name": "tau_pt",
                "yaxis_label": "Number of Events",
                "xaxis_label": "tau2 $p_{T}$ [GeV]",
                "bins": 30,
                "xaxis_start": 0,
                "xaxis_end": 300,
                "n": 2,
                "divide": 1000,
            },

            {
                'name': 'tau_eta',
                'yaxis_label': 'Number of Events',
                'xaxis_label': 'tau2 $\eta$',
                'bins': 30,
                'xaxis_start': -3,
                'xaxis_end': 3,
                'n': 2,         
            },

            {
                'name': 'tau_phi',
                'yaxis_label': 'Number of Events',
                'xaxis_label': 'tau2 $\phi$',
                'bins': 30,
                'xaxis_start': -3.5,
                'xaxis_end': 3.5,
                'n': 2,
            },

            {
                'name': 'tau_charge',
                'yaxis_label': 'Number of Events',
                'xaxis_label': 'tau2 charge',
                'bins': 5,
                'xaxis_start': -2.5,
                'xaxis_end': 2.5,
                'n': 2,
            },

            {
                'name': 'tau_ntracks',
                'yaxis_label': 'Number of Events',
                'xaxis_label': 'tau2 num tracks',
                'bins': 5,
                'xaxis_start': -0.5,
                'xaxis_end': 4.5,
                "n" : 2,
            },

            {
                "name" : "tau_nIsolatedTracks",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "tau2 num isolated tracks",
                "bins" : 5,
                "xaxis_start" : -0.5,
                "xaxis_end" : 4.5,
                "n" : 2,
            },

            {
                "name" : "tau_nAllTracks",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "tau2 num all tracks",
                "bins" : 5,
                "xaxis_start" : -0.5,
                "xaxis_end" : 4.5,
                "n" : 2,
            },

            {
                "name" : "tau_width",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "tau2 width",
                "bins" : 30, 
                "xaxis_start": 0,
                "xaxis_end" : 0.5,
                "n" : 2,
            },

            {
                "name" : "tau_delPhiMet",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "tau2 delPhiMet",
                "bins" : 30,
                "xaxis_start" : 0,
                "xaxis_end" : 2,
                "n" : 2,
            },

            {
                "name" : "tau_mtMet",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "mT(tau2, met)",
                "bins" : 30,
                "xaxis_start": 0,
                "xaxis_end" : 200,
                "n" : 2,
                "divide" : 1000,
            },

            {
                "name" : "tau_NNDecayMode",
                "yaxis_label" : "Number of Events",
                "xaxis_label" : "tau2 NN Decay Mode",
                "bins" : 5,
                "xaxis_start" : -0.5,
                "xaxis_end" : 5,
                "n" : 2,
            },
            #-------------------------MT2 stuff--------------------------------
        ])

    return plot_kinematics


def loadKinematics_ele_only():
    plot_kinematics = ([
        #------------------------ Electrons----------------------------
        {
            "name" : "ele_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of electrons",
            "bins" : 5,
            "xaxis_start" : 0,
            "xaxis_end" : 5,
        },

        {
            "name" : "ele_eta",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "ele1 $\eta$",
            "bins" : 30,
            "xaxis_start" : -3,
            "xaxis_end" : 3,
            "n" : 1,
        },

        {
            "name" : "ele_pt",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "ele1 $p_{T}$",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 500,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "ele_phi",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "ele1 $\phi$",
            "bins" : 30,
            "xaxis_start" : -3.5,
            "xaxis_end" : 3.5,
            "n" : 1,
        },

        {
            "name" : "ele_mtMet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mT(ele1, MET)",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 1000,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "ele_charge",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "ele1 charge",
            "bins" : 5,
            "xaxis_start" : -2.5,
            "xaxis_end" : 2.5,
            "n" : 1,
        },
    ])

    return plot_kinematics


def loadKinematics_mu_only():
    plot_kinematics = ([
        #------------------------ Electrons----------------------------
        {
            "name" : "mu_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of muons",
            "bins" : 5,
            "xaxis_start" : 0,
            "xaxis_end" : 5,
        },

        {
            "name" : "mu_eta",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mu1 $\eta$",
            "bins" : 30,
            "xaxis_start" : -3,
            "xaxis_end" : 3,
            "n" : 1,
        },

        {
            "name" : "mu_pt",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mu1 $p_{T}$",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 500,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "mu_phi",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mu1 $\phi$",
            "bins" : 30,
            "xaxis_start" : -3.5,
            "xaxis_end" : 3.5,
            "n" : 1,
        },

        {
            "name" : "mu_mtMet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mT(mu1, MET)",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 1000,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "mu_charge",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mu1 charge",
            "bins" : 5,
            "xaxis_start" : -2.5,
            "xaxis_end" : 2.5,
            "n" : 1,
        },

        {
            "name" : "mu_author",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mu1 author",
            "bins" : 5,
            "xaxis_start" : -2.5,
            "xaxis_end" : 2.5,
            "n" : 1
        },
    ])

    return plot_kinematics
    