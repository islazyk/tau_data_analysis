def plot_variables(channel):

    plot_kinematics = [

        #---------------------Prediction-------------------------------------------
        {
            "name" : "y_pred",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "y pred",
            "bins" : 5,
            "xaxis_start" : 0,
            "xaxis_end" : 5,
        },

        #---------------------Object multiplicities-----------------------------------
        {
            "name" : "nVtx",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "num Vertices",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 30, 
        },

        {
            "name" : "jet_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of jets",
            "bins" : 15,
            "xaxis_start" : 0,
            "xaxis_end" : 15,
        },

        {
            "name" : "jet_n_btag",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of b-jets",
            "bins" : 10,
            "xaxis_start" : 0,
            "xaxis_end" : 10,
        },
    #---------------------Taus-----------------------------
        {
            "name": "tau_pt_0",
            "yaxis_label": "Number of Events",
            "xaxis_label": "tau1 $p_{T}$ [GeV]",
            "bins": 30,
            "xaxis_start": 0,
            "xaxis_end": 300,
            "n": 1,
            "divide": 1000,
        },

        {
            'name': 'tau_eta_0',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau1 $\eta$',
            'bins': 30,
            'xaxis_start': -3,
            'xaxis_end': 3,
            'n': 1,         
        },

        {
            'name': 'tau_phi_0',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau1 $\phi$',
            'bins': 30,
            'xaxis_start': -3.5,
            'xaxis_end': 3.5,
            'n': 1,
        },

        {
            'name': 'tau_charge_0',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau1 charge',
            'bins': 5,
            'xaxis_start': -2.5,
            'xaxis_end': 2.5,
            'n': 1,
        },

        {
            'name': 'tau_ntracks_0',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau1 num tracks',
            'bins': 5,
            'xaxis_start': -0.5,
            'xaxis_end': 4.5,
            "n" : 1,
        },

        {
            "name" : "tau_nIsolatedTracks_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau1 num isolated tracks",
            "bins" : 5,
            "xaxis_start" : -0.5,
            "xaxis_end" : 4.5,
            "n" : 1,
        },

        {
            "name" : "tau_nAllTracks_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau1 num all tracks",
            "bins" : 5,
            "xaxis_start" : -0.5,
            "xaxis_end" : 4.5,
            "n" : 1,
        },

        {
            'name': 'tau_trackWidth_0',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau1 track width',
            'bins': 30,
            'xaxis_start': 0,
            'xaxis_end': 0.2,
            "n" : 1,
        },

        {
            "name" : "tau_delPhiMet_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau1 delPhiMet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 2,
            "n" : 1,
        },

        {
            "name" : "tau_mtMet_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mT(tau1, met)",
            "bins" : 30,
            "xaxis_start": 0,
            "xaxis_end" : 200,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "tau_NNDecayMode_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau1 NN Decay Mode",
            "bins" : 5,
            "xaxis_start" : -0.5,
            "xaxis_end" : 5,
            "n" : 1,
        },
        #---------------------Leading Jet-----------------------------
        {
            "name": "jet_pt_0",
            "yaxis_label": "Number of Events",
            "xaxis_label": "jet1 $p_{T}$ [GeV]",
            "bins": 30,
            "xaxis_start": 0,
            "xaxis_end": 500,
            "n": 1,
            "divide": 1000,
        },

        {
            'name': 'jet_eta_0',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'jet1 $\eta$',
            'bins': 30,
            'xaxis_start': -3,
            'xaxis_end': 3,
            'n': 1,         
        },

        {
            'name': 'jet_phi_0',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'jet1 $\phi$',
            'bins': 30,
            'xaxis_start': -3,
            'xaxis_end': 3,
            'n': 1,
        },

        {
            "name" : "jet_width_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet1 width",
            "bins" : 30, 
            "xaxis_start": 0,
            "xaxis_end" : 0.5,
            "n" : 1,
        },

        {
            "name" : "jet_delPhiMet_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet1 delPhiMet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 3,
            "n" : 1,
        },

        {
            "name" : "jet_mtMet_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mT(jet1, met)",
            "bins" : 30,
            "xaxis_start": 0,
            "xaxis_end" : 1000,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "jet_isBjet_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet1_is_bjet",
            "bins" : 5,
            "xaxis_start" : -2.5,
            "xaxis_end" : 2.5,
            "n" : 1,
        },

        {
            "name" : "jet_jvt_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet1 jvt",
            "bins" : 10,
            "xaxis_start" : 0,
            "xaxis_end" : 1,
            "n" : 1,
        },
        #-------------------Subleading Jet-----------------------------
        {
            "name": "jet_pt_1",
            "yaxis_label": "Number of Events",
            "xaxis_label": "jet2 $p_{T}$ [GeV]",
            "bins": 30,
            "xaxis_start": 0,
            "xaxis_end": 500,
            "n": 2,
            "divide": 1000,
        },

        {
            'name': 'jet_eta_1',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'jet2 $\eta$',
            'bins': 30,
            'xaxis_start': -3,
            'xaxis_end': 3,
            'n': 2,         
        },

        {
            'name': 'jet_phi_1',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'jet2 $\phi$',
            'bins': 30,
            'xaxis_start': -3,
            'xaxis_end': 3,
            'n': 1,
        },

        {
            "name" : "jet_width_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet2 width",
            "bins" : 30, 
            "xaxis_start": 0,
            "xaxis_end" : 0.5,
            "n" : 2,
        },

        {
            "name" : "jet_delPhiMet_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet2 delPhiMet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 3,
            "n" : 2,
        },

        {
            "name" : "jet_mtMet_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mT(jet2, met)",
            "bins" : 30,
            "xaxis_start": 0,
            "xaxis_end" : 1000,
            "n" : 2,
            "divide" : 1000,
        },

        {
            "name" : "jet_isBjet_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet2_is_bjet",
            "bins" : 5,
            "xaxis_start" : -2.5,
            "xaxis_end" : 2.5,
            "n" : 2,
        },

        {
            "name" : "jet_jvt_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "jet2 jvt",
            "bins" : 10,
            "xaxis_start" : 0,
            "xaxis_end" : 1,
            "n" : 2,
        },
        #----------------------MET stuff-----------------------------
        {
            "name" : "sumMTJet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "sum MTJet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 3000,
            "divide" : 1000,
        },

        {
            "name" : "sumMTTauJet",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "sum MT TauJet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 3000,
            "divide" : 1000,
        },

        {
            "name" : "met",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "MET",
            "bins" : 30,
            "xaxis_start" : 150,
            "xaxis_end" : 350,
            "divide" : 1000,
        },

        {
            "name": "METSig",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "MET sig",
            "bins": 30,
            "xaxis_start" : 0,
            "xaxis_end" : 20,
        },

        {
            "name" : "meff",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "meff",
            "bins" : 30,
            "xaxis_start" : 300,
            "xaxis_end" : 1500,
            "divide" : 1000,
        },

        {
            "name" : "ht",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "ht",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 1000,
            "divide" : 1000,
        },
    ]

    if "1had0lep" in channel:
        plot_kinematics.extend([

        {
            "name" : "output_score_class_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Top quarks",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_class_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Wtaunu",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_class_2",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Ztautau",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_class_3",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Dibosons",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_class_4",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Fakes",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_signal",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Signal",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        ])

    #add muons and electrons
    if "1had1lep" in channel:
        plot_kinematics.extend([

        {
            "name" : "output_score_class_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Top quarks",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_class_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Ztautau",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_class_2",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Dibosons",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_class_3",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Fakes",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_signal",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Signal",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "mu_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of muons",
            "bins" : 5,
            "xaxis_start" : 0,
            "xaxis_end" : 5,
        },

        {
            "name" : "ele_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of electrons",
            "bins" : 5,
            "xaxis_start" : 0,
            "xaxis_end" : 5,
        },

        {
            "name" : "ele_eta_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "ele1 $\eta$",
            "bins" : 30,
            "xaxis_start" : -3,
            "xaxis_end" : 3,
            "n" : 1,
        },

        {
            "name" : "ele_pt_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "ele1 $p_{T}$",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 500,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "ele_delPhiMet_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "ele1 delPhiMet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 3,
            "n" : 1,
        },

        {
            "name" : "ele_mtMet_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mT(ele1, MET)",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 1000,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "ele_charge_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "ele1 charge",
            "bins" : 5,
            "xaxis_start" : -2.5,
            "xaxis_end" : 2.5,
            "n" : 1,
        },
        
        {
            "name" : "mu_pt_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mu1 $p_{T}$",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 500,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "mu_eta_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mu1 $\eta$",
            "bins" : 30,
            "xaxis_start" : -3,
            "xaxis_end" : 3,
            "n" : 1,
        },

        {
            "name" : "mu_delPhiMet_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mu1 delPhiMet",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 3,
            "n" : 1,
        },

        {
            "name" : "mu_mtMet_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mT(mu1, MET)",
            "bins" : 30,
            "xaxis_start" : 0,
            "xaxis_end" : 1000,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "mu_charge_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mu1 charge",
            "bins" : 5,
            "xaxis_start" : -2.5,
            "xaxis_end" : 2.5,
            "n" : 1,
        },

        {
            "name" : "Mt2_taumu",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Mt2(tau,mu)",
            "bins" : 30,
            "xaxis_start" : -10,
            "xaxis_end" : 1000,
            "divide" : 1000,
        },

        {
            "name" : "Mt2_tauel",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Mt2(tau,el)",
            "bins" : 30,
            "xaxis_start" : -10,
            "xaxis_end" : 1000,
            "divide" : 1000,
        },

        {
            "name" : "Mtaumu",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "M(tau,mu)",
            "bins" : 30,
            "xaxis_start" : -10,
            "xaxis_end" : 1000,
            "divide" : 1000,
        },

        {
            "name" : "Mtauel",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "M(tau,el)",
            "bins" : 30,
            "xaxis_start" : -10,
            "xaxis_end" : 1000,
            "divide" : 1000,
        },
        ])

    #Add subleading tau
    if "2had" in channel:
        plot_kinematics.extend([

        {
            "name" : "output_score_class_0",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Top quarks",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_class_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Ztautau",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_class_2",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Dibosons",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_class_3",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Fakes",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "output_score_signal",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Output score: Signal",
            "bins" : 30,
            "xaxis_start" : 0, 
            "xaxis_end" : 1,
            "scale" : "log",
        },

        {
            "name" : "mu_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of muons",
            "bins" : 5,
            "xaxis_start" : 0,
            "xaxis_end" : 5,
        },

        {
            "name" : "ele_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of electrons",
            "bins" : 5,
            "xaxis_start" : 0,
            "xaxis_end" : 5,
        },

        {
            "name" : "tau_n",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "Number of taus",
            "bins" : 5,
            "xaxis_start": 0,
            "xaxis_end" : 5,
        },    

        {
            "name": "tau_pt_1",
            "yaxis_label": "Number of Events",
            "xaxis_label": "tau2 $p_{T}$ [GeV]",
            "bins": 30,
            "xaxis_start": 0,
            "xaxis_end": 300,
            "n": 1,
            "divide": 1000,
        },

        {
            'name': 'tau_eta_1',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau2 $\eta$',
            'bins': 30,
            'xaxis_start': -3,
            'xaxis_end': 3,
            'n': 1,         
        },

        {
            'name': 'tau_phi_1',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau2 $\phi$',
            'bins': 30,
            'xaxis_start': -3.5,
            'xaxis_end': 3.5,
            'n': 1,
        },

        {
            'name': 'tau_charge_1',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau2 charge',
            'bins': 5,
            'xaxis_start': -2.5,
            'xaxis_end': 2.5,
            'n': 1,
        },

        {
            'name': 'tau_ntracks_1',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau2 num tracks',
            'bins': 5,
            'xaxis_start': -0.5,
            'xaxis_end': 4.5,
            "n" : 1,
        },

        {
            'name': 'tau_trackWidth_1',
            'yaxis_label': 'Number of Events',
            'xaxis_label': 'tau2 track width',
            'bins': 30,
            'xaxis_start': 0,
            'xaxis_end': 0.2,
            "n" : 1,
        },

        {
            "name" : "tau_nIsolatedTracks_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau2 num isolated tracks",
            "bins" : 5,
            "xaxis_start" : -0.5,
            "xaxis_end" : 4.5,
            "n" : 1,
        },

        {
            "name" : "tau_nAllTracks_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau2 num all tracks",
            "bins" : 5,
            "xaxis_start" : -0.5,
            "xaxis_end" : 4.5,
            "n" : 1,
        },

        {
            "name" : "tau_mtMet_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "mT(tau2, met)",
            "bins" : 30,
            "xaxis_start": 0,
            "xaxis_end" : 200,
            "n" : 1,
            "divide" : 1000,
        },

        {
            "name" : "tau_NNDecayMode_1",
            "yaxis_label" : "Number of Events",
            "xaxis_label" : "tau2 NN Decay Mode",
            "bins" : 5,
            "xaxis_start" : -0.5,
            "xaxis_end" : 5,
            "n" : 1,
        },
        ])

    return plot_kinematics