#List of variables to load for each channel

def loadFeatures(channel, fakeWeights = False, new_fakeWeights = False):

    baseline = [
    # ---EXCLUDED BEFORE TRAINING-------------------------------------
    # ---Cleaning---
    "eventClean",
    "isBadTile",
    "jet_isBadTight",
    "IsMETTrigPassed",
    # ---Truth---
    "tau_isTruthMatchedTau",
    "tau_truthType",
    # ---Weights---
    "mcEventWeight",
    "pileupweight",
    "beamSpotWeight",
    "tau_medium_weight",
    "ele_weight",
    "mu_weight",
    "jvt_weight",
    "bjet_weight",
    "jet_btag_weight",
    "lumiweight",
    "tau_JetRNNMedium",
    # ---INCLUDED IN TRAINING--------------------------------------
    "nVtx",
    "LeptonVeto",
    #-------Taus----------------
    "tau_n",
    "tau_pt",
    "tau_eta",
    "tau_phi",
    "tau_charge",
    "tau_ntracks",
    "tau_nIsolatedTracks",
    "tau_nAllTracks",
    "tau_width",
    "tau_jetpt",
    "tau_delPhiMet",
    "tau_mtMet",
    "tau_NNDecayMode",
    #----------Jets--------------------
    "jet_n",
    "jet_n_btag",
    "jet_pt",
    "jet_eta",
    "jet_phi",
    "jet_mtMet",
    "jet_isBjet",
    "jet_jvt",
    "jet_delPhiMet",
    "jet_width",
    #----------Met and masses---------------
    "sumMTJet",
    "sumMTTauJet",
    "met",
    "met_phi",
    "METSigSoftTrk",
    "METSig",
    "METSigPow3",
    "meff",
    "ht",
    ]

    if channel == "1had1lep" or channel == "1hadInclusive":
        baseline.extend([
            #--------Electrons-----------------
            "ele_n",
            "ele_pt",
            "ele_eta",
            "ele_phi",
            "ele_mtMet",
            "ele_charge",
            #---------Muons--------------------
            "mu_n",
            "mu_pt",
            "mu_eta",
            "mu_phi",
            "mu_mtMet",
            "mu_charge",
            "mu_author",
            "mu_muonType",
            #--------Functions of Tau + mu/ele----------
            "Mt2_taumu",
            "Mt2_tauel",
            "Mtaumu",
            "Mtauel",
            ])
    
    if channel == "2tau":
        baseline.extend([
            "Mt2",
            "sumMT",
            "ele_n",
            "mu_n",
        ])

    if channel == "all":
        baseline.extend([
            #--------Electrons-----------------
            "ele_n",
            "ele_pt",
            "ele_eta",
            "ele_phi",
            "ele_mtMet",
            "ele_charge",
            #---------Muons--------------------
            "mu_n",
            "mu_pt",
            "mu_eta",
            "mu_phi",
            "mu_mtMet",
            "mu_charge",
            "mu_author",
            "mu_muonType",
            #--------Functions of Tau + mu/ele----------
            "Mt2_taumu",
            "Mt2_tauel",
            "Mtaumu",
            "Mtauel",
            #--------Functions requiring two taus-------
            "Mt2",
            "sumMT",
            "ele_n",
            "mu_n",
        ])

    if fakeWeights:
        baseline.extend([
        "ff_weight_1", 
        "ff_weight_2",
        "ff_weight_3",
        "ff_weight_4", 
        "tau_assoc_width", 
        "tau_RNNJetScoreSigTrans",
        ])

    if new_fakeWeights:
        baseline.extend([
        "zmm_hjvt_weight",
        "muhad_ljvt_weight",
        "mj_hvjt_weight",
        "tau_assoc_width", 
        "tau_RNNJetScoreSigTrans",
        ])

    return baseline
