#Define a cut dictionary 

def cuts(channel, region):

    cut_dict = {}

    if channel == "2had":

        if region == "preselection":

            cut_dict = {
                "name" : "Preselection",
                "cut" : "True" #acts as no cut
            }

        elif region == "topCR":

            cut_dict = {
                "name" : "Top CR",
                "cut" : "y_pred  == 0",
            }

        elif region == "ZCR":

            cut_dict = {
                "name" : "Z CR",
                "cut" : "y_pred == 1",
            }
        
        elif region == "dibosonCR":

            cut_dict = {
                "name" : "VV CR",
                "cut" : "y_pred == 2",
            }

        elif region == "fakeCR":

            cut_dict = {
                "name" : "Fake CR",
                "cut" : "y_pred == 3",
            }
        
        elif region == "signal":

            cut_dict = {
                "name" : "Signal region",
                "cut" : "output_score_signal > 0.75",
            }

    elif channel == "1had1lep":

        if region == "preselection":

            cut_dict = {
                "name" : "Preselection",
                "cut" : "True" #acts as no cut
            }

        elif region == "topCR":

            cut_dict = {
                "name" : "Top CR",
                "cut" : "y_pred  == 0",
            }

        elif region == "ZCR":

            cut_dict = {
                "name" : "Z CR",
                "cut" : "y_pred == 1",
            }
        
        elif region == "dibosonCR":

            cut_dict = {
                "name" : "VV CR",
                "cut" : "y_pred == 2",
            }

        elif region == "fakeCR":

            cut_dict = {
                "name" : "Fake CR",
                "cut" : "y_pred == 3",
            }
        
        elif region == "signal":

            cut_dict = {
                "name" : "Signal region",
                "cut" : "output_score_signal > 0.80",
            }
            
    elif channel == "1had0lep":

        if region == "preselection":

            cut_dict = {
                "name" : "Preselection",
                "cut" : "True",
            }

        elif region == "topCR":

            cut_dict = {
                "name" : "Top CR",
                "cut" : "y_pred  == 0",
            }

        elif region == "WCR":

            cut_dict = {
                "name" : "W CR",
                "cut" : "y_pred == 1",
            }
        
        elif region == "ZCR":

            cut_dict = {
                "name" : "VV CR",
                "cut" : "y_pred == 2",
            }

        elif region == "dibosonCR":

            cut_dict = {
                "name" : "VV CR",
                "cut" : "y_pred == 3",
            }

        elif region == "fakeCR":

            cut_dict = {
                "name" : "Fake CR",
                "cut" : "y_pred == 4",
            }
        
        elif region == "signal":

            cut_dict = {
                "name" : "Signal region",
                "cut" : "output_score_signal > 0.80",
            }

    else:
        raise Exception("Invalid channel argument")


    return cut_dict




