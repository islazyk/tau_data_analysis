#Data has is treated a bit differently as it's not used for training, hence never leaves the preprocessing stage to be padded and concatenated as backgrounds and signals
#So first load the preprocessed data.pkl files, perform padding and then load and apply the classifier. Then save back as flat ntuples. 

#External imports
import uproot 
import xgboost as xgb
import awkward as ak
import numpy as np
import pandas as pd
import pickle
import os, sys
from pathlib import Path

#Config path
sys.path.append(str(Path(os.getcwd()).parent) + '/Scripts/Definers/')
from PathsDefiner import paths_definer
path_pickles = paths_definer()

#Local imports
from new_padder import flatten_and_pad
from InsertPredictions import insert_preds

#small function to pad dataframes with missing variables
def pad_diff(df, all_cols, fill = -999):
    missing_cols = all_cols.difference(df.columns)
    for col in missing_cols:
        df[col] = fill 
    return df 

fake_scheme = "mc_fakes"
fold = "1"

pad_leptons = True  
pad_value = -999
variables_to_pad = ['ele_pt_0', 'ele_eta_0', 'ele_charge_0', 'ele_mtMet_0', 'ele_delPhiMet_0', 'mu_pt_0', 'mu_eta_0', 'mu_charge_0', 'mu_mtMet_0', 'mu_delPhiMet_0']
pad_channels = ["2had", "1had0lep"]

runs = ["Run2", "Run3"]
channels = ["1had0lep", "2had"]

for run in runs:
    for channel in channels:
        print(f"starting to process: {run} {channel} data")

        infile = f"/disk/atlas1/users/thillers/tauX/25.2.8/{run}/{channel}/pickles/data.pkl"
        if fake_scheme == "anti_ID":
            modelpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/models/{channel}_model_{fold}.json"
            outfile = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/{channel}/backgrounds/data_{channel}_{run}_model_{fold}.root" 
        elif fake_scheme == "mc_fakes":
            modelpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/models/{channel}_{fake_scheme}_vector_systs_model_1.json" 
            outfile = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/{channel}_{fake_scheme}_vector_systs/backgrounds/data_{channel}_{run}.root" 
        elif fake_scheme == "mixed_fakes":
            modelpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/models/{channel}_{fake_scheme}_v2_model_1.json"
            outfile = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/{channel}_{fake_scheme}_v2/backgrounds/data_{channel}_{run}.root" 
        else:
            raise Exception("Invalid fake scheme")

        #Load data and pad 
        with open(infile, "rb") as file:
            ar = pickle.load(file)
        ar = ar["data"]["data"]
        df = flatten_and_pad(ar)
        df["bkgOrigin"] = "data"

        #Load classifier and predict on data 
        model = xgb.Booster()
        model.load_model(modelpath)
        training_variables = model.feature_names

        df_test = df[training_variables]
        dtest = xgb.DMatrix(df_test)
        y_prob = model.predict(dtest)
        y_pred = [int(np.argmax(row)) for row in y_prob]
        df = insert_preds(df, y_prob = y_prob, channel = channel)
        #Insert run 
        df["Run"] = run


        #load an example fully processed file -- flat ntuple -- to pad the difference in variables
        if fake_scheme == "anti_ID":
            flat_ntuple_path = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/{channel}/backgrounds/ztautau_{channel}_{run}.root"
        elif fake_scheme == "mc_fakes":
            flat_ntuple_path = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/{channel}_{fake_scheme}_vector_systs/backgrounds/ztautau_{channel}_{run}.root" 
        elif fake_scheme == "mixed_fakes":
            flat_ntuple_path = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/{channel}_{fake_scheme}_v2/backgrounds/ztautau_{channel}_{run}.root"
        else:
            raise Exception("invalid fake scheme")
        with uproot.open(flat_ntuple_path) as tmp:
            tree = tmp["nominal"]
            ar = tree.arrays(library = "ak")
        flat_variables = ak.fields(ar)
        
        #Pad data with the remaining stuff 
        all_columns = set(df.columns) ^ set(flat_variables)
        all_columns = pd.Index(all_columns)
        df = pad_diff(df, all_columns)

        #Save to root
        with uproot.recreate(outfile) as f:
            data = {col : np.array(df[col]) for col in df.columns}
            f["nominal"] = data 
        
        print("saved successfully")
    

if pad_leptons:
    print("padding lepton variables for non-lepton channels for data")
    for channel in pad_channels:
        for run in runs:
            infile = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/{channel}_{fake_scheme}_vector_systs/backgrounds/data_{channel}_{run}"
            with uproot.open(f"{infile}.root") as f:
                df = f["nominal"].arrays(library="pd")

                # Apply padding to missing variables
                for var in variables_to_pad:
                    if var not in df.columns:
                        df[var] = pad_value

                #Save back padded
                outfile = f"{infile}_pad.root"
                with uproot.recreate(outfile) as f_out:
                    data = {col : np.array(df[col]) for col in df.columns}
                    f_out["nominal"] = data 

            print(f"Padded file saved as: {outfile}")