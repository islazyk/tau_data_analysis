#Test how systematics fare in this world of uproot 

#External imports
import uproot
import os, sys 
import pandas as pd 
import numpy as np 
import xgboost as xgb
import time
from pathlib import Path 
from collections import defaultdict

#Config path
sys.path.append(str(Path(os.getcwd()).parent) + '/Scripts/Definers/')
from PathsDefiner import paths_definer
path_pickles = paths_definer()

#Local imports
from InsertPredictions import insert_preds

def select_relevant_trees(trees):
    selected_trees = defaultdict(int)  # Dictionary to store the highest cycle per tree

    for tree in trees:
        name, cycle = tree.split(";")
        cycle = int(cycle)
        
        # Convert to uppercase for consistent matching and skip lowercase'nominal'
        if name == "NOMINAL" or name.startswith("NOMINAL_"):
            selected_trees[name] = max(selected_trees[name], cycle)

    # Rebuild the tree names with the highest cycle numbers
    return [f"{name};{cycle}" for name, cycle in selected_trees.items()]

channels = ["2had"]
runs = ["Run2"]

for channel in channels:
    for run in runs:
        print("starting the loop")

        if run == "Run2":
            run_name = "run2"
        elif run == "Run3":
            run_name == "run3"
        else:
            raise Exception("Invalid run")
            
        start = time.time()

        infile = f"/disk/atlas1/users/nifomin/tauX/ff_fodder_25.2.8/fftaus/trues_to_use_alt_fix/ttbar_{run_name}_{channel}_syst.root"
        outpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/systematics/{channel}_mc_fakes/"
        modelpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/models/{channel}_mc_fakes_model_1.json"

        model = xgb.Booster()
        model.load_model(modelpath)
        training_variables = model.feature_names

        with uproot.open(infile) as f:
            trees = f.keys() #get the different systematic trees and loop over them
            selected_trees = select_relevant_trees(trees)
            with uproot.recreate(f"{outpath}ttbar_{run_name}_{channel}_syst.root") as f_out:
                for tree in selected_trees:
                    print(f"Processing: {tree}")
                    df = f[tree].arrays(library = "pd")

                    if tree.startswith("NOMINAL;"):
                        print(f"Encountered nominal tree: skipping processing")
                    else:
                        df_test = df[training_variables]
                        DMatrix_test = xgb.DMatrix(df_test)
                        y_prob = model.predict(DMatrix_test)
                        y_pred = [int(np.argmax(row)) for row in y_prob]
                        df = insert_preds(df, y_prob, channel)

                    #Save to outgoing tree
                    data = {col : np.array(df[col]) for col in df.columns}
                    f_out[tree] = data

        stop = time.time()

        print(f"Time spend on {channel} ttbar systematics: {np.round(((stop - start) / 60),2)} minutes")
