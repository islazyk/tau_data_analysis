#External imports
import awkward as ak
import numpy as np
import pandas as pd
import pickle
import os, sys
from pathlib import Path

#Config path
sys.path.append(str(Path(os.getcwd()).parent) + '/Scripts/Definers/')
from PathsDefiner import paths_definer
path_pickles = paths_definer()

#Local imports
from new_padder import flatten_and_pad

#small function to pad dataframes with missing variables
def pad_diff(df, all_cols, fill = -999):
    missing_cols = all_cols.difference(df.columns)
    for col in missing_cols:
        df[col] = fill 
    return df 

#Define the background class scheme
backgrounds = ["singletop", "ttbar", "wenu", "wmunu", "wtaunu", "zee", "zmumu", "znunu", "ztautau", "ttX", "diboson"]
mc_fakes = [f"{x}_mc_fakes" for x in backgrounds]
other = ["ttX", "wenu", "wmunu", "zee", "zmumu", "znunu"]
tops = ["singletop", "ttbar"]

#Define the class numbering scheme
class_map_1had0lep = {
    "topquarks" : 0,
    "wtaunu" : 1,
    "ztautau" : 2,
    "diboson" : 3,
    "fake" : 4, 
    "signal" : 5,
    "other" : 10,
}

class_map_1had1lep = {
    "topquarks" : 0,
    "ztautau" : 1,
    "diboson" : 2,
    "fake" : 3,
    "signal" : 4,
    "other" : 10,
}

class_map_2had = {
    "topquarks" : 0,
    "ztautau" : 1,
    "diboson" : 2,
    "fake" : 3,
    "signal" : 4,
    "other" : 10,
}

#Configs
runs = ["Run2", "Run3"]
channels = ["1had1lep"]
mc_types = ["mc_fakes", "background", "signal"]

for run in runs:
    for channel in channels:
        print(f"processing {run} {channel} into a single dataframe")
        combined_background = []
        combined_signal = []
        combined_mc_fakes = []
        df_fake = pd.DataFrame()
        for mc_type in mc_types:
            
            if mc_type == "background":
                for background in backgrounds:
                    path = f"/disk/atlas1/users/thillers/tauX/25.2.8/{run}/{channel}/pickles/{background}.pkl"

                    with open(path, "rb") as infile:
                        ar = pickle.load(infile)
                    ar = ar[background][background]
                    if len(ar) < 1:
                        print(f"no background yield for {background}")
                        continue
                    df = flatten_and_pad(ar)
                    df["bkgOrigin"] = background

                    if background in other: #Config the classes for ML
                        df["class"] = "other"
                    elif "wtaunu" in background and "2had" in channel: #effectively 0 events here
                        df["class"] = "other"
                    elif "wtaunu" in background and "1had1lep" in channel: #effectively 0 events here aswell
                        df["class"] = "other"
                    elif background in tops:
                        df["class"] = "topquarks"
                    else:
                        df["class"] = background
                    combined_background.append(df)

            elif mc_type == "mc_fakes": #This is a bit of a mix between fake and background -- there are many fake MC files which needs to be added here
                for mc_fake in mc_fakes:
                    print(mc_fake)
                    path = f"/disk/atlas1/users/thillers/tauX/25.2.8/{run}/{channel}/pickles/{mc_fake}.pkl"

                    with open(path, "rb") as infile:
                        ar = pickle.load(infile)
                    ar = ar[mc_fake.removesuffix("_mc_fakes")][mc_fake.removesuffix("_mc_fakes")]
                    if len(ar) < 1:
                        print(f"no mc fakes for {mc_fake}")
                        continue 
                    df = flatten_and_pad(ar)
                    df["class"] = "fake"
                    combined_mc_fakes.append(df)

            elif mc_type == "signal":
                path = f"/disk/atlas1/users/thillers/tauX/25.2.8/{run}/{channel}/pickles/signal.pkl"
                with open(path, "rb") as infile:
                    ar = pickle.load(infile)
                    ar = ar["signal"]

                for signal_type in ar.keys():
                    if len(ar[signal_type]) < 1:
                        print(f"no signal yield for {signal_type}")
                        continue
                    df = flatten_and_pad(ar[signal_type])
                    df["class"] = "signal"
                    combined_signal.append(df)

            elif mc_type == "fake":
                path = f"/disk/atlas1/users/thillers/tauX/25.2.8/{run}/{channel}/pickles/fake.pkl"
                with open(path, "rb") as infile:
                    ar = pickle.load(infile)
                    ar = ar["fake"]["fake"]
                    df = flatten_and_pad(ar)
                    df["class"] = "fake"
                    df_fake = df 

            else:
                raise Exception("Error in mc type")

            del df 

        df_bkgs = pd.concat(combined_background, ignore_index = True) 
        df_sgns = pd.concat(combined_signal, ignore_index = True)
        if "mc_fakes" in mc_types:
            print("concatenating mc fakes")
            df_fake = pd.concat(combined_mc_fakes, ignore_index = True)

        #Find and pad the differences in variables between the s,b and fake dataframes so we can pad
        all_columns = set(df_sgns.columns).union(set(df_bkgs.columns), set(df_fake.columns))
        all_columns = pd.Index(all_columns)

        df_sgns = pad_diff(df_sgns, all_columns)
        df_bkgs = pad_diff(df_bkgs, all_columns)
        df_fake = pad_diff(df_fake, all_columns)
        df = pd.concat([df_sgns, df_bkgs, df_fake], axis = 0)

        #map class to integer values
        if "2had" in channel:
            df["class"] = df["class"].map(class_map_2had)
        elif "1had1lep" in channel:
            df["class"] = df["class"].map(class_map_1had1lep)
        elif "1had0lep" in channel:
            df["class"] = df["class"].map(class_map_1had0lep)
        else:
            raise Exception("invalid channel -- class map combination")

        #add run info as a column
        df["Run"] = run 

        #shuffle so the resulting dataframe has a uniform sub-division of signal, fakes and backgrounds
        df = df.sample(frac = 1)

        if "mc_fakes" in mc_types: #save depending on the fake scheme 
            outpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/{run}/{channel}/pickles/{run}_{channel}_dataframe_mc_fakes_no_wtaunu.csv" #check later
        else:
            outpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/{run}/{channel}/pickles/{run}_{channel}_dataframe.csv"
        df.to_csv(outpath, index = False)

        print(f"successfully saved the combined dataframe for {run} {channel}")