#External imports
import pandas as pd 
import numpy as np 
import xgboost as xgb
import matplotlib.pyplot as plt 
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay 
import os, sys 
from pathlib import Path 

#Config path
sys.path.append(str(Path(os.getcwd()).parent) + '/Scripts/Definers/')
from PathsDefiner import paths_definer
path_pickles = paths_definer()

# Custom callback class for printing log loss every 10th iteration
class CustomEvalCallback(xgb.callback.TrainingCallback):
    def __init__(self, print_every_n):
        self.print_every_n = print_every_n

    def after_iteration(self, model, epoch, evals_log):
        if epoch % self.print_every_n == 0:
            train_logloss = evals_log['train']['mlogloss'][-1]
            val_logloss = evals_log['validation']['mlogloss'][-1]
            print(f"[{epoch}] train-mlogloss:{train_logloss:.5f}\tvalidation-mlogloss:{val_logloss:.5f}")
        return False  # Returning False means no early stopping


class_map_1had = {
    0: "topquarks",
    1: "wtaunu",
    2: "ztautau",
    3: "diboson",
    4: "fake", 
    5: "signal",
}

class_map_2had = {
    0: "topquarks",
    1: "ztautau",
    2: "diboson",
    3: "fake",
    4: "signal",
}

#training config
depth = 5
numBoosters = 10
learning_rate = 0.1
gamma = 0.1
reg_lambda = 0
early_stopping_rounds = 10

plot_loss = False 

channels = ["2had"]

for channel in channels:
    print(f"initializing kfold training in the {channel} channel")
    model = None
    eval_results = {}

    basepath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/data/{channel}/{channel}_train_set"

    train_sets = [basepath+"_1.csv", basepath+"_2.csv", basepath+"_3.csv"]
    valid_set = basepath+"_4.csv"
    test_set = basepath+"_5.csv"

    for i,train_set in enumerate(train_sets):
        print(f"started processing fold {i+1} of {len(train_sets)}")

        df_train = pd.read_csv(train_set) 
        X_train = df_train.drop(columns = ["class", "class_weight"])
        y_train = df_train["class"]
        X_train_class_weights = df_train["class_weight"]

        df_valid = pd.read_csv(valid_set)
        X_valid = df_valid.drop(columns = ["class", "class_weight"])
        y_valid = df_valid["class"]
        X_valid_class_weights = df_valid["class_weight"]

        if "2had" in channel:
            num_class = 5
            class_map = class_map_2had
        else:
            num_class = 6
            class_map = class_map_1had

        hyperparameters = {
            'booster':'gbtree',     
            'learning_rate': learning_rate,     
            'max_depth': depth,                     
            'min_child_weight':1,                  
            'gamma':gamma,                          
            'subsample':1.0,                      
            'colsample_bytree':1.0,             
            'reg_alpha':0,                        
            'reg_lambda':reg_lambda,                              
            'max_delta_step':0,               
            'objective':"multi:softprob",                                             
            'seed':1,                            
            'num_class': num_class,
            "eval_metric": "mlogloss",       
        }

        dtrain = xgb.DMatrix(X_train, label = y_train, weight = X_train_class_weights)
        dvalid = xgb.DMatrix(X_valid, label = y_valid, weight = X_valid_class_weights)
        evals = [(dtrain, "train"), (dvalid, "validation")]

        # Using the custom callback for logging every 10 iterations
        custom_callback = CustomEvalCallback(print_every_n=10)

        if model is None:
            #initialize if the first iteration
            model = xgb.train(hyperparameters, dtrain, num_boost_round = numBoosters, evals = evals, early_stopping_rounds = early_stopping_rounds, evals_result = eval_results, callbacks = [custom_callback], verbose_eval = False)
        else:
            #else..update the existing model
            model = xgb.train(hyperparameters, dtrain, num_boost_round = numBoosters, evals = evals, early_stopping_rounds = early_stopping_rounds, evals_result = eval_results, xgb_model = model, callbacks = [custom_callback], verbose_eval = False)
        print(f"successfully trained on fold {i+1} of {len(train_sets)}")

        del df_train, df_valid 

    # Plot the training and validation loss 
    if plot_loss:
        epochs = len(eval_results['train']['mlogloss'])
        x_axis = range(0, epochs)
        plt.figure(figsize=(10, 6))
        plt.plot(x_axis, eval_results['train']['mlogloss'], label='Train')
        plt.plot(x_axis, eval_results['validation']['mlogloss'], label='Validation')
        plt.title('XGBoost Training and Validation Loss')
        plt.xlabel('Number of Trees')
        plt.ylabel('Log Loss')
        plt.legend()
        plt.show()

    #once the model is trained, evaluate on the test set
    print("starting to predict on test data")
    df_test = pd.read_csv(test_set)

    X_test = df_test.drop(columns = ["class", "class_weight"])
    y_test = df_test["class"]

    dtest = xgb.DMatrix(X_test, label = y_test)
    
    y_prob = model.predict(dtest)
    y_pred = [int(np.argmax(line)) for line in y_prob]

    cm = confusion_matrix(y_test, y_pred)
    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    
    labels = [class_map[i] for i in range(num_class)]
    cm_display = ConfusionMatrixDisplay(confusion_matrix=cm_normalized, display_labels= labels)

    # Plot the confusion matrix
    plt.figure(figsize=(10, 6))
    cm_display.plot(cmap=plt.cm.Blues)
    plt.title('Confusion Matrix')
    plt.show()

    print("job done")