#External imports
import pandas as pd 
import numpy as np 
import os, sys 
from pathlib import Path 

#Config path
sys.path.append(str(Path(os.getcwd()).parent) + '/Scripts/Definers/')
from PathsDefiner import paths_definer
path_pickles = paths_definer()

#Local imports
from LoadVariables import trainingVariables

nfolds = 5
fake_scheme = "mc_fakes"

channels = ["1had1lep"]

for channel in channels:
    BDT_inputs = trainingVariables(channel = channel)

    if fake_scheme == "anti_ID":
        infile1 = f"/disk/atlas1/users/thillers/tauX/25.2.8/Run2/{channel}/pickles/Run2_{channel}_dataframe.csv"
        infile2 = f"/disk/atlas1/users/thillers/tauX/25.2.8/Run3/{channel}/pickles/Run3_{channel}_dataframe.csv"
    elif fake_scheme == "mc_fakes":
        infile1 = f"/disk/atlas1/users/thillers/tauX/25.2.8/Run2/{channel}/pickles/Run2_{channel}_dataframe_mc_fakes_no_wtaunu.csv"
        infile2 = f"/disk/atlas1/users/thillers/tauX/25.2.8/Run3/{channel}/pickles/Run3_{channel}_dataframe_mc_fakes_no_wtaunu.csv"

    nrows1 = sum(1 for row in open(infile1)) -1
    nrows2 = sum(1 for row in open(infile2)) -1 

    chunksize1 = nrows1 // nfolds 
    chunksize2 = nrows2 // nfolds 

    csv1 = pd.read_csv(infile1, chunksize = chunksize1) #due to imperfect division there will be a nfolds + 1 csv file with 1-4 events. This is an edge effect so we don't really care
    csv2 = pd.read_csv(infile2, chunksize = chunksize2)

    for i, (df1, df2) in enumerate(zip(csv1, csv2)):
        print("processing new chunk")
        print(f"chunk 1 shape: {df1.shape}. MET mean: {np.mean(df1.met)}")
        print(f"chunk 2 shape: {df2.shape}. MET mean: {np.mean(df2.met)}")

        df = pd.concat([df1, df2], axis = 0).reset_index(drop = True)
        df = df.sample(frac = 1)
        print(f"combined shape: {df.shape}. MET mean: {np.mean(df.met)}")

        #divide into training variables and metadata, do not perform any shuffling on these after. Also define the class weights
        #maybe here define class weights such that each fake background gets equally large weight. as it stands now there is an imbalance between ttbar, diboson, wtaunu fakes etc
        df = df[df["class"] != 10] #remove the other class, not really sure how to reinsert, but I guess this can be done later. Not very important
        n_minority = df["class"].value_counts().min()
        class_weights = n_minority / df["class"].value_counts()
        df["class_weight"] = df["class"].map(class_weights)

        df_train = df[BDT_inputs]
        df_metadata = df.drop(columns = BDT_inputs)
        
        if fake_scheme == "anti_ID":
            df_train.to_csv(f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/data/{channel}/{channel}_train_set_{i+1}.csv", index = False)
            print("successfully saved training chunk")
            df_metadata.to_csv(f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/data/{channel}/{channel}_metadata_set_{i+1}.csv", index = False)
            print("successfully saved metadata chunk")
        
        elif fake_scheme == "mc_fakes":
            df_train.to_csv(f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/data/{channel}_mc_fakes_no_wtaunu/{channel}_train_set_{i+1}.csv", index = False) #change back
            print("successfully saved training chunk")
            df_metadata.to_csv(f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/data/{channel}_mc_fakes_no_wtaunu/{channel}_metadata_set_{i+1}.csv", index = False) #change back
            print("successfully saved metadata chunk")

        else:
            raise Exception("Invalid fake scheme")