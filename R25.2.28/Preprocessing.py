#External imports
import uproot as ur
import awkward as ak
import numpy as np
import pickle
import os, sys
from pathlib import Path

#Config path
sys.path.append(str(Path(os.getcwd()).parent) + '/Scripts/Definers/')
from PathsDefiner import paths_definer
path_pickles = paths_definer()

#Local imports
from newAnalysisProcessor import new_analysis_processor
from LoadVariables import loadVariables
from newMCMerger import new_mc_merger

#Small function to add the fake origin to the collective fake background
def add_origin(ar, origin_type):
    updated_dict = {}
    for key, arr in ar.items():
        if len(arr) == 0:
            updated_dict[key] = arr
        else:
            updated_array = ak.with_field(arr, key, origin_type)
            updated_dict[key] = updated_array 
    return(updated_dict)


#Script configurations
runs = ["Run2", "Run3"]
channels = ["1had0lep"]
#mc_types = ["signal", "fake", "data", "singletop", "ttbar", "wenu", "wmunu", "wtaunu", "zee", "zmumu", "znunu", "ztautau", "ttX", "diboson"]

mc_types = ["singletop", "ttbar", "wenu", "wmunu", "wtaunu", "zee", "zmumu", "znunu", "ztautau", "ttX", "diboson"]
mc_fakes = True

for run in runs:
    for channel in channels:
        for mc_type in mc_types:
            print(f"processing {run} {channel} {mc_type}")

            if mc_type == "fake": mc_fakes = False #choose one

            if mc_type == "fake":
                variables = loadVariables(channel = channel, fakeWeights = True)
            else:
                variables = loadVariables(channel = channel, fakeWeights = False)

            mc_dict = new_analysis_processor(
                run = run,
                channel = channel,
                load_features = variables,
                mc_type = mc_type,
                mc_fakes = mc_fakes 
            )

            if (mc_type == "fake") or mc_fakes == True: #add origin to fakes
                mc_dict = add_origin(mc_dict, "fakeOrigin")

            if (mc_type == "fake") or (mc_type == "signal"): #merge select objects
                mc_dict = new_mc_merger(
                    mc_type = mc_type,
                    indict = mc_dict,
                )

            analysis_container = {
                mc_type : mc_dict
            }
            if mc_fakes:
                savepath = f"/disk/atlas1/users/thillers/tauX/25.2.8/{run}/{channel}/pickles/{mc_type}_mc_fakes.pkl"
            else:
                savepath = f"/disk/atlas1/users/thillers/tauX/25.2.8/{run}/{channel}/pickles/{mc_type}.pkl"

            with open(savepath, "wb") as file:
                pickle.dump(analysis_container, file)

            print(f"successfully saved {run} {channel} {mc_type}")

print("Job done")
