#Extract the "other" class and get it to a state where we can use the trained classifier to predict on it. Then save it based on run, channel and bkgOrigin.
import numpy as np 
import pandas as pd 
import uproot
import ast 
import os,sys
import xgboost as xgb
from pathlib import Path 

#Config path
sys.path.append(str(Path(os.getcwd()).parent) + '/Scripts/Definers/')
from PathsDefiner import paths_definer
path_pickles = paths_definer()

#Local imports
from InsertPredictions import insert_preds

fake_scheme = "mc_fakes"
systs = True 
vector_systs = ["mcEventWeights_fsr", "mcEventWeights_alphas", "mcEventWeights_scale"]

channels = ["2had"]

def load_other(infile, chunksize=10000): #This works as other is a minority class
    return pd.concat(
        (chunk[chunk["class"] == 10] for chunk in pd.read_csv(infile, chunksize=chunksize)),
        ignore_index=True
    )

for channel in channels:
    print(f"considering channel: {channel}")

    if fake_scheme == "anti_ID":
        infile1 = f"/disk/atlas1/users/thillers/tauX/25.2.8/Run2/{channel}/pickles/Run2_{channel}_dataframe.csv"
        infile2 = f"/disk/atlas1/users/thillers/tauX/25.2.8/Run3/{channel}/pickles/Run3_{channel}_dataframe.csv"
        modelpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/models/{channel}_model_1.json" #all folds are degenerate, just pick one.
        outpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/backgrounds/{channel}"
    elif fake_scheme == "mc_fakes":
        infile1 = f"/disk/atlas1/users/thillers/tauX/25.2.8/Run2/{channel}/pickles/Run2_{channel}_dataframe_mc_fakes_vector_systs.csv" #change back
        infile2 = f"/disk/atlas1/users/thillers/tauX/25.2.8/Run3/{channel}/pickles/Run3_{channel}_dataframe_mc_fakes_vector_systs.csv" #change back
        modelpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/models/{channel}_{fake_scheme}_vector_systs_model_1.json" #all folds are degenerate, just pick one. #change back
        outpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/{channel}_{fake_scheme}_vector_systs/backgrounds/" #change back
    elif fake_scheme == "mixed_fakes":
        infile1 = f"/disk/atlas1/users/thillers/tauX/25.2.8/Run2/{channel}/pickles/Run2_{channel}_dataframe_mixed_fakes_v2.csv" #change back
        infile2 = f"/disk/atlas1/users/thillers/tauX/25.2.8/Run3/{channel}/pickles/Run3_{channel}_dataframe_mixed_fakes_v2.csv" #change back
        modelpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/models/{channel}_{fake_scheme}_v2_model_1.json" #all folds are degenerate, just pick one. #change back
        outpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/{channel}_{fake_scheme}_v2/backgrounds/" #change back
    else:
        raise Exception("Invalid fake scheme")

    #get classifier variables
    model = xgb.Booster()
    model.load_model(modelpath)
    training_variables = model.feature_names

    #combine dataframes 
    df1 = load_other(infile1)
    df2 = load_other(infile2)
    df = pd.concat([df1, df2], ignore_index = True)

    #slice on training variables and make predictions
    df_test = df[training_variables]
    DMatrix_test = xgb.DMatrix(df_test)
    y_prob = model.predict(DMatrix_test)
    y_pred = [int(np.argmax(row)) for row in y_prob]
    df = insert_preds(df, y_prob = y_prob, channel = channel)

    if systs: 
        for vector_syst in vector_systs:
            df[vector_syst] = df[vector_syst].apply(lambda x: ast.literal_eval(x))

    print("dataframe construction complete, starting the saving process")
    for run in ["Run2", "Run3"]:
        for bkg in df.bkgOrigin.unique():
            if "-999" in bkg: continue #should not really happen but whatever
            print(f"background type: {bkg}")
            df_out = df[df["Run"] == run]
            df_out = df_out[df_out["bkgOrigin"] == bkg]
            outfile = f"{outpath}{bkg}_{channel}_{run}.root"
            with uproot.recreate(outfile) as f:
                data = {col: np.array(df_out[col]) for col in df_out.columns}
                f["nominal"] = data 
