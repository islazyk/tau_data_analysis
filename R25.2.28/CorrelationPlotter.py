#external imports
import os, sys 
import matplotlib.pyplot as plt 
import seaborn as sns 
import pandas as pd 
import numpy as np 
from pathlib import Path 

#Config path
sys.path.append(str(Path(os.getcwd()).parent) + '/Scripts/Definers/')
from PathsDefiner import paths_definer
path_pickles = paths_definer()

#Correlation plots only need to see processed training data. And as each fold is ~ equal it will suffice to load 
#only a single training fold

fold = "1"
num_variables = 15 
channels = ["2had", "1had1lep", "1had0lep"]

for channel in channels:

    inpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/data/{channel}_mc_fakes/{channel}_train_set_{fold}.csv"
    df = pd.read_csv(inpath)
    df = df.drop(columns="class")

    correlation_matrix = df.corr()
    correlation_pairs = correlation_matrix.abs().unstack().sort_values(kind="quicksort", ascending=False)
    #Remove self-correlations
    correlation_pairs = correlation_pairs[correlation_pairs < 1]

    #Get num_variables unique variables
    selected_vars = set()
    for var1, var2 in correlation_pairs.index:
        selected_vars.add(var1)
        selected_vars.add(var2)
        if len(selected_vars) >= num_variables:
            break

    selected_vars = list(selected_vars)  # Convert the set back to a list
    reduced_correlation_matrix = correlation_matrix.loc[selected_vars, selected_vars]

    plt.figure(figsize=(12, 10))
    sns.heatmap(reduced_correlation_matrix, annot=True, fmt=".2f", cmap="coolwarm", square=True, cbar_kws={"shrink": .8})
    plt.title(f" {channel} Correlation Matrix", fontsize=16)
    plt.savefig(f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/plots/{channel}_mc_fakes/{channel}_correlations.pdf")
    plt.show()