#For whatever reason the first time the latex path is defined it does not register, so a bootleg solution is to just load it twice
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import atlas_mpl_style as ampl
import seaborn as sns
import sys

# Ensure proper LaTeX path format, add a colon before the path
os.environ['PATH'] += ':/cvmfs/sft.cern.ch/lcg/external/texlive/2023/bin/x86_64-linux'
print("Updated PATH:", os.environ['PATH'])
mpl.rcdefaults()
plt.rc('text', usetex=True)  
# Configure ATLAS style with LaTeX enabled
ampl.use_atlas_style(usetex=True)  # Enable ATLAS style with LaTeX
sns.set_theme()

#External imports
import atlas_mpl_style as ampl
import pandas as pd 
import seaborn as sns 
import numpy as np 
import xgboost as xgb
import matplotlib.pyplot as plt 
import uproot 
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay 
import os, sys 
from pathlib import Path 

#Config matplotlib with ATLAS and LaTeX
os.environ['PATH'] += '/cvmfs/sft.cern.ch/lcg/external/texlive/2023/bin/x86_64-linux'
plt.rc('text', usetex=True)             # Matplotlib LaTeX Compatibility
ampl.use_atlas_style(usetex=True)       # ATLAS Style

#supress warning and shit
# Set the default font family to DejaVu Sans
plt.rcParams['font.family'] = 'DejaVu Sans'
import warnings 
warnings.filterwarnings("ignore", category=UserWarning, message=".*findfont.*")

#Config path
sys.path.append(str(Path(os.getcwd()).parent) + '/Scripts/Definers/')
from PathsDefiner import paths_definer
path_pickles = paths_definer()

from PlotDictionary import plot_variables
from CutDictionary import cuts 

weights = ["tau_weight", "mu_weight", "ele_weight", "jvt_weight", "bjet_weight", "lumiweight", "pileupweight", "mcEventWeight", "beamSpotWeight"]
fake_anti_ID_weights = ["weight_zmm_hjvt", "weight_mj_ljvt"]
fake_scheme = "mc_fakes"

channels = ["1had1lep"]
runs = ["Run2"]
regions = ["topCR", "dibosonCR"]

#regions = ["preselection", "topCR", "ZCR", "dibosonCR", "fakeCR", "signal"]

for channel in channels:
    for region in regions:
        print(f"making plots for: {channel} {region}")

        basepath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/{channel}_{fake_scheme}_no_wtaunu/backgrounds/" #revert
        outpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/plots/{channel}_{fake_scheme}_no_wtaunu/" #revert

        if "1had0lep" in channel:
            backgrounds = ["diboson", "fakes", "singletop", "ttbar", "ztautau", "wtaunu", "other", "data"]
        else:
            backgrounds = ["diboson", "fakes", "singletop", "ttbar", "ztautau", "other", "data"]
        
        variables = plot_variables(channel = channel) #add a channel argument here to select say two taus for 2had and electron/muons for 1had1lep
        colors = sns.color_palette("Set3", n_colors = len(backgrounds) -1) #subtract data 

        #get the cuts 
        cuts_dict = cuts(channel = channel, region = region)
        cut_expression = cuts_dict["cut"]
        cut_name = cuts_dict["name"]

        #split into run2 and run3
        for run in runs:
            df_bkg = {}
            df_data = None

            #loop over backgrounds and add them to a combined dataframe
            for background in backgrounds:
                inpath = f"{basepath}{background}_{channel}_{run}.root"
                with uproot.open(inpath) as infile:
                    tree = infile["nominal"]
                    df = tree.arrays(library = "pd")
                    
                #apply cuts
                if region != "preselection":
                    df.query(cut_expression, inplace = True)

                if background == "data":
                    df_data = df 
                else:
                    df_bkg[background] = df 

            #start looping over the variables dictionary
            for variable_dict in variables:
                variable = variable_dict["name"]
                print(f"plotting: {variable}")
                bin_edges = np.linspace(variable_dict["xaxis_start"], variable_dict["xaxis_end"], variable_dict["bins"] +1)
                bin_centers = (bin_edges[:-1]+ bin_edges[1:]) / 2

                bkg_hists = []
                bkg_labels = []

                #start creating the figure. Need a top part for the histograms and a bottom part for the ratio plots
                fig, (ax1, ax2) = plt.subplots(2, 1, gridspec_kw = {"height_ratios" : [3,1]}, figsize = (8,8), sharex = True)

                #for each background and variable create a list to plot
                for bkg, df, color in zip(df_bkg.keys(), df_bkg.values(), colors):
                    values = df[variable]
                    if "divide" in variable_dict:
                        values = values / variable_dict["divide"]

                    comb_weight = df[weights].prod(axis = 1)
                    hist, _ = np.histogram(values, bins = bin_edges, weights = comb_weight)
                    bkg_hists.append(hist)
                    bkg_labels.append(bkg)
                    
                ax1.hist([bin_edges[:-1]] * len(bkg_hists), bins = bin_edges, weights = bkg_hists, label = bkg_labels, stacked = True, histtype = "stepfilled", alpha = 0.7, color = colors)

                #make a red line displaying the combined background
                combined_bkg = np.sum(bkg_hists, axis = 0)
                ax1.hist(bin_edges[:-1], bins=bin_edges, weights=combined_bkg, histtype="step", color="red", linewidth=2)

                #display data as black dots -- errorbars
                if df_data is not None:
                    data_values = df_data[variable]
                    if "divide" in variable_dict:
                        data_values = data_values / variable_dict["divide"]

                    data_hist, _ = np.histogram(data_values, bins = bin_edges)
                    data_plot = ax1.errorbar(bin_centers, data_hist, yerr = np.sqrt(data_hist), fmt = "o", color = "black", label = "Data") #might change the y_err to relative error - so sqrt(N) / N

                #Create legends and move data to the top of the legend
                handles, labels = ax1.get_legend_handles_labels()
                ax1.legend(handles, labels, loc = "upper right")

                #Config the upper plot
                ax1.set_ylabel(variable_dict["yaxis_label"])
                max_y = max(np.sum(bkg_hists, axis = 0))
                if max_y <= 0:
                    break
                ax1.set_ylim(bottom = 0, top = max_y * 1.30) #make the y-axis slightly larger to have space for the run info
                ax1.set_xlim(left = variable_dict["xaxis_start"], right = variable_dict["xaxis_end"])

                if run == "Run2":
                    run_info = r"$\sqrt{s} = 13$ TeV, $\mathcal{L} = 140.1 fb^{-1},$"
                elif run == "Run3":
                    run_info = r"$\sqrt{s} = 13.6$ TeV, $\mathcal{L} = 51.8 fb^{-1},$"
                else:
                    raise Exception("Wait for Run4 man")
                ax1.text(0.55, 0.95, run_info + " " +cut_name, ha = "center", va = "center", transform = ax1.transAxes, fontsize = 12)

                #Creating the lower plot
                if df_data is not None:
                    with np.errstate(divide = "ignore", invalid = "ignore"): #ignore the zero yield bins, just set them to zero -- benefit of automatically fixing events which are not always defined like ele/mu variables.
                        ratio = np.divide(data_hist, combined_bkg, out = np.zeros_like(data_hist, dtype = np.float64), where = combined_bkg != 0)
                        ratio_error = np.clip(np.divide(np.sqrt(data_hist), combined_bkg, out=np.zeros_like(data_hist, dtype=np.float64), where = combined_bkg != 0), 0, np.inf)
                        stat_error = np.divide(np.sqrt(combined_bkg), combined_bkg, out = np.zeros_like(combined_bkg, dtype = np.float64), where = combined_bkg != 0)

                    ax2.errorbar(bin_centers, ratio, yerr = ratio_error, fmt = "o", color = "black")
                    ax2.fill_between(bin_edges, np.append(1 - stat_error, 1 - stat_error[-1]), np.append(1 + stat_error, 1 + stat_error[-1]), step='post', color='red', alpha=0.3)
                    ax2.set_ylabel("Data / SM")
                    ax2.axhline(1, color = "red", linestyle = "-", linewidth = 1)

                #Config the lower plot
                ax2.set_xlabel(variable_dict["xaxis_label"])
                ax2.set_ylim(0.5, 1.5)
                ax2.set_xlim(left = variable_dict["xaxis_start"], right = variable_dict["xaxis_end"])
            
                plt.tight_layout()
                ampl.draw_atlas_label(0.02, 0.97, ax = ax1)
                #fig.suptitle(f"{run} - {variable}", fontsize  = 16, y = 1.0)

                plt.savefig(f"{outpath}{region}_{run}_{variable}.pdf")