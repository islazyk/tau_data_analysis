#Input Nikolai's fakes and predict on them. Then save back as flat ntuples

#External imports
import numpy as np 
import pandas as pd 
import xgboost as xgb 
import os
import sys 
import uproot 
import awkward as ak 
from pathlib import Path 

#Config path
sys.path.append(str(Path(os.getcwd()).parent) + '/Scripts/Definers/')
from PathsDefiner import paths_definer
path_pickles = paths_definer()

#local imports
from InsertPredictions import insert_preds


fake_version = "fix7"
remove_and_reinsert = True 
fold = "1"
model_type = "mc_fakes"

channels = ["1had0lep"]
runs = ["Run2", "Run3"]

for channel in channels:
    for run in runs:
        print(f"starting to predict on {channel} {run} fakes")

        if "2had" in channel:
            naming_scheme_tau = "2tau"
        else:
            naming_scheme_tau = channel

        if "Run2" in run:
            naming_scheme_run = "run2"
        else:
            naming_scheme_run = "run3"

        modelpath = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/models/{channel}_{model_type}_model_{fold}.json"
        infile = f"/disk/atlas1/users/nifomin/tauX/ff_fodder_25.2.8/fftaus_ml/fakes_to_use/fakes_{naming_scheme_run}_{naming_scheme_tau}_{fake_version}.root"
        outfile = f"/disk/atlas1/users/thillers/tauX/25.2.8/ML/flat_ntuples/{channel}_{model_type}/backgrounds/fakes_{channel}_{run}_mc_fakes_on_antiID.root" 

        model = xgb.Booster()
        model.load_model(modelpath)
        training_variables = model.feature_names 

        with uproot.open(infile) as tmp:
            tree = tmp["NOMINAL"]
            df = tree.arrays(library = "pd")

        df_test = df[training_variables]
        dtest = xgb.DMatrix(df_test)
        y_prob = model.predict(dtest)
        y_pred = [int(np.argmax(row)) for row in y_prob]
        df = insert_preds(df, y_prob = y_prob, channel = channel)

        with uproot.recreate(outfile) as f:
            data = {col : np.array(df[col]) for col in df.columns}
            f["nominal"] = data 

        print("saved successfully")