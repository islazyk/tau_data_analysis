#Here we want to check the performance by dropping Phi and tauNN variables

#External imports
import uproot as ur
import json 
import awkward as ak
import numpy as np
import pandas as pd
import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
import sklearn
import xgboost as xgb
import shap
from tqdm import tqdm
import pickle
import matplotlib.patches as mpatches
from matplotlib.backends.backend_pdf import PdfPages
from sklearn.model_selection import RepeatedStratifiedKFold
import warnings
import os, sys
from pathlib import Path
warnings.filterwarnings('ignore')

#Config matplotlib with ATLAS and LaTeX
os.environ['PATH'] += '/cvmfs/sft.cern.ch/lcg/external/texlive/2020/bin/x86_64-linux'
plt.rc('text', usetex=True)             # Matplotlib LaTeX Compatibility
ampl.use_atlas_style(usetex=True)       # ATLAS Style

#Config path
sys.path.append(str(Path(os.getcwd()).parent) + '/Scripts/Definers/')
from PathsDefiner import paths_definer
path_pickles = paths_definer()

#Local imports
from ObjectiveDefiner import objective_definer
from TrainTestSplitter import split_data
from ResultsPresenter import results_presenter
from LossPlotter import loss_plotter
from ConfusionMatrixPlotter import confusion_matrix_plotter
from FeatureExtractor import feature_extractor

#Configs
channels = ["1had1lep", "2tau", "1had0lep"]
diboson_sample = "new_diboson" #Change accordingly

metric = "mlogloss"
objective = "multi:softproba"
framework_type = 'XGBoost'
algorithm_type = 'multiclass-classification'
test_size = 0.2
split = "train_test"

#Training parameters
n_trees = 50000
learning_rate = 0.1
depth = 5
gamma = 0.1

metaBothRuns = {
    "run" : "bothRuns",
    "release" : "R24",
    "analysis_base" : "24.2.28",
    "campaigns" : ["MC20a", "MC20d", "MC20e", "MC23a", "MC23c"],
    "architecture" : "RNN",
}

for channel in channels:
    print(f"initializing training in the {channel} channel")
    print("-----------------------------------------------")

    #Load both dataframes and combine them
    path_Run2 = f"/disk/atlas3/users/thillers/tauX/Run2/{channel}/pickles/{diboson_sample}/dataframe.pkl"
    path_Run3 = f"/disk/atlas3/users/thillers/tauX/Run3/{channel}/pickles/{diboson_sample}/dataframe_VV_tweak.pkl"


    with open(path_Run2, "rb") as file:
        df2 = pickle.load(file)
    with open(path_Run3, "rb") as file:
        df3 = pickle.load(file)

    df_Run2 = df2["df"].sample(frac = 1).reset_index(drop = True)
    df_Run3 = df3["df"].sample(frac = 1).reset_index(drop = True)

    #Add a label to indicate which Run a given sample is from
    df_Run2["run"] = "Run2"
    df_Run3["run"] = "Run3"

    #Harmonize the dictionary layout with other scripts
    df_bothRuns = pd.concat([df_Run2, df_Run3], axis = 0, ignore_index = True)
    df = {
        "meta" : metaBothRuns,
        "region" : df2["meta"],
        "include" : df2["include"],
        "df" : df_bothRuns,
        "labels" : df2["labels"],
    }

    #Extra random shuffle for good measure - now also the run should be mixed in randomly
    df["df"] = df["df"].sample(frac = 1).reset_index(drop = True)

    #Don't need these anymore and this will be quite memory intensive
    del df_Run2
    del df_Run3

    #Add class weights to the training set simply defined as W_i = N(minority class)/N(class_i)
    num_minority_class = df["df"]["class"].value_counts().min()
    class_weights = num_minority_class / df["df"]["class"].value_counts()
    df["df"]["class_weight"] = df["df"]["class"].map(class_weights)

    #Dropping certain buggy/bad kinematics
    print("checking if muon type is in the dataframe and dropping it")
    if "mu_muonType_0" in df["df"].columns:
        df["df"].drop(["mu_muonType_0"], axis = 1, inplace = True)
        print(f"dropped muon type from the {channel} channel")

    else:
        print(f"muon type not present in the {channel} channel, continuing")
        pass 

    print("attempting to remove leading tau width")
    df["df"].drop(["tau_width_0"], axis = 1, inplace = True)
    if "tau_width_0" not in df["df"].columns:
        print("successfully dropped leading tau width")

    if "2tau" in channel:
        print(f"attempting to remove subleading tau width in the {channel} channel")
        df["df"].drop(["tau_width_1"], axis = 1, inplace = True)
        if "tau_width_1" not in df["df"].columns:
            print("successfully dropped subleading tau width")


    #Drop Phi variables
    _1had0lep_list = ["met_phi", "tau_phi_0", "jet_phi_0", "jet_phi_1", "jet_phi_2", "tau_NNDecayMode_0"]

    if "1had0lep" in channel:
        print(f"removind phi variables for the {channel} channel")
        df["df"].drop(_1had0lep_list, axis = 1, inplace = True)

    _1had1lep_list = ["met_phi", "tau_phi_0", "jet_phi_0", "jet_phi_1", "jet_phi_2", "ele_phi_0", "mu_phi_0", "tau_NNDecayMode_0"]

    if "1had1lep" in channel:
        print(f"removing phi variables for the {channel} channel")
        df["df"].drop(_1had1lep_list, axis = 1, inplace = True)

    _2had_list = ["met_phi", "tau_phi_0", "tau_phi_1", "jet_phi_0", "jet_phi_1", "jet_phi_2", "tau_NNDecayMode_0", "tau_NNDecayMode_1"]

    if "2tau" in channel:
        print(f"dropping phi variables for the {channel} channel")
        df["df"].drop(_2had_list, axis = 1, inplace = True)

    X, y, X_train, X_test, y_train, y_test = split_data(df = df['df'], target = "class", split = split, test_size = test_size)

    #Remove some variables from training to be re-instated later
    X_selection_name, X_train_selection_name, X_test_selection_name = feature_extractor(feature = "selection_name", X = X, X_train = X_train, X_test = X_test)
    X_class_weight, X_train_class_weight, X_test_class_weight = feature_extractor(feature = "class_weight", X = X, X_train = X_train, X_test = X_test)
    X_weight, X_train_weight, X_test_weight = feature_extractor(feature = 'weight', X = X, X_train = X_train, X_test = X_test)
    X_run, X_train_run, X_test_run = feature_extractor(feature = "run", X = X, X_train = X_train, X_test = X_test)


    #Create outpaths
    outpath = f"/disk/atlas3/users/thillers/tauX/featureTests/no_NNDecayMode/{channel}/"
    os.makedirs(outpath, exist_ok = True)
    print(f"dumping results into {outpath}")

    hyperparameters = {
        'booster':'gbtree',

        'n_estimators':n_trees,      
        'learning_rate': learning_rate,     
        'max_depth': depth,                     
        'min_child_weight':1,                  
        'gamma':gamma,                          
        'subsample':1.0,                      
        'colsample_bytree':1.0,             
        'reg_alpha':0,                        
        'reg_lambda':0,                              
        'max_delta_step':0,               

        'objective':objective,               
        'tree_method':'gpu_hist',            
        'gpu_id':0,                          
        'predictor':'gpu_predictor',
        'seed':1,                            

        'num_class':len(np.unique(y_test))          
    }

    fit_parameters = {
        'X': X_train,
        'y': y_train,
        "sample_weight" : X_train_class_weight,
        'eval_set': [(X_train, y_train), (X_test, y_test)],
        'eval_metric': metric,
        'early_stopping_rounds': 100,
        'verbose': 0
    }

    xgb_model = xgb.XGBClassifier(**hyperparameters)

    xgb_model.fit(X_train, y_train, **fit_parameters)

    predictions = xgb_model.predict(X_test, ntree_limit = xgb_model.best_ntree_limit)
    predictions_proba = xgb_model.predict_proba(X_test, ntree_limit = xgb_model.best_ntree_limit)

    #Save stuff
    dict_results, df_results = results_presenter(framework_type = framework_type,algorithm_type = algorithm_type,y_test = y_test,predictions = predictions,predictions_proba = predictions_proba)

    xgb_model.save_model(f"{outpath}model.json")

    loss_plotter(framework_type = framework_type, model = xgb_model, metric = metric, save = True, savepath = f"{outpath}loss.pdf")

    confusion_matrix_plotter(
        algorithm_type = algorithm_type, 
        y_test = df_results['y_test'],
        predictions = dict_results['y_predictions'],
        labels = df['labels'],
        save = True,
        savepath = f"{outpath}confusion_matrix.pdf",
        channel = channel,
    )

    output_xgboost = {
        "meta" : metaBothRuns,
        "region" : df["region"],
        "include" : df["include"],
        "labels" : df["labels"],
        "output" : {
            "X_test" : X_test, 
            "y_test" : y_test,
            "X_test_selection_name" : X_selection_name,
            "X_test_weights" : X_weight,
            "X_class_test_weights" : X_class_weight,
            "X_run" : X_run,
            "dict_results" : dict_results,
            "df_results" : df_results,
        },
    }
    with open(f"{outpath}output_xgboost_VV_tweak_no_NNDecayMode.pkl", "wb") as file:
        pickle.dump(output_xgboost, file)

    print("finished dumping")

print("done")

    




    

    
 
    
    
    


